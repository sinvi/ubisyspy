**UbiSysPy dependencies:**

  * python
  * sqlite3
  * smartmontools
  * sysstat


**Graph dependencies:**

  * python-virtualenv
  * pip (in virtualenv)
  * pygal (by pip)
  * flask (by pip)
  * python-crontab (by pip)
