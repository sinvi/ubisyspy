# Synopsis
UbiSysPy is a console graphic program to get basic system information,
to set some system settings (like swappiness) and to save system logs.

There is a Flask application for the UbiSysPy to display the saved
system logs in interactive graphs.

# Installation
To install UbiSysPy to Ubuntu 16.04 you should copy the main UbiSysPy
directory to an arbitary path without accented characters.

Older versions of Ubuntu and other distros based on Ubuntu are not
tested with UbiSysPy, however it can run. The intallation of UbiSysPy
to other distro than Ubuntu 16.04 is not suggested.

# Dependencies
You need some packages to install before you can use UbiSysPy.

Mandatory dependencies are the following:

  * python
  * python-virtualenv
  * sqlite3

You can install the named packages in Ubuntu 16.04 with the
following command:
`sudo apt install python python-virtualenv sqlite3`

There are other, optional packages which you can install also.
Although you **do not have** to install these packages manaully.
UbiSysPy will ask for your permission to install these packages
when you want to use them.

These packages are the following:

  * smartmontools
  * sysstat

UbiSysPy uses virtualenv and requires the following Python modules
(by pip):

  * pygal
  * flask
  * python-crontab

You **do not have** to install these Python modules manaully,
UbiSysPy can install them automatically if you want.

For details, please see the next section.

# Usage
After you place UbiSysPy in a directory, you should initialize the
virtualenv and install the necessary Python modules.

You can do that with the following command:
`/bin/bash <PATH/TO/UBISYSPY>/virtualenv_init.sh`

This will create a directory (named venv) and install the necessary
Python modules by virtualenv's pip.

After the initialization you can start UbiSysPy with the following
command:
`/bin/bash <PATH/TO/UBISYSPY>/menu_init.sh`

After you set the saving schedule of system logs in UbiSysPy (menu 5),
you can use the Flask to view the interactive graphs generated from the
saved logs.

You can start Flask with the following command:
`/bin/bash <PATH/TO/UBISYSPY>/flask_init.sh`

Next, you should open the appearing link in a web browser to view
graphs.

# Contributors
UbiSysPy was written by Vivien Sinkovics <sinkovics.vivien@gmail.com>.

# License
Distributed under the GNU GPLv2 license. See COPYING.txt for more
information.
