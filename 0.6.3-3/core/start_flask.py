#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A Flasket indítja el.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
from graph import create_graphs_for_flask as cpf

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges

# meghívjuk a Flaskot indító függvényt
cpf.start()
