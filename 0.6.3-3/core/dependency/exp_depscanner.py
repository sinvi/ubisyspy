#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2014, Takács László
Module: Dependency scanner

This file is free software: you may copy, redistribute and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 2 of the License.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

__author__  = "Takács László"
__name__    = "Dependency scanner"
__version__ = 0.1
__updated__ = "2014-11-01"
__license__ = "GPLv2"

import commands


"""
    Függőségek, jelenleg _csak_ Ubuntu alatt vannak a csomagnevek tesztelve
    csomagnév   :: használt_app
"""

# A függőségeket tartalmazó szótár tesztekkel.
packages = {
    'lm-sensors': {
                'app'   :   'sensors',
                'test'  :   'sensors',
                'return':   0
                },
    ##
    'net-tools' : {
                'app'   : 'ifconfig',
                'test'  : 'ifconfig',
                'return': 0
                },
    ##
    'udisks'    : {
                'app'   : 'udisks',
                'test'  : 'udisks --enumerate',
                'return' : 0
                },
    ##
    'sysstat'   : {
                'app'   : 'sar',
                'test'  : 'sar -h',
                'return': 0
                },
    ##
    'toilet'   : {
                'app'   : 'toilet',
                'test'  : 'toilet -v',
                'return': 0
                },
    ##
    'iproute2'  : {
                'app'   : 'ip',
                'test'  : 'ip -V',
                'return': 0
    }
}


def dep_check(program,op=""):
    """
    Függőség ellenőrző függvény két paraméterrel.

    A program paraméter a teszelendő/keresett csomag neve.
    Az op opcionális paraméter, amely a függőséget igénylő művelet neve.
    True-val tér vissza, ha telepített a függőség, és False-szal, ha nem.
    Ezt használjuk a hívása helyén a loadnow-ban.
    """
    #print "A keresett csomag: {nev}".format(nev=program)

    # ha a netifaces-t keressük, a try_netifaces() által visszaadott értéket vizsgáljuk
    if program == "netifaces" or program == "python-netifaces":
        if try_netifaces() == False:    # ha a visszatérési érték hamis, a netifaces nincs telepítve
            print "FIGYELEM!\nA(z) {op} művelet által igényelt csomag ({prog}) NEM telepített függőség.".format(op=op,prog=program)
            return False    # hamissal térünk vissza, hogy a hívó loadnow tudjon rá építeni
        else:
            print "A(z) {op} művelet által igényelt csomag ({prog}) telepített függőség.".format(op=op,prog=program)
            return True
    else:
        keys = packages.keys()  # a keys tartalmazza a szótár kulcsait
        if program in keys: # ha a megadott programnév szerepel a kulcsok között, függőség
            #print "A keresett csomag ({prog}) függőség:".format(prog=program),
            app = packages[program]['app']
            test = packages[program]['test']
            ret = packages[program]['return']

            if commands.getstatusoutput(test)[0] == ret:    # ha a test visszatérési értéke egyenlő a ret-tel, telepítve van
                #print "Megtaláltam a programot."
                print "A(z) {op} művelet által igényelt csomag ({prog}) telepített függőség.".format(op=op,prog=program)
                return True
            else:
                #print "NEM találom a programot!"
                print "FIGYELEM!\nA(z) {op} művelet által igényelt csomag ({prog}) NEM telepített függőség.".format(op=op,prog=program)
                return False
        else: # ha nem szerepel a kulcsok között, megnézzük az értékeket
            #print "A keresett csomag ({prog}) nincs a kulcsok között. Keresés az értékek között...".format(prog=program),
            for key in keys:    # bejárjuk a kulcsokat
                if packages[key]['app'] == program: # ha szerepel a kulcsok között, függőség és...
                    #print "A csomag függőség: ",
                    program = key   # ...felülírjuk a megadott parancsnevet az őt tartalmazó csomaggal, és azt ellenőrízzük
                    app = packages[program]['app']
                    test = packages[program]['test']
                    ret = packages[program]['return']

                    if commands.getstatusoutput(test)[0] == ret:    # ha a test visszatérési értéke egyenlő a ret-tel, telepítve van
                        print "A(z) {op} művelet által igényelt csomag ({prog}) telepített függőség.".format(op=op,prog=program)
                        #print "Megtaláltam a programot."
                        return True
                    else:
                        #print "NEM találom a programot!"
                        print "FIGYELEM!\nA(z) {op} művelet által igényelt csomag ({prog}) NEM telepített függőség.".format(op=op,prog=program)
                        return False
            print "A csomag ({prog}) nem függőség.".format(prog=program)    # ha nem netifaces, és nincs a szótárban, nem függőség


def try_netifaces():
    """
    Teszteli a netifaces modult.

    Igazzal tér vissza, ha telepítve van, és Hamissal, ha nem.
    """
    try:
        import netifaces
        return True
    except:
        return False


def all_dept_check():
    """
    Az összes függőséget egyszerre vizsgálja.

    Bejárjuk az egész függőséget tartalmazó szótárt, és megvizsgáljuk, hogy melyik van telepítve.
    """
    # TODO: belevenni a netifaces-t.

    print "\nFüggőségek ellenőrzése..."
    for package in packages.keys():
        app = packages[package]['app']
        test =  packages[package]['test']
        ret = packages[package]['return']

        if commands.getstatusoutput(test)[0] == ret:
            print "[+] Megtaláltam a(z) {package}\{app} programot.".format(package=package, app=app)
        else:
            print "[-] NEM találom a(z) {package}\{app} programot!".format(package=package, app=app)


if __name__ == "__main__":
    dep_check("net-tools","IP és MAC lekérdezés")
    dep_check("kismalac")
    dep_check("sensors","CPU hőmérséklet ellenőrzés")
    dep_check("netifaces","MAC és IP lekérdezés")
    dep_check("iproute2","MAC és IP lekérdezés")
    dep_check("ip","MAC és IP lekérdezés")
    all_dept_check()
