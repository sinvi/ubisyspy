#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A UbiSysPy függőség ellenőrzője.

Készült Takács László Dependency scannerje alapján:
./dependency/exp_despscanner.py

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.

This file incorporates work covered by the following copyright and
permission notice:

Copyright (c) 2014, Takács László
Module: Dependency scanner

This file is free software: you may copy, redistribute and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 2 of the License.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__  = "Takács László"
__name__    = "Dependency scanner"
__version__ = 0.1
__updated__ = "2014-11-01"
__license__ = "GPLv2"
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import commands


# A függőségeket tartalmazó szótár tesztekkel.
packages = {
    'lm-sensors': {
        'app': 'sensors',
        'test': 'sensors',
        'return': 0
    },
    'udisks': {
        'app': 'udisks',
        'test': 'udisks --enumerate',
        'return': 0
    },
    'sysstat': {
        'app': 'sar',
        'test': 'sar -h',
        'return': 0
    },
    'smartmontools': {
        'app': 'smartctl',
        'test': 'smartctl -v',
        'return': 256
    },
}


def dep_check(program):
    """
    Függőség ellenőrző függvény két paraméterrel.

    A program paraméter a teszelendő/keresett csomag neve.
    True-val tér vissza, ha telepített a függőség, és False-szal, ha nem.
    """

    keys = packages.keys()  # a keys tartalmazza a szótár kulcsait

    # ha a megadott programnév szerepel a kulcsok között, függőség
    if program in keys:
        # a program teszteléséhez szükséges parancs kimentése a szótárból
        test = packages[program]['test']
        # az elvárt visszatérési érték kimentése a szótárból
        ret = packages[program]['return']

        # ha a test visszatérési értéke egyenlő a ret-tel, telepítve van
        if commands.getstatusoutput(test)[0] == ret:
            # igazzal térünk vissza
            return True

        # ha a test visszatérési értéke nem egyenlő a ret-tel, nincs telepítve
        else:
            # hamissal térünk vissza
            return False

    # ha nem szerepel a kulcsok között, megnézzük az értékeket
    else:
        # bejárjuk a kulcsokat
        for key in keys:
            # ha szerepel a kulcsok között, függőség és...
            if packages[key]['app'] == program:
                # ...felülírjuk a megadott parancsnevet az őt tartalmazó
                # csomaggal, és azt ellenőrízzük
                program = key
                test = packages[program]['test']
                ret = packages[program]['return']

                # ha a test visszatérési értéke egyenlő a ret-tel, telepítve van
                if commands.getstatusoutput(test)[0] == ret:
                    # igazzal térünk vissza
                    return True

                # ha a test visszatérési értéke nem egyenlő a ret-tel,
                # nincs telepítve
                else:
                    # hamissal térünk vissza
                    return False
