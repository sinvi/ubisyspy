#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A rendszerbeállításokat végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import os
import subprocess
import time
import shutil
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def get_default_shell():
    """Lekérdezi az alapértelmezett shellt."""

    # tömb a visszatérési értékeknek
    to_return = []

    # felhasználó lekérdezése
    username = os.environ["USER"]

    # aktuális shell lekérdezése
    shell = os.environ["SHELL"]
    # command = subprocess.Popen(["echo", "$0"], stdin=subprocess.PIPE,
    #    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # shell = command.communicate()

    # visszatérési értékek hozzáadása a tömbhöz
    to_return.append("A jelenleg aktív shelled:")
    to_return.append(shell)

    # összepipe-oljuk a parancsokat
    cat = subprocess.Popen(["cat", "/etc/passwd"], stdout=subprocess.PIPE)
    # szűrűnk a felhasználónévre
    grep = subprocess.Popen(["grep", username], stdin=cat.stdout,
        stdout=subprocess.PIPE)
    # a shellt tartalmazó oszlopot írjuk ki
    awk = subprocess.Popen(["cut", "-f7", "-d:"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # elmentjük a kimenetet
    new_shell = awk.stdout.read()
    # mentés sortörés nélkül
    new_shell = new_shell.strip("\n")

    # ha az aktuális és a /etc/passwd-ben lévő shell nem egyezik, le lett
    # cserélve, ám újraindítás még nem volt
    if shell != new_shell:
        message = "Az X újraindítása után a következő shell lesz használatban:"

        # opcionális visszatérési értékek hozzáadása a tömbhöz
        to_return.append(message)
        to_return.append(new_shell)
        # új sor a cursesben a kérdés elé
        to_return.append("")

    # visszatérés a tömbbel
    return to_return


def get_shell_list():
    """Lekérdezi az elérhető shelleket."""

    # tömb a visszatérési értékeknek
    to_return = []

    # megnyitjuk a fájlt olvasásra
    with open("/etc/shells") as f:
        # soronként beolvassuk
        for num, line in enumerate(f):
            # ha a sorban nincs #,
            if line.find("#") == -1:
                # eléírjuk a sorszámát
                line = str(num) + ") " + line
                # hozzáadjuk a tömbhöz
                to_return.append(line)

    # visszatérés a tömbbel
    return to_return


def change_shell(shell, passwd):
    """Módosítja a felhasználó alapértelmezett shelljét.

    A shell paraméter a beállítani kívánt shell.
    A passwd paraméter a felhasználó jelszava, vagy True érték esetén a
    felhasználó root voltát jelzi."""

    # felhasználó lekérdezése
    username = os.environ["USER"]

    # ha a felhasználó root
    if passwd == True:
        # parancs végrehajtása
        execute = subprocess.Popen(["usermod", username, "-s", shell],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = execute.communicate()
        exitcode = execute.returncode

    # különben
    else:
        # parancs végrehajtása
        execute = subprocess.Popen(["sudo", "-S", "usermod", username, "-s",
            shell], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = execute.communicate(passwd + '\n')
        exitcode = execute.returncode

        # felülírjuk a felhasználó jelszavát
        passwd = os.urandom(len(passwd))

    # ha nullával tér vissza, sikeres
    if exitcode == 0:
        return True

    # különben nem
    else:
        return False


def get_current_swappiness():
    """Lekérdezi az akutális swappiness értéket."""

    # tömb a visszatérési értékeknek
    to_return = []

    # a jelenlegi swappiness érték lekérdezése
    cat = subprocess.Popen(["cat", "/proc/sys/vm/swappiness"],
        stdout=subprocess.PIPE)
    # beolvasás
    swappiness = cat.stdout.read()
    # mentés sortörés nélkül
    swappiness = swappiness.strip("\n")

    # visszatérési értékek hozzáadása a tömbhöz
    to_return.append("A jelenlegi swappiness értéked (0-100):")
    to_return.append(swappiness)

    # visszatérés a tömbbel
    return to_return


def temp_change_swappiness(value, passwd):
    """Az aktuális munkamenetben módosítja a swappinesst.

    A value az érték, amire átállítjuk a swappinesst.
    A passwd a felhasználó jelszava, vagy True érték esetén a felhasnáló
    root voltát jelzi."""

    # ha a value érték nem szám vagy az érték nagyobb száznál
    if not value.isdigit() or int(value) > 100:
        # kettővel térünk vissza
        return 2

    # különben
    else:
        # sztring összefűzése az új értékkel
        new_value = "vm.swappiness=" + value

        # ha a felhasználó root
        if passwd == True:
            temp_swappiness = subprocess.Popen(["sysctl", new_value],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = temp_swappiness.communicate()
            exitcode = temp_swappiness.returncode

            # ha nullával tér vissza, sikeres
            if exitcode == 0:
                return 0

            # különben sikertelen
            else:
                return 1

        # ha a felhasználó nem root
        else:
            temp_swappiness = subprocess.Popen(["sudo", "-S", "sysctl",
                new_value], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = temp_swappiness.communicate(passwd + '\n')
            exitcode = temp_swappiness.returncode

            # ha nullával tér vissza, sikeres
            if exitcode == 0:
                return 0

            # különben sikertelen
            else:
                return 1


def change_swappiness(value, passwd):
    """Módosítja a swappinesst (állandó jelleggel).

    A value az érték, amire átállítjuk a swappinesst.
    A passwd a felhasználó jelszava, vagy True érték esetén a felhasnáló root
    voltát jelzi."""
    to_return = []  # a visszatérési értékeknek

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/sysctl/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)

    # előállítjuk a mentési fájl nevét és útvonalát
    backup_path = into_mkdir + "sysctl_" + time_stamp + ".bak"

    # kiírjuk a mentés helyét
    to_return.append("A /etc/sysctl.conf mentési helye:")
    to_return.append("{path}".format(path=backup_path))

    original_path = "/etc/sysctl.conf"

    # készítünk egy mentést a /etc/sysctl.conf-ról
    try:
        shutil.copy(original_path, backup_path)

    # ha IO hibát kapunk, nem sikerült
    except IOError:
        # hárommal térünk vissza, hogy jelezzük az IO hibát
        return 3

    # ha shutil hibát kapunk, szintén nem sikerült
    except shutil.Error:
        # néggyel térünk vissza, hogy jelezzük a shutil hibát
        return 4

    # ha OSError-t kapunk, valószínűleg létezik a fájl
    except OSError:
        # öttel térünk vissza, hogy jelezzük az OS hibát
        return 5

    # ha sikerült a másolás
    else:
        # készítünk egy ideiglenes állományt a módosításoknak
        temp_path = into_mkdir + "modified-sysctl_" + time_stamp

        # megnyitjuk írásra (w+) az ideiglenes állományt
        with open(temp_path, "w+") as temp:
            # megnyitjuk olvasásra a /etc/sysctl.conf-ot
            with open(original_path, "r") as sysctl:
                # végigmegyünk a /etc/sysctl.conf sorain
                for line in sysctl:
                    # ha szerepel benne sor, ami tartalmazza a
                    # vm.swappiness=XY-t, felülírjuk
                    if line.find("vm.swappiness") != -1:
                        # kicseréljük a sort semmire
                        new_line = ""
                        # az új sort kiírjuk az ideiglenes állományba
                        temp.write(new_line)

                    # ha nem szerepel benne, a sor átírjuk a temp fájlba is
                    else:
                        # beírjuk az eredeti sort
                        temp.write(line)

                # mindkét esetben beírjuk a kívánt értéket tartalmazó sort
                swappiness_line = "vm.swappiness=" + value
                temp.write(swappiness_line)

    # ha a felhasználó root
    if passwd == True:
        # módosított sysctl visszamásolása a /etc/-be
        command = subprocess.Popen(["cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate()
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2

    # ha a felhasználó nem root
    else:
        # módosított sysctl visszamásolása a /etc/-be
        command = subprocess.Popen(["sudo", "-S", "cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        exitcode = command.returncode

        # felülírjuk a felhasználó jelszavát
        passwd = os.urandom(len(passwd))

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["sudo", "-S", "chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate(passwd + '\n')
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2


def add_alias(alias, command):
    """Alias hozzáadása a ~/.bash_aliases állományhoz.

    Első paramétere, az alias, az új alias neve.
    Második paramétere, a command, a parancs maga."""

    to_return = []

    # visszatérési érték inicializálás hamisra
    status = False

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    bash_aliases = user_dir + "/.bash_aliases"

    # megnézzük, létezik-e a fájl
    # ha létezik, készítünk róla mentést
    if os.path.exists(bash_aliases):
        # létrehozzuk az elérési út sztringjét
        into_mkdir = user_dir + "/.ubisyspy/bash/"
        # időbélyeg a biztonsági mentéshez
        time_stamp = time.strftime('%Y-%m-%d_%T')

        # ha nem létezik, létrehozzuk
        if not os.path.isdir(into_mkdir):
            os.makedirs(into_mkdir)
            os.chdir(into_mkdir)

        # ha létezik, belépünk
        else:
            os.chdir(into_mkdir)

        # előállítjuk a mentési fájl nevét és útvonalát
        backup_path = into_mkdir + ".bash_aliases_" + time_stamp + ".bak"

        # kiírjuk a mentés helyét
        return_string = "A " + bash_aliases + " mentési helye:"
        to_return.append(return_string)
        to_return.append("{path}".format(path=backup_path))

        try:
            # készítünk egy mentést a ~/.bash_aliases-ról
            shutil.copy(bash_aliases, backup_path)

        # ha IO hibát kapunk, nem sikerült
        except IOError:
            # hárommal térünk vissza, hogy jelezzük az IO hibát
            return 3

        # ha shutil hibát kapunk, szintén nem sikerült
        except shutil.Error:
            # néggyel térünk vissza, hogy jelezzük a shutil hibát
            return 4

        # ha OSError-t kapunk, valószínűleg létezik a fájl
        except OSError:
            # öttel térünk vissza, hogy jelezzük az OS hibát
            return 5

    # ha nem létezik, létrehozzuk
    else:
        os.mknod(bash_aliases)

    # összefűzzük magát az alias parancsot, az új alias nevét, a hozzá
    # tartozó parancsot és a sortörést
    new_alias = "alias " + alias + "='" + command + "'\n"

    # megnyitjuk a bash_aliases-t írásra
    with open(bash_aliases, "a") as f:
        # hozzáfűzzük az új aliast
        f.write(new_alias)
        # ha sikerült, True-ra állítjuk
        status = True

    # visszatérünk a status-szal
    return [to_return, status]


def compare_dirs(src_dir, dest_dir):
    """A felhasználó által megadott könyvtárak méretét hasonlítja össze.

    Az első paramétere, a src_dir, a mentendő mappa.
    A második paramétere, a dest_dir, a célkönyvtárban elárhető szabad hely
    mérete."""

    # a forrás, mentendő könyvtár méretének lekérdezése bájtban
    # az első elem a könyvtár mérete
    # megpróbáljuk lekérdezni a könyvtár méretét
    try:
        source_size = subprocess.check_output(['du', '-s', src_dir]).split()[0]

    # ha kivételt dob, nincs ilyen könyvtár
    except subprocess.CalledProcessError:
        # hamissal térünk vissza
        return False

    # ha nincs kivétel, összehasonlítjuk a két könyvtárat
    else:
        # a célkönyvtárhoz tartozó eszköz szabad méretének lekérdezése bájban
        dest_dir_size = os.statvfs(dest_dir)

        # összehasonlítjuk a forrás és a célkönyvtár méretét
        # ha a célkönyvtár nagyobb, igazzal térük vissza
        if dest_dir_size > source_size:
            return True

        # ha a célkönyvtár kisebb, False-szal
        else:
            return False
