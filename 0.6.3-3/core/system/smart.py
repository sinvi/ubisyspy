#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A SMART-hoz szükséges függvényeket tartalmazza.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import os
import subprocess
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def get_devices():
    """Listába gyűjti a lemezeket.

    Összegyűjti a számítógépen elérhető blokkeszközöket egy tömbbe
    és visszatér ezzel a tömbbel.
    """

    to_return = []  # tömb a visszatérési értékeknek
    # az eszközöket tartalmazza
    check_device = os.popen(
        "ls -al /sys/block/*d* | awk {'print $9'}").readlines()
    devices = []  # üres lista a _különböző_ eszközöknek
    for dev in check_device:  # végigjárjuk a lista elemeit
        dev = dev.rstrip("\n")
        dev = dev.replace("/sys/block", "/dev")
        devices.append(dev)  # hozzáadjuk a listához
    devices = list(set(devices))  # kiszűrjük az ismétlődéseket
    devices.sort()  # rendezzük a listát

    dev_num = len(devices)  # az eltérő eszközök száma
    # hozzáadjuk a tömbhöz az eszközök számát sztringként
    to_return.append(str(dev_num))

    for i in devices:
        to_return.append(i)  # hozzáadjuk a tömbhöz az eszközöket

    return to_return


def get_smart(device, passwd, number, dev_num):
    """Tömbe gyűjti a paraméterül kapott lemez adatait.

    Az első paraméter, a device adja meg, hogy melyik eszköz adatait kell
    lekérdezni.
    A második paraméter, a password adja meg a felhasználó sudo jelszavát
    vagy a felhasználó root voltát, ha az érték True.
    A harmadik, number paraméter az éppen soron lévő eszközök sorszáma.
    A negyedik, dev_num paraméter az összes eszköz száma.
    """

    to_return = []  # tömb a visszatérési értékeknek
    smart_list = []  # tömb a Popenhez

    # ha a felhasználó root, nem kell jelszó a parancsba
    if passwd == True:
        # végrehajtjuk a SMART lekérdezést
        command = subprocess.Popen(['smartctl', '-a', '-d', 'sat', device],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()

    # ha a felhasználó nem root, kell jelszó
    else:
        # végrehajtjuk a SMART lekérdezést
        command = subprocess.Popen(
            ['sudo', '-S', 'smartctl', '-a', '-d', 'sat', device],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        # megadjuk a jelszót
        communicate = command.communicate(passwd + '\n')
        # eltároljuk a visszatérési értéket
        exitcode = command.returncode

    # bejárjuk a listává alakított Popen által visszaadott truple-t
    for items in list(communicate):
        # bejárjuk a szuperlista listáját soronként
        for string in items.splitlines():
            # a nem üres sorokat tömbbet tesszük
            if string != "":
                smart_list.append(string)

    # kezdőértéket adunk a változónak
    support = False
    # kezdőértéket adunk a rotation változónak
    rotation = ""
    # kezdőérték a modelnek
    model = ""
    # kezdőérték a capacity-nek
    capacity = ""
    # kezdőérték a health-nek
    health = ""

    # bejárjuk az előbb létrehozott tömböt, hogy
    for num, item in enumerate(smart_list):
        # ha engedélyezve van a SMART, felülírjuk a support = False-ot
        if item.find("SMART support is: Enabled") != -1:
            support = smart_list[num]
        # megkeressük az alábbi elemeket:
        elif item.find("Device Model") != -1:
            # változóba mentjük a tömb adott elemét
            model = smart_list[num]
        elif item.find("User Capacity") != -1:
            # változóba mentjük a tömb adott elemét
            capacity = smart_list[num]
        elif item.find("Rotation Rate") != -1 and item.find("rpm") != -1:
            # ha van megadva érték, HDD, ezért ezt mentjük változóba
            rotation = "Az eszköz típusa: HDD"
        elif item.find("Rotation Rate") != -1 and item.find("rpm") == -1:
            # ha nem forog, az eszköz SSD
            rotation = "Az eszköz típusa: SSD"
        elif item.find("overall-health") != -1 and item.find("PASSED") != -1:
            # ha az eszköz egészséges
            health = "Az eszköz egészséges."
        elif item.find("overall-health") != -1 and item.find("PASSED") == -1:
            # ha az érték nem PASSED, nem egészséges
            health = "Az eszköz NEM egészséges!'"
        # ha nem volt Rotation Rate érték
        elif not rotation:
            # ha van Spinup attribútum, HDD
            if item.find("Spin_Up_Time") != -1 or item.find("3") != -1:
                # ha van felpörégis idő, HDD
                rotation = "Az eszköz típusa: HDD"
            # ha nincs, SSD
            else:
                # ha nincs felpörgési idő, SSD
                rotation = "Az eszköz típusa: SSD"

    # ha nem találtuk meg a "SMART support is: Enabled" sort
    if not support:
        # hamissal térünk vissza
        return False

    # különben
    else:
        # lefordítjuk a sztinget magyarra
        model = model.replace("Device Model:", "Az eszköz modellje:")
        model = model.replace("    ", "")
        # lefordítjuk a sztinget magyarra
        capacity = capacity.replace("User Capacity:", "Az eszköz mérete: ")
        capacity = capacity.replace("    ", "")
        # összegzés összefűzése
        summary = "{dn}/{n}. észlelt eszköz".format(dn=dev_num, n=number)
        # sztring összehűzése az eszköz nevével
        dev = "SMART összegzés a {dev} eszközhöz.".format(dev=device)
        # sortörés a tagolásért
        line_break = ""

        to_return.append(summary)
        to_return.append(line_break)
        to_return.append(dev)  # hozzáadjuk a tömbhöz az eszközt
        to_return.append(rotation)  # a fajtáját
        to_return.append(model)  # a modelljét
        to_return.append(capacity)  # a kapacitását
        to_return.append(health)  # az összegző állapotát

        return to_return  # visszatérünk a tömbbel


def smart_details(device, passwd, rotation, number, dev_num):
    """
    Lekérdezi és kiírja a SMART részleteket.

    Van-e hibás szektor, hány lehetséges hibás és hány áthelyezett szektor
    van a lemezen.
    Kiírja a lemez fontosabb SMART értékeit.
    A device paraméter adja meg a függvénynek, hogy melyik eszközt kell
    vizsgálnia. Pl. /dev/sda
    A passwd paraméter adja meg a felhasználó sudo jelszavát a művelethez
    vagy azt, hogy a felhasználó root (ha az érték True).
    A rotation paraméter adja meg, hogy HDD vagy SSD az eszköz.
    A negyedik, number paraméter az éppen soron lévő eszközök sorszáma.
    Az ötödik, dev_num paraméter az összes eszköz száma.
    """


    to_return = []  # tömb a visszatérési értékeknek
    smart_list = []  # tömb a Popenhez

    # kezdőértékek HDD_nek
    pending = -1
    raw = -1
    multizone = -1

    # kezdőértékek SSD-nek
    errors = -1
    used_reserved = -1
    wear = -1

    # ha a felhasználó root, nem kell jelszó a parancsba
    if passwd == True:
        # végrehajtjuk a SMART lekérdezést
        out = subprocess.Popen(['smartctl', '-a', '-d', 'sat', device],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE).communicate()

    # ha a felhasználó nem root, kell jelszó
    else:
        # végrehajtjuk a SMART lekérdezést
        out = subprocess.Popen(
            ['sudo', '-S', 'smartctl', '-d', 'sat', '-a', device],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE).communicate(passwd + '\n')

    # bejárjuk a listává alakított Popen által visszaadott truple-t
    for items in list(out):
        # bejárjuk a szuperlista listáját soronként
        for string in items.splitlines():
            # a nem üres sorokat tömbbet tesszük
            if string != "":
                smart_list.append(string)

    # ha SSD, ezekre lesz szükségünk
    if rotation.find("SSD") != -1:
        dev_type = "SSD"
        wear = "Nem található Wear_Leveling_Count attribútum."
        used_reserved = "Nem található Used_Rsvd_Blk_Cnt_Tot attribútum."
        errors = "Nem található Uncorrectable_Error_Cnt attribútum."

    # ha HDD, ezekre
    else:
        dev_type = "HDD"
        pending = "Az eszköz nem támogatja a függő szektorok lekérdezését."
        multizone = "Az eszköz nem támogatja az írási hibák számának " \
                    "lekérdezését."
        raw = "Az eszköz nem támogatja az olvasási hibák számának lekérdezését."

    # mindkét esetben kellenek ezek
    temperature = "Az eszköz nem támogatja a hőmérséklet lekérdezését."
    power_time = "Az eszköz nem támogatja a bekapcsolva töltött idő " \
                 "lekérdezését."
    power_count = "Az eszköz nem támogatja a bekapcsolási ciklusok " \
                  "lekérdezését."
    reallocated = "Az eszköz nem támogatja az áthelyezett szektorok " \
                  "lekérdezését."

    # bejárjuk az előbb létrehozott tömböt, hogy
    for num, item in enumerate(smart_list):

        # ha az eszköz SSD:
        if dev_type == "SSD":
            # az alábbi SMART értékeket tartalmazó sorok indexét megtudjuk
            if item.find("Wear_Leveling_Count") != -1\
                    or item.find("177", 0, 3) != -1:
                # és változóba mentjük a tömb adott elemét
                wear = "Wear Leveling Count: " + \
                       smart_list[num].rsplit(None, 1)[-1]

            elif item.find("Used_Rsvd_Blk_Cnt_Tot") != -1\
                    or item.find("179", 0, 3) != -1:
                used_reserved = "Használatban lévő fenntartott blokkok száma: "
                used_reserved += smart_list[num].rsplit(None, 1)[-1]

            elif item.find("Uncorrectable_Error_Cnt") != -1\
                    or item.find("187", 0, 3) != -1:
                errors = "Jelentett javíthatatlan hibák száma: " + \
                         smart_list[num].rsplit(None, 1)[-1]

        # ha az eszköz HDD
        elif dev_type == "HDD":
            # az alábbi SMART értékeket tartalmazó sorok indexét megtudjuk
            if item.find("Current_Pending_Sector") != -1\
                    or item.find("197", 0, 3) != -1:
                pending = "Függő szektorok száma: " + \
                          smart_list[num].rsplit(None, 1)[-1]

            elif item.find("Multi_Zone_Error_Rate") != -1\
                    or item.find("200", 0, 3) != -1:
                multizone = "Írási hibák száma: " + \
                            smart_list[num].rsplit(None, 1)[-1]

            elif item.find("Raw_Read_Error_Rate") != -1\
                    or item.find("1", 0, 1) != -1:
                raw = "Olvasási hibák száma: " + \
                      smart_list[num].rsplit(None, 1)[-1]

        # közös SMART értékek
        # az alábbi SMART értékeket tartalmazó sorok indexét megtudjuk

        # hőmérsékleti értékek keresése
        # ha SMART attribútum ID-t használunk, a find()-ba
        # kezdő és végpozíciót is bele kell írni, különben az egész
        # sorban egyezést keres
        if item.find("Temperature_Celsius") != -1 or item.find(
            "Airflow_Temperature_Cel") != -1\
                or item.find("194", 0, 2) != -1\
                or item.find("190", 0, 3) != -1:
            # ha megtaláltuk, kivágjuk az item utolsó szavát (hőmérséklet)
            # és hozzáfűzzük a szöveget és a hőmérsékletet
            # ha van min/max hőmérséklet
            if item.find("(") != -1 and item.find(")") != -1:
                # azt levágjuk és
                start = item.find("(")
                # úgy nézzük az utolsó szót
                temperature = "Az eszköz hőmérséklete: " + \
                              smart_list[num][:start].rsplit(None, 1)[-1] + "°C"

            # különben
            else:
                # csak kiírjuk az utolsót
                temperature = "Az eszköz hőmérséklete: " + \
                              smart_list[num].rsplit(None, 1)[-1] + "°C"

        # bekapcsolva töltött órák keresése
        elif item.find("Power_On_Hours") != -1 or item.find("9", 0, 1) != -1:
            # ha nem csak az üzemórákat jeleníti meg a SMART
            if item.find("Power_On_Hours_and_Msec") != -1:
                # pl.   9 Power_On_Hours_and_Msec 9372h+43m+20.950s
                # megkeressük a "h" betűt
                hour = smart_list[num].rsplit(None, 1)[-1].find("h")
                # addig írjuk ki a sztringet
                power_time = smart_list[num].rsplit(None, 1)[-1][:hour]

            # ha van zárójeles érték a valós érték mögött
            elif item.find("(") != -1 and item.find(")") != -1:
                # akkor megkeressük a nyitó zárójelet
                start = item.find("(")
                # és csak addig mentjük el a sztringet, ami a sorban az utolsó
                power_time = smart_list[num][:start].rsplit(None, 1)[-1]

            # különben
            else:
                # csak menjtük a sor utolsó sztringjét, ami az érték
                power_time = smart_list[num].rsplit(None, 1)[-1]

        # bekapcsolási ciklusok számának keresése
        elif item.find("Power_Cycle_Count") != -1\
                or item.find("12", 0, 2) != -1:
            power_count = "A bekapcsolási ciklusok száma: " + \
                          smart_list[num].rsplit(None, 1)[-1]

        # áthelyezett szektorok számának keresése
        elif item.find("Reallocated_Sector_Ct") != -1\
                or item.find("5", 0, 1) != -1:
            reallocated = "Az áthelyezett szektorok száma: " + \
                          smart_list[num].rsplit(None, 1)[-1]

    summary = "{dn}/{number}. észlelt eszköz".format(dn=dev_num, number=number)
    # sztring összehűzése az eszköz nevével
    dev = "SMART részletek a {dev} eszközhöz.".format(dev=device)
    # sortörés a tagolásért
    line_break = ""
    to_return.append(summary)
    to_return.append(line_break)
    to_return.append(dev)

    # hozzáadjuk a visszatérési tömbhöz
    to_return.append(temperature)  # a hőmérsékletet

    # ha az eszköz nem támogatja a bekapcsolva töltött idő lekérdezését
    # a power_time változó tartalma nem szám lesz, ezért ezt ellenőrizzük
    # ha szám, elvégezzük a számolást és visszatérünk az eredménnynel
    if power_time.isdigit():
        power_time_year = int(power_time) / (24 * 365)
        power_time_month = int(power_time) / (24 * 30) % 12
        power_time_day = (int(power_time) / 24) % 30
        power_time_string = "A bekapcsolva töltött idő: {y} év, {m} hónap, " \
                            "{d} nap".format(y=power_time_year,
                                             m=power_time_month,
                                             d=power_time_day)
        # hozzáadjuk a visszatérési tömbhöz
        to_return.append(power_time_string)  # a bekapcsolva töltött időt

    # ha nem szám, az eredeti sztringgel térünk vissza
    else:
        # hozzáadjuk a visszatérési tömbhöz
        to_return.append(power_time)  # a bekapcsolva töltött időt (nincs)

    # hozzáadjuk a visszatérési tömbhöz
    to_return.append(power_count)  # a bekapcsolási ciklusok számát
    to_return.append(reallocated)  # az áthelyezett szektorok számát

    # eszköz típustól függő  értékkel térünk visza
    # ha az eszköz HDD
    if dev_type == "HDD":
        to_return.append(pending)  # a függő szektorok számát
        to_return.append(raw)  # olvasási hibák számát
        to_return.append(multizone)  # írási hibák számát

    # ha az ezsköz SSD
    elif dev_type == "SSD":
        to_return.append(errors)  # javíthatatlan hibák számát
        to_return.append(used_reserved)  # használt fenntartott blokkok számát
        to_return.append(wear)  # a wear leveling countot

    return to_return  # visszatérünk a tömbbel
