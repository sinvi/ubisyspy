#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A sysstat beállítását végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import os
import subprocess
import time
import shutil
import locale
import multiprocessing

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def sysstat_init(passwd):
    """Elvégzi a sysstat telepítése után szükséges beállításokat.

    Paramétere, a passwd a felhasználó jelszava. True értéke esetén a
    felhasználó root voltát jelzi."""

    to_return = []  # a visszatérési értékeknek

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/sysstat/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)

    # előállítjuk a mentési fájl nevét és útvonalát
    backup_path = into_mkdir + "sysstat_" + time_stamp + ".bak"

    # kiírjuk a mentés helyét
    to_return.append("A /etc/default/sysstat mentési helye:")
    to_return.append("{path}".format(path=backup_path))

    original_path = "/etc/default/sysstat"

    # készítünk egy mentést a /etc/default/sysstat-ról
    try:
        shutil.copy(original_path, backup_path)

    # ha IO hibát kapunk, nem sikerült
    except IOError:
        # hárommal térünk vissza, hogy jelezzük az IO hibát
        return 3

    # ha shutil hibát kapunk, szintén nem sikerült
    except shutil.Error:
        # néggyel térünk vissza, hogy jelezzük a shutil hibát
        return 4

    # ha OSError-t kapunk, valószínűleg létezik a fájl
    except OSError:
        # öttel térünk vissza, hogy jelezzük az OS hibát
        return 5

    # ha sikerült a másolás
    else:
        # készítünk egy ideiglenes állományt a módosításoknak
        temp_path = into_mkdir + "modified-sysstat_" + time_stamp

        # megnyitjuk írásra (w+) az ideiglenes állományt
        with open(temp_path, "w+") as temp:
            # megnyitjuk olvasásra a /etc/default/sysstat-ot
            with open(original_path, "r") as sysstat:
                # végigmegyünk a /etc/default/sysstat sorain
                for line in sysstat:
                    # ha szerepel benne sor, ami tartalmazza az
                    # ENABLED="false"-t, felülírjuk
                    if line.find('ENABLED="false"') != -1:
                        # kicseréljük a sort
                        new_line = 'ENABLED="true"' + "\n"
                        # az új sort kiírjuk az ideiglenes állományba
                        temp.write(new_line)

                    # ha már true az érték
                    elif line.find('ENABLED="true"') != -1:
                        # hattal térünk vissza, jelezve, hogy nem kell
                        # sysstat újraindítás
                        return 6

                    # különben a sor átírjuk a temp fájlba is
                    else:
                        # beírjuk az eredeti sort
                        temp.write(line)

    # ha a felhasználó root
    if passwd == True:
        # módosított sysstat visszamásolása a /etc/default-ba
        command = subprocess.Popen(["cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate()
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2

    # ha a felhasználó nem root
    else:
        # módosított sysstat visszamásolása a /etc/default-ba
        command = subprocess.Popen(["sudo", "-S", "cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["sudo", "-S", "chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate(passwd + '\n')
            exit_code = command.returncode

            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))

            # kettővel térünk visza
            return 2


def sysstat_restart(passwd):
    """Újraindítja a sysstat folyamatot.

     Paramétere, a passwd a felhasználó jelszava. True értéke esetén a
    felhasználó root voltát jelzi."""

    # ha a felhasználó root
    if passwd == True:
        # parancs végrehajtása
        command = subprocess.Popen(["service", "sysstat", "restart"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        # visszatérési érték elmentése
        exit_code = command.returncode

    # ha a felhasználó nem root
    else:
        # parancs végrehajtása
        command = subprocess.Popen(["sudo", "-S", "service", "sysstat",
            "restart"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        # visszatérési érték elmentése
        exit_code = command.returncode

    # ha nullával tér vissza, sikeres
    if exit_code == 0:
        return 0

    # ha nem, sikertelen
    else:
        # eggyel térünk vissza
        return 1


def sysstat_setting(passwd, value, key):
    """A sysstat különböző értékeit állítja.

    Első paramétere, a passwd, a felhasználó jelszava. True érték esetén a
    felhasználó root voltát jelzi.
    Második paramétere, a value, a beállítandó, felhasználó által megadott
    érték.
    Harmadik paramétere, a key, az az attribútum, amihez beállítjuk a value
    értéket."""

    to_return = []  # a visszatérési értékeknek

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/sysstat/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)

    # előállítjuk a mentési fájl nevét és útvonalát
    backup_path = into_mkdir + "sysstat-conf_" + time_stamp + ".bak"

    # kiírjuk a mentés helyét
    to_return.append("A /etc/sysstat/sysstat mentési helye:")
    to_return.append("{path}".format(path=backup_path))

    original_path = "/etc/sysstat/sysstat"

    # készítünk egy mentést a /etc/sysstat/sysstat-ról
    try:
        shutil.copy(original_path, backup_path)

    # ha IO hibát kapunk, nem sikerült
    except IOError:
        # hárommal térünk vissza, hogy jelezzük az IO hibát
        return 3

    # ha shutil hibát kapunk, szintén nem sikerült
    except shutil.Error:
        # néggyel térünk vissza, hogy jelezzük a shutil hibát
        return 4

    # ha OSError-t kapunk, valószínűleg létezik a fájl
    except OSError:
        # öttel térünk vissza, hogy jelezzük az OS hibát
        return 5

    # ha sikerült a másolás
    else:
        # készítünk egy ideiglenes állományt a módosításoknak
        temp_path = into_mkdir + "modified-sysstat-conf_" + time_stamp

        # megnyitjuk írásra (w+) az ideiglenes állományt
        with open(temp_path, "w+") as temp:
            # megnyitjuk olvasásra a /etc/sysstat/sysstat-ot
            with open(original_path, "r") as sysstat:
                # végigmegyünk a /etc/sysstat/sysstat sorain
                for line in sysstat:
                    # ha szerepel benne sor, ami tartalmazza a
                    # 'key'=-t, felülírjuk
                    old_to_find = key + "="
                    if line.find(old_to_find) != -1:

                        # ha a kulcs ZIP, sztinget várunk, tehát idézőjelbe
                        # kell tenni az egyelőségjel után
                        if key == "ZIP":
                            value = '"' + value + '"'

                        # az új sor az attribútum és a user által megadott
                        # value érték összefűzése
                        # kicseréljük a sort
                        new_line = old_to_find + value + "\n"
                        # az új sort kiírjuk az ideiglenes állományba
                        temp.write(new_line)

                    # ha nem szerepel
                    else:
                        # beírjuk az eredeti sort
                        temp.write(line)

    # ha a felhasználó root
    if passwd == True:
        # módosított sysstat visszamásolása a /etc/sysstat-ba
        command = subprocess.Popen(["cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate()
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2

    # ha a felhasználó nem root
    else:
        # módosított sysstat visszamásolása a /etc/sysstat-ba
        command = subprocess.Popen(["sudo", "-S", "cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["sudo", "-S", "chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate(passwd + '\n')
            exit_code = command.returncode

            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))
            # kettővel térünk visza
            return 2


def change_cron(passwd, value):
    """A sysstat cronját állítja át.

    Első paramétere, a passwd, a felhasználó jelszava. True érték esetén a
    felhasználó root voltát jelzi.
    Második paramétere, a value, a beállítandó, felhasználó által megadott
    érték."""

    to_return = []  # a visszatérési értékeknek

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/sysstat/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)

    # előállítjuk a mentési fájl nevét és útvonalát
    backup_path = into_mkdir + "sysstat-cron_" + time_stamp + ".bak"

    # kiírjuk a mentés helyét
    to_return.append("A /etc/cron.d/sysstat mentési helye:")
    to_return.append("{path}".format(path=backup_path))

    original_path = "/etc/cron.d/sysstat"

    # készítünk egy mentést a /etc/cron.d/sysstat-ról
    try:
        shutil.copy(original_path, backup_path)

    # ha IO hibát kapunk, nem sikerült
    except IOError:
        # hárommal térünk vissza, hogy jelezzük az IO hibát
        return 3

    # ha shutil hibát kapunk, szintén nem sikerült
    except shutil.Error:
        # néggyel térünk vissza, hogy jelezzük a shutil hibát
        return 4

    # ha OSError-t kapunk, valószínűleg létezik a fájl
    except OSError:
        # öttel térünk vissza, hogy jelezzük az OS hibát
        return 5

    # ha sikerült a másolás
    else:
        # készítünk egy ideiglenes állományt a módosításoknak
        temp_path = into_mkdir + "modified-sysstat-cron_" + time_stamp

        # megnyitjuk írásra (w+) az ideiglenes állományt
        with open(temp_path, "w+") as temp:
            # megnyitjuk olvasásra a /etc/cron.d/sysstat-ot
            with open(original_path, "r") as sysstat:
                # végigmegyünk a /etc/cron.d/sysstat sorain
                for line in sysstat:

                    # a cron sor: az összegző cron sor vége debian-sa1 60 2,
                    # így nem tudjuk őket összekeverni
                    cron_line = "root command -v debian-sa1 > /dev/null && " \
                                "debian-sa1 1 1"

                    # megkeressük a sort, amiben szerepel a cron_line
                    if line.find(cron_line) != -1:
                        # a cron_line sor első "szavát" kell lecserélni a
                        # felhasználó által megadott érték alapján arra,
                        # hogy minden value-adik perceben készüljön napló
                        new_cron_minutes = "*/" + str(value) + " * * * * "
                        # az új cron perceket összefűzzük a cron_line-nal
                        new_cron_line = new_cron_minutes + cron_line + "\n"
                        # az új sort kiírjuk a temp fájlba
                        temp.write(new_cron_line)

                    # ha nem szerepel
                    else:
                        # visszaírjuk az eredeti sort
                        temp.write(line)


    # ha a felhasználó root
    if passwd == True:
        # módosított sysstat visszamásolása a /etc/cron.d/sysstat-ba
        command = subprocess.Popen(["cp", temp_path, original_path],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["chown", "root:root", original_path],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2

    # ha a felhasználó nem root
    else:
        # módosított sysstat visszamásolása a /etc/cron.d/sysstat-ba
        command = subprocess.Popen(["sudo", "-S", "cp", temp_path,
            original_path], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["sudo", "-S", "chown", "root:root",
                original_path], stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate(passwd + '\n')
            exit_code = command.returncode

            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))
            # kettővel térünk visza
            return 2


def print_cpu_log():
    """A CPU napló utolsó bejegyzését írja ki."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    cpu = subprocess.Popen(["sar", "-P", "ALL"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=cpu.stdout,
        stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó 9 sort
    tail = subprocess.Popen(["tail", "-9"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # szűrűnk az all szót tartalmazó szóra
    grep_all = subprocess.Popen(["grep", "all"], stdin=tail.stdout,
        stdout=subprocess.PIPE)
    # tr -s ' ' paranccsal az egymás melletti szóközöket megkeressük és egy
    # darabra cseréljük
    tr = subprocess.Popen(["tr", "-s", " "], stdin=grep_all.stdout,
        stdout=subprocess.PIPE)
    # kivágjuk az első oszlopot most,hogy már csak egy szóköz választja el
    # őket egymástól
    cut = subprocess.Popen(["cut", "-f1", "-d", " "], stdin=tr.stdout,
        stdout=subprocess.PIPE)

    # a cut kimenet lesz a naplózás ideje
    cpu_log_time = cut.stdout.read()
    # mentés sortörés nélkül
    cpu_log_time = cpu_log_time.strip("\n")

    # ha nincs dátum, még nincs mentés
    if len(cpu_log_time) == 0:
        to_return.append("Még nincs mentett CPU napló.")
        return to_return

    # ha van dátum, van napló is
    else:
        # sztringet készítünk az időnek
        cpu_log_time_str = "A CPU napló mentésének ideje: " + cpu_log_time

        # CPU napló fejléc sora
        cpu_header = "CPU     %user     %nice   %system   %iowait" \
                     "    %steal     %idle"

        # hozzáadjuk őket a visszatérési tömbhöz
        to_return.append(cpu_log_time_str)
        to_return.append("")
        to_return.append(cpu_header)

        # lekérdezzük a CPU magok számát
        cpu_count = multiprocessing.cpu_count()
        # kapcsolót csinálunk belőle a tail-nek
        cpu_count_switch = "-" + str(cpu_count)

        # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét
        cpu = subprocess.Popen(["sar", "-P", "ALL"], stdout=subprocess.PIPE)
        # kigrepeljük az Átlag szót tartalmazó sorokat
        grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=cpu.stdout,
            stdout=subprocess.PIPE)
        # kigrepeljük a LINUX RESTART sorokat
        grep_restart = subprocess.Popen(["grep", "-v", "-i", "linux"],
            stdin=grep.stdout, stdout=subprocess.PIPE)
        # kiírjatjuk az utolsó 9 sort
        tail = subprocess.Popen(["tail", cpu_count_switch],
            stdin=grep_restart.stdout, stdout=subprocess.PIPE)

        # az out tartalmazza a fenti parancs kimenetét
        out = tail.stdout.read()
        # mentés sortörés nélkül
        out = out.strip("\n")

        # lecseréljük az időt a semmire, hogy eltűnjön az első oszlop
        out_replace = out.replace(cpu_log_time, "")
        # listává alakítjuk a több szoros sztringet soronként
        out_list = out_replace.splitlines()

        # bejárjuk a tömböt és hozzáadjuk a to_return tömbhöz
        for item in out_list:
            # megszabadulunk a vezető szóközöktől, de így elcsúszik a tizedes
            # igazítás
            item = item.strip()
            # ha az item nem all, elé teszünk két szóközt
            if item.find("all") == -1:
                item = "  " + item

            to_return.append(item)

        # visszatérünk a tömbbel
        return to_return


def print_ram_log():
    """A RAM napló utolsó bejegyzését írja ki."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    ram = subprocess.Popen(["sar", "-r"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=ram.stdout,
        stdout=subprocess.PIPE)
    # kigrepeljük a Linuxot/LINUX RESTART-ot tartalmazó sorokat
    grep_linux = subprocess.Popen(["grep", "-v", "-i", "linux"],
        stdin=grep.stdout, stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep_linux.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # ha az out csak az első sort tartalmazza
    if len(out_list) <= 8:
        to_return.append("Még nincs mentett memória napló.")
        return to_return

    # ha van napló
    else:
        # a naplózás ideje az out nulladik eleme
        ram_log_time = out_list[0]
        # sztringet készítünk az időnek
        ram_log_time_str = "A RAM napló mentésének ideje: " + ram_log_time

        # a szabad memória KB-ban az első elem
        kbmemfree = out_list[1]
        # átváltjuk GB-ra
        gbmemfree = "%.2f" % (float(kbmemfree) / 1024 / 1024)
        # sztringet készítünk neki
        gbmemfree_str = "A szabad memória mérete GB-ban: " + \
                        str(gbmemfree).replace(".", ",")

        # a foglalt memória mérete KB-ban a második elem
        kbmemused = out_list[2]
        # átváltjuk GB-ra
        gbmemused = "%.2f" % (float(kbmemused) / 1024 / 1024)
        # sztringet készítünk neki
        gbmemused_str = "A foglalt memória mérete GB-ban: " + \
                        str(gbmemused).replace(".", ",")

        # a foglalt memória mérete százalékban a harmadik elem
        memused = out_list[3]
        # sztringet készítünk neki
        memused_str = "A foglalt memória mérete százalékban: " + memused

        # a pufferelt memória mérete KB-ban a negyedik elem
        kbbuffers = out_list[4]
        # átváltjuk GB-ra
        gbbuffers = "%.2f" % (float(kbbuffers) / 1024 / 1024)
        # sztringet készítünk neki
        gbbuffers_str = "A pufferelt memória mérete GB-ban: " + \
                        str(gbbuffers).replace(".", ",")

        # a gyorsítótárazott memória mérete KB-ban az ötödik elem
        kbcached = out_list[5]
        # átváltjuk GB-ra
        gbcached = "%.2f" % (float(kbcached) / 1024 / 1024)
        # sztringet készítünk neki
        gbcached_str = "A gyorsítótárazott memória mérete GB-ban: " + \
                       str(gbcached).replace(".", ",")

        # hozzáadjuk őket a visszatérési tömbhöz
        to_return.append(ram_log_time_str)
        to_return.append("")
        to_return.append(gbmemfree_str)
        to_return.append(gbmemused_str)
        to_return.append(memused_str)
        to_return.append(gbbuffers_str)
        to_return.append(gbcached_str)

        # visszatérünk a tömbbel
        return to_return


def print_swap_log():
    """A swap napló utolsó bejegyzését írja ki."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    swap = subprocess.Popen(["sar", "-S"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=swap.stdout,
        stdout=subprocess.PIPE)
    # kigrepeljük a Linuxot/LINUX RESTART-ot tartalmazó sorokat
    grep_linux = subprocess.Popen(["grep", "-v", "-i", "linux"],
        stdin=grep.stdout, stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep_linux.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # ha az out csak az első sort tartalmazza
    if len(out_list) < 6:
        to_return.append("Még nincs mentett swap napló.")
        return to_return

    # ha van napló
    else:
        # a naplózás ideje az out nulladik eleme
        swap_log_time = out_list[0]
        # sztringet készítünk az időnek
        swap_log_time_str = "A swap napló mentésének ideje: " + swap_log_time

        # a szabad swap KB-ban az első elem
        kbswpfree = out_list[1]
        # átváltjuk GB-ra
        gbswpfree = "%.2f" % (float(kbswpfree) / 1024 / 1024)
        # sztringet készítünk neki
        gbswpfree_str = "A szabad swap mérete GB-ban: " + \
                        str(gbswpfree).replace(".", ",")

        # a foglalt swap mérete KB-ban a második elem
        kbswpused = out_list[2]
        # átváltjuk GB-ra
        gbswpused = "%.2f" % (float(kbswpused) / 1024 / 1024)
        # sztringet készítünk neki
        gbswpused_str = "A foglalt swap mérete GB-ban: " + \
                        str(gbswpused).replace(".", ",")

        # a foglalt swap mérete százalékban a harmadik elem
        swpused = out_list[3]
        # sztringet készítünk neki
        swpused_str = "A foglalt swap mérete százalékban: " + swpused

        # a gyorsítótárazott swap mérete KB-ban a negyedik elem
        kbswpcad = out_list[4]
        # átváltjuk GB-ra
        gbswpcad = "%.2f" % (float(kbswpcad) / 1024 / 1024)
        # sztringet készítünk neki
        gbswpcad_str = "A gyorsítótárazott swap mérete GB-ban: " + \
                       str(gbswpcad).replace(".", ",")

        # a gyorsítótárazott swap mérete százalékban az ötödik elem
        swpcad = out_list[5]
        # sztringet készítünk neki
        swpcad_str = "A gyorsítótárazott swap mérete százalékban: " + swpcad

        # hozzáadjuk őket a visszatérési tömbhöz
        to_return.append(swap_log_time_str)
        to_return.append("")
        to_return.append(gbswpfree_str)
        to_return.append(gbswpused_str)
        to_return.append(swpused_str)
        to_return.append(gbswpcad_str)
        to_return.append(swpcad_str)

        # visszatérünk a tömbbel
        return to_return


def print_io_log():
    """Az I/O napló utolsó bejegyzését írja ki."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    io = subprocess.Popen(["sar", "-b"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=io.stdout,
        stdout=subprocess.PIPE)
    # kigrepeljük a Linuxot/LINUX RESTART-ot tartalmazó sorokat
    grep_linux = subprocess.Popen(["grep", "-v", "-i", "linux"],
        stdin=grep.stdout, stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep_linux.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # ha nincs napló
    if len(out_list) < 6:
        to_return.append("Még nincs mentett IO napló.")
        return to_return

    # ha van napló
    else:
        # a naplózás ideje az out nulladik eleme
        io_log_time = out_list[0]
        # sztringet készítünk az időnek
        io_log_time_str = "Az I/O napló mentésének ideje: " + io_log_time

        # az adatátvitelek száma másodpercenként az első elem
        tps = out_list[1]
        # sztringet készítünk neki
        tps_str = "Az adatátvitelek száma másodpercenként: " + tps

        # az olvasási kérelmek száma másodpercenként a második elem
        rtps = out_list[2]
        # sztringet készítünk neki
        rtps_str = "Az olvasási kérelmek száma másodpercenként: " + rtps

        # az írási kérelmek száma másodpercenként a harmadik elem
        wtps = out_list[3]
        # sztringet készítünk neki
        wtps_str = "Az írási kérelmek száma másodpercenként: " + wtps

        # az eszközről olvasott teljes adatmennyiség MB/s-ban az ötödik elem
        breads = out_list[4]
        # a breads block/secundumban van megadva, ahol a block 512 bájt
        # az értéket 2048-cal kell osztani, hogy MB/s-et kapjunk
        # ám float osztás előtt a vesszőt tizedespontra kell cserélni
        breads = breads.replace(",", ".")
        breadmbs = "%.2f" % (float(breads) / 2048)
        # sztringet készítünk neki
        breadmbs_str = "Az eszközről olvasott teljes adatmennyiség " \
                       "MB/s-ban: " + str(breadmbs).replace(".", ",")

        # az eszközre írt teljes adatmennyiség MB/s-ban az ötödik elem
        bwrtns = out_list[5]
        # a bwrtns block/secundumban van megadva, ahol a block 512 bájt
        # az értéket 2048-cal kell osztani, hogy MB/s-et kapjunk
        # ám float osztás előtt a vesszőt tizedespontra kell cserélni
        bwrtns = bwrtns.replace(",", ".")
        bwrtnmbs = "%.2f" % (float(bwrtns) / 2048)
        # sztringet készítünk neki
        bwrtnmbs_str = "Az eszközre írt teljes adatmennyiség MB/s-ban: " + \
            str(bwrtnmbs).replace(".", ",")

        # hozzáadjuk őket a visszatérési tömbhöz
        to_return.append(io_log_time_str)
        to_return.append("")
        to_return.append(tps_str)
        to_return.append(rtps_str)
        to_return.append(wtps_str)
        to_return.append(breadmbs_str)
        to_return.append(bwrtnmbs_str)

        # visszatérünk a tömbbel
        return to_return
