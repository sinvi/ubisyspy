#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A memória adatok kérdezi le.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import os
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def get_mem_info():
    """
    A memóriára és swapra vonatkozó információkat kérdezi le és írja ki.
    """

    meminfo = []  # tömb a visszatérési értéknek

    # memória méretnének lekérdezése
    mem_info = os.popen(
        "cat /proc/meminfo | grep MemTotal | awk {'print $2'}").read()

    # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
    mem_info = float(mem_info) / 1024
    if mem_info > 1023:
        mem_info = float(mem_info) / 1024
        # sztringbe mentjük és tömbhöz adjuk a memória méretét
        all_ram = "Összes memória: {mem_info} GB".format(
            mem_info="%.2f" % mem_info).replace(".", ",")
        meminfo.append(all_ram)
    else:
        # sztringbe mentjük és tömbhöz adjuk a memória méretét
        all_ram = "Összes memória: {mem_info} MB".format(
            mem_info="%.2f" % mem_info).replace(".", ",")
        meminfo.append(all_ram)

    # foglalt memória lekérdezése
    mem_used = os.popen(
        "cat /proc/meminfo | grep MemAvailable | awk {'print $2'}").read()
    # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
    mem_used = float(mem_used) / 1024
    if mem_used > 1023:
        mem_used = float(mem_used) / 1024
        # sztringbe mentjük és tömbhöz adjuk a foglalt memória méretét
        used_ram = "Foglalt memória: {mem_used} GB".format(
            mem_used="%.2f" % mem_used).replace(".", ",")
        meminfo.append(used_ram)
    else:
        # sztringbe mentjük és tömbhöz adjuk a foglalt memória méretét
        used_ram = "Foglalt memória: {mem_used} MB".format(
            mem_used="%.2f" % mem_used).replace(".", ",")
        meminfo.append(used_ram)

    # szabad memória lekérdezése
    mem_free = os.popen(
        "cat /proc/meminfo | grep MemFree | awk {'print $2'}").read()
    # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
    mem_free = float(mem_free) / 1024
    if mem_free > 1023:
        mem_free = float(mem_free) / 1024
        # sztringbe mentjük és tömbhöz adjuk a szabad memória méretét
        free_ram = "Szabad memória: {mem_free} GB".format(
            mem_free="%.2f" % mem_free).replace(".", ",")
        meminfo.append(free_ram)
    else:
        # sztringbe mentjük és tömbhöz adjuk a szabad memória méretét
        free_ram = "Szabad memória: {mem_free} MB".format(
            mem_free="%.2f" % mem_free).replace(".", ",")
        meminfo.append(free_ram)

    # összes swap lekérdezése
    swap_total = os.popen(
        "cat /proc/meminfo | grep SwapTotal | awk {'print $2'}").read()
    # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
    swap_total = float(swap_total) / 1024
    if swap_total > 1023:
        swap_total = float(swap_total) / 1024
        # sztringbe mentjük és tömbhöz adjuk az összes swap méretét
        total_swap = "Összes swap: {swap_total} GB".format(
            swap_total="%.2f" % swap_total).replace(".", ",")
        meminfo.append(total_swap)
    else:
        # sztringbe mentjük és tömbhöz adjuk az összes swap méretét
        total_swap = "Összes swap: {swap_total} MB".format(
            swap_total="%.2f" % swap_total).replace(".", ",")
        meminfo.append(total_swap)

    # szabad swap lekérdezése
    swap_free = os.popen(
        "cat /proc/meminfo | grep SwapFree | awk {'print $2'}").read()
    swap_total = os.popen(
        "cat /proc/meminfo | grep SwapTotal | awk {'print $2'}").read()
    if swap_total != swap_free:
        # kiszámítjuk a szabad swapot
        swap_used = int(swap_total) - int(swap_free)
        # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
        swap_used = float(swap_used) / 1024
        if swap_used > 1023:
            swap_used = float(swap_used) / 1024
            # sztringbe mentjük és tömbhöz adjuk a foglalt swap méretét
            used_swap = "Foglalt swap: {swap_used} GB".format(
                swap_used="%.2f" % swap_used).replace(".", ",")
            meminfo.append(used_swap)
        else:
            # sztringbe mentjük és tömbhöz adjuk a foglalt swap méretét
            used_swap = "Foglalt swap: {swap_used} MB".format(
                swap_used="%.2f" % swap_used).replace(".", ",")
            meminfo.append(used_swap)

        # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
        swap_free = float(swap_free) / 1024
        if swap_free > 1023:
            swap_free = float(swap_free) / 1024
            # sztringbe mentjük és tömbhöz adjuk a szabad swap méretét
            free_swap = "Szabad swap: {swap_free} GB".format(
                swap_free="%.2f" % swap_free).replace(".", ",")
            meminfo.append(free_swap)
        else:
            # sztringbe mentjük és tömbhöz adjuk a szabad swap méretét
            free_swap = "Szabad swap: {swap_free} MB".format(
                swap_free="%.2f" % swap_free).replace(".", ",")
            meminfo.append(free_swap)

    else:
        swap_used = int(swap_total) - int(swap_free)
        # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
        swap_used = float(swap_used) / 1024
        if swap_used > 1023:
            swap_used = float(swap_used) / 1024
            # sztringbe mentjük és tömbhöz adjuk a foglalt swap méretét
            used_swap = "Használt swap: {swap_used} GB".format(
                swap_used="%.2f" % swap_used).replace(".", ",")
            meminfo.append(used_swap)
        else:
            # sztringbe mentjük és tömbhöz adjuk a foglalt swap méretét
            used_swap = "Használt swap: {swap_used} MB".format(
                swap_used="%.2f" % swap_used).replace(".", ",")
            meminfo.append(used_swap)

        # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
        swap_free = float(swap_free) / 1024
        if swap_free > 1023:
            swap_free = float(swap_free) / 1024
            # sztringbe mentjük és tömbhöz adjuk a szabad swap méretét
            free_swap = "Szabad swap: {swap_free} GB".format(
                swap_free="%.2f" % swap_free).replace(".", ",")
            meminfo.append(free_swap)
        else:
            # sztringbe mentjük és tömbhöz adjuk a szabad swap méretét
            free_swap = "Szabad swap: {swap_free} MB".format(
                swap_free="%.2f" % swap_free).replace(".", ",")
            meminfo.append(free_swap)

    cached = os.popen(
        "cat /proc/meminfo | grep '^Cached' | awk {'print $2'}").read()
    # osztásokat végztünk a MB-ba és GB-ba történő átváltáshoz
    cached = float(cached) / 1024
    if cached > 1023:
        cached = float(cached) / 1024
        # sztringbe mentjük és tömbhöz adjuk a gyorsítótár méretét
        cached_ram = "Gyorsítótárban: {cached} GB".format(
            cached="%.2f" % cached).replace(".", ",")
        meminfo.append(cached_ram)
    else:
        # sztringbe mentjük és tömbhöz adjuk a gyorsítótár méretét
        cached_ram = "Gyorsítótárban: {cached} MB".format(
            cached="%.2f" % cached).replace(".", ",")
        meminfo.append(cached_ram)

    return meminfo  # visszatérünk az értékeket tartalmazó tömbbel
