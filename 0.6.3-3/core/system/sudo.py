#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A sudo és root ellenőrzéshez szükséges függvényeket tartalmazza.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import getpass
import subprocess
import os
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def sudo_test(passwd):
    """Ellenőrzi a sudo jelszót.

    Paramétere a passwd a felhasználó jelszava."""

    # sudo jelszó ellenőrzése
    sudo = subprocess.Popen(["sudo", "-S", "echo", "teszt"],
        stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    sudo_communicate = sudo.communicate(passwd + '\n')
    sudo_return = sudo.returncode

    # felülírjuk a felhasználó jelszavát
    passwd = os.urandom(len(passwd))
    
    # ha a sudo hitelesítés sikeres
    if sudo_return == 0:
        # igazzal térünk vissza
        return True
    
    # ha nem
    else:
        # akkor hamissal
        return False


def check_root():
    """Ellenőrzi, hogy a felhasználó root-e."""

    # lekérdezzük a user nevét
    user = getpass.getuser()

    # ha az aktuális felhasználó root, nem kell sudo a parancsokhoz
    if user == "root":
        # visszatérünk nullával
        return 0
    
    # ha a felhasználó nem root, ellenőrizzük a sudo jogát
    else:
        # ellenőrizzük a sudo jogot
        out = subprocess.Popen(["sudo", "-v", "-n"], stdin=subprocess.PIPE,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1]
        
        # tartalmazza a sorry szót, nincs sudo joga
        if out.find("Sorry, user") != -1:
            # kettővel térünk vissza
            return 2
        
        # különben
        else:
            # van sudo joga
            return 1
