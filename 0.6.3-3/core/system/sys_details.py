#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A rendszeradatok lekérdezését végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import platform
import os
import time
import subprocess
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def ask_sys_details():
    """
    Lekérdezi a rendszeradatokat. Debug funkció.
    """

    # tömb elemeiként tároljuk a platform modul által biztosított adatokat
    profile = [platform.architecture(),  # gép architektúra
               platform.dist(),  # disztribúció
               platform.machine(),  # rendszer architektúra
               platform.node(),  # gép neve
               platform.platform(),  # kernel
               platform.processor(),  # processzor név
               platform.system(),  # operációs rendszer típusa
               platform.uname(),  # teljes kernel verzió
               platform.version(),  # rendszer verzió]
               ]

    # bejárjuk a tömböt
    for item in profile:
        # és kiírjuk az elemeit
        print item


def get_machine_name():
    """
    A számítógép nevét kérdezi le és írja ki.
    """

    # lekérdezzük a gép nevét
    machine_name = platform.node()
    machine_name_ext = "A gép neve: " + machine_name

    # visszatérünk az értékkel
    return machine_name_ext


def get_machine_type():
    """
    Lekérdezi és kiírja a számítógép típusát.
    """

    # lekérdezzük a gép típusát és gyártóját
    machine_type = subprocess.check_output(
        ["cat", "/sys/devices/virtual/dmi/id/product_name"]).strip()
    sys_vendor = subprocess.check_output(
        ["cat", "/sys/devices/virtual/dmi/id/sys_vendor"]).strip()

    # ha üres sztringek
    if machine_type == "" and sys_vendor == "":
        machine_type_ext = "A gép típusa és gyártója ismeretlen"

    # ha a típus üres, a gyártó nem
    elif machine_type == "" and sys_vendor != "":
        machine_type_ext = "A gép típusa ismeretlen, gyártója: " + sys_vendor

    # ha a típus nem üres, a gyártó igen
    elif machine_type != "" and sys_vendor == "":
        machine_type_ext = "A gép gyártója ismeretlen, típusa: " + machine_type

    # különben
    else:
        machine_type_ext = "A gép típusa: " + sys_vendor + " " + machine_type

    # visszatérünk az értékkel
    return machine_type_ext


def get_architecture():
    """
    A számítógép architektúrájának lekérdezése és kiírása.
    """

    # lekérdezzük a gép architektúráját
    architecture = platform.machine()
    architecture_ext = "Architektúra: " + architecture

    # visszatérünk az értékkel
    return architecture_ext


def get_distro_name():
    """
    Lekérdezi és kiírja a disztribúció nevét.
    """

    # lekérdezzük a disztribúciót
    distro_name = platform.dist()[0]
    distro_name_ext = "Disztribúció neve: " + distro_name

    # visszatérünk az értékkel
    return distro_name_ext


def get_distro_code():
    """
    Lekérdezi és kiírja a disztribúció kódját, amennyiben ez támogatott.
    """

    # lekérdezzük a disztribúció verziószámát
    distro_code = platform.dist()[1]

    # ha nem üres sztring
    if distro_code != "":
        distro_code_ext = "Disztribúció verziószáma: " + distro_code

    # ha üres sztring
    else:
        distro_code_ext = "A disztribúcióban a verziószám lekérdezés " \
                          "nem elérhető."

    # visszatérünk az értékkel
    return distro_code_ext


def get_distro_codename():
    """
    Lekérdezi és kiírja a disztribúció kódnevét, amennyiben ez támogatott.
    """

    # lekérdezzük a disztribúció kódnevét
    distro_codename = platform.dist()[2]

    # ha nem üres sztring
    if distro_codename != "":
        distro_codename_ext = "Disztribúció kódneve: " + distro_codename

    # ha üres sztring
    else:
        distro_codename_ext = "A disztribúcióban a kódnév lekérdezés " \
                              "nem elérhető."

    # visszatérünk az értékkel
    return distro_codename_ext


def get_uptime():
    """
    A számítógép futási idejét kérdezi le és írja ki.
    """

    # uptime =
    # "23:59:40 up 5 days, 34 min, 7 users, load average: 9,62, 9, 11, 8,82"
    # "00:34:40 up 5 days, 1:09, 7 users, load average: 10,83, 10, 34, 10,04"
    # "12:04:47 up 28 days,  1:42,  1 user,  load average: 6.03, 6.04, 6.03"
    # "18:42:36 up 1 day, 13 min, 3 user, load average: 0.12, 0.10, 0.02"
    # "18:42:36 up 1 day, 4:03, 5 user, load average: 0,12, 0,10, 0,02"

    # lekérdezzük a futási időt
    uptime = os.popen("uptime").read()
    # megkeressük az uptime kimenetében az up szót, mert utána áll a futási idő
    up = uptime.find("up")
    # megkeressük az uptime kimenetében a user szót (illik a users-re is),
    # mert előtte áll a futási idő
    users = uptime.find("user")
    uptime = uptime[up + 3:users]
    uptime = uptime[::-1]  # megfordítjuk a sztringet
    #  megkeressük a kimenetben az első vesszőt, mert addig tart a futási idő
    comma = uptime.find(",")
    #  vessző egy karakter, ezért azt hozzáadjuk és onnan írjuk ki
    uptime = uptime[comma + 1:]
    uptime = uptime[::-1]  # visszafordítjuk a sztringet

    # ha a kimenetben "min" van...
    if uptime.find("min"):
        uptime = uptime.replace("min", "perc")  # ... írjunk helyette percet

    # ha a kimenetben "day" van (ez illik a days-re is)...
    if uptime.find("days"):
        uptime = uptime.replace("days", "nap")  # ... írjunk helyette napot

    if uptime.find("day"):
        uptime = uptime.replace("day", "nap")  # ... írjunk helyette napot

    # töröljük a sor eleji szóközt
    uptime_ext = "Futási idő [nap óra:perc]: " + uptime.lstrip()

    # visszatérünk az értékkel
    return uptime_ext


def get_cpu_type():
    """
    A CPU típusát kérdezi le és írja ki.
    """

    # lekérdezzük a CPU típusát
    cpu_type = os.popen('cat /proc/cpuinfo | grep "model name" | '
                        'uniq | cut -f3- -d" "').read().replace("  ", " ").\
        rstrip("\n")
    # változóba mentjük
    cpu_type_ext = "A CPU típusa: " + cpu_type

    # visszatérünk az értékkel
    return cpu_type_ext


def get_cpu_core():
    """
    A CPU magjainak számát (összes mag!) kérdezi le és írja ki.
    """

    # lekérdezzük a CPU magok számát
    cpu_count = os.popen("cat /proc/cpuinfo | grep 'model name' | "
                         "uniq -c | awk {'print $1'}").read().rstrip("\n")
    cpu_count_ext = "CPU magok száma: " + cpu_count

    # visszatérünk az értékkel
    return cpu_count_ext


def get_load_avg():
    """
    Lekérdezi a rendszer átlagos terhelését.
    """

    #  23:59:40 up 5 days, 34 min, 7 users, load average: 9.62, 9.11, 8.82

    # lekérdezzük a rendszerterhelést
    load_avg = os.popen("uptime").read()
    # megkeressük az uptime kimenetében a ": "- t, mert utána áll a load
    load = load_avg.find(": ")
    # az ": " 2 karakter, ezért kell kettőt hozzáadni. Onnantól írjuk ki.
    load_avg = load_avg[load + 2:].replace(", ", "; ").replace(".", ",").strip()

    load_avg_ext = "Átlagos terhelés az elmúlt 1, 5 és 15 percben: " + load_avg

    # visszatérünk az értékkel
    return load_avg_ext


def get_kernel():
    """
    Lekérdezi a kernel verzióját.
    """

    # lekérdezzük a kernel verzióját
    kernel = os.popen("uname -r -i -o").read().rstrip("\n")
    kernel_ext = "Kernel verzió: " + kernel

    # visszatérünk az értékkel
    return kernel_ext


def results():
    """
    Meghívja az összes lekérdező függvényt és tömbben visszaadja a kimenetüket.
    """

    # listába mentjük a fenti függvények visszatérési értékeit
    result_list = [get_machine_name(),
        get_machine_type(),
        get_distro_name(),
        get_distro_code(),
        get_distro_codename(),
        get_cpu_type(),
        get_cpu_core(),
        get_architecture(),
        get_kernel(),
        get_uptime(),
        get_load_avg()]

    # visszatérünk az értékekkel
    return result_list


def write_results():
    """
    Fájlba írjuk az eredményeket.
    """

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/rendszeradatok/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)
        os.system("rm * 2> /dev/null")  # töröljük a régebbi fájlokat

    # előállítjuk a mentési fájl nevét és útvonalát
    copy_sys_details = into_mkdir + "sys_details_" + time_stamp + ".txt"

    # megnyitjuk a fájlt
    with open(copy_sys_details, "w") as f:
        # soronként kiírjuk az eredményeket a fájlba
        for line in results():
            line += "\n"
            f.write(line)

    # visszatérünk a fájl elérési útjával
    return copy_sys_details


def get_partitions():
    """A / és a /home partíciók adatait kérdezi le."""

    to_return = []  # tömb a visszatérési értékeknek

    # felvezető mondat
    to_return.append("A rendszerpartíció adatai:")

    # lekérdezzük a / méretét
    root_size = os.popen('df -h 2>/dev/null | grep -w "/" | '
                         'tr -s " " "#" | cut -f2 -d"#"').read().rstrip("\n")
    # lecseréljük a mértékeegységek jelölését
    root_size = root_size.replace("K", " KB").replace("M", " MB").replace(
        "G", " GB")
    # lekérdezzük a /-en foglalt lemezterület nagyságát
    root_used = os.popen('df -h 2>/dev/null | grep -w "/" | '
                         'tr -s " " "#" | cut -f3 -d"#"').read().rstrip("\n")
    # lecseréljük a mértékeegységek jelölését
    root_used = root_used.replace("K", " KB").replace("M", " MB").replace(
        "G", " GB")
    # lekérdezzük a /-en elérhető lemezterület nagyságát
    root_avail = os.popen('df -h 2>/dev/null | grep -w "/" | '
                          'tr -s " " "#" | cut -f4 -d"#"').read().rstrip("\n")
    # lecseréljük a mértékeegységek jelölését
    root_avail = root_avail.replace("K", " KB").replace("M", " MB").replace(
        "G", " GB")
    # a /-en foglalt lemezterület nagysága százalékosan
    root_used_percent = os.popen('df -h 2>/dev/null | grep -w "/" | '
                                 'tr -s " " "#" | '
                                 'cut -f5 -d"#"').read().rstrip("\n")

    # sztringek összeállítása
    root_partition = "A / partíció mérete: " + root_size
    root = "Ebből " + root_used + " (" + root_used_percent + \
           ") használt és " + root_avail + " felhasználható."

    # sztringek hozzaádása a tömbhöz
    to_return.append(root_partition)
    to_return.append(root)
    # elválasztó új sor a /home adatai előtt
    to_return.append("")

    # lekérdezzük a /home partíció méretét
    size_home_partition = os.popen('df -h 2>/dev/null | grep -w '
                                   '"/home"').read().rstrip("\n")

    # felvezető mondat
    to_return.append("A /home partíció adatai:")

    # ha nincs
    if size_home_partition == "":
        # visszadjuk, hogy nincs /home partíció
        message = "Nincs külön /home partíció."
        to_return.append(message)

    # ha van külön /home
    else:
        # lekérdezzük a /home méretét
        home_size = os.popen('df -h 2>/dev/null | grep -w "/home" | '
                             'grep "/dev/" | tr -s " " "#" | '
                             'cut -f2 -d"#"').read().rstrip("\n")
        # lecseréljük a mértékeegységek jelölését
        home_size = home_size.replace("K", " KB").replace("M",
            " MB").replace("G", " GB")
        # lekérdezzük a /home-on foglalt lemezterület nagyságát
        home_used = os.popen('df -h 2>/dev/null | grep -w "/home" | '
                             'grep "/dev/" | tr -s " " "#" | '
                             'cut -f3 -d"#"').read().rstrip("\n")
        # lecseréljük a mértékeegységek jelölését
        home_used = home_used.replace("K", " KB").replace("M",
            " MB").replace("G", " GB")
        # lekérdezzük a /home-on elérhető lemezterület nagyságát
        home_avail = os.popen('df -h 2>/dev/null | grep -w "/home" | '
                              'grep "/dev/" | tr -s " " "#" | '
                              'cut -f4 -d"#"').read().rstrip("\n")
        # lecseréljük a mértékeegységek jelölését
        home_avail = home_avail.replace("K", " KB").replace("M",
            " MB").replace("G", " GB")
        # a /home-on foglalt lemezterület nagysága százalékosan
        home_used_percent = os.popen('df -h 2>/dev/null | grep -w "/home" | '
                                     'grep "/dev/" | tr -s " " "#" | '
                                     'cut -f5 -d"#"').read().rstrip("\n")
        home_partition = "A /home partíció mérete: " + home_size
        home = "Ebből " + home_used + " (" + home_used_percent + \
               ") használt és " + home_avail + " felhasználható."
        to_return.append(home_partition)
        to_return.append(home)

    # visszatérünk a tömbbel
    return to_return


def get_default_applications():
    """Az alapértelmezett alkalmazásokat írja ki."""

    to_return = []  # tömb a visszatérési értékeknek

    # az alapértelmezett fájlkezelő lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "inode/directory"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        file_manager_string = "Nincs alapértelmezett fájlkezelő."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot, addig tart a program neve
        desktop = temp.find(".desktop")
        # nagy kezdőbetűsítjük a program nevét
        file_manager = temp[:desktop].title()

        # ha tartalmazza a "-Folder-Handler", kivágjuk belőle
        if file_manager.find("-Folder-Handler") != -1:
            folder = file_manager.find("-Folder-Handler")
            file_manager = file_manager[:folder]

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if file_manager.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            file_manager = file_manager[gnome + gnome_len:]

        # összeállítjuk a sztringet
        file_manager_string = "Az alapértelmezett fájlkezelő: " + file_manager

    # hozzáadjuk a tömbhöz
    to_return.append(file_manager_string)


    # az alapértelmezett böngésző lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "text/html"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        browser_string = "Nincs alapértelmezett webböngésző."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        browser = temp[:desktop].title()

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if browser.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            browser = browser[gnome + gnome_len:]

        # összeállítjuk a sztringet
        browser_string = "Az alapértelmezett webböngésző: " + browser

    # hozzáadjuk a tömbhöz
    to_return.append(browser_string)


    # az alapértelmezett levelező lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "message/rfc822"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        mail_client_string = "Nincs alapértelmezett levelezőprogram."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        mail_client = temp[:desktop].title()

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if mail_client.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            mail_client = mail_client[gnome + gnome_len:]

        # összeállítjuk a sztringet
        mail_client_string = "Az alapértelmezett levelezőprogram: " + \
                             mail_client

    # hozzáadjuk a tömbhöz
    to_return.append(mail_client_string)


    # az alapértelmezett szövegszerkesztő (text editor) lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "text/plain"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        text_editor_string = "Nincs alapértelmezett szövegszerkesztő."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        text_editor = temp[:desktop].title()

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if text_editor.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            text_editor = text_editor[gnome + gnome_len:]

        # összeállítjuk a sztringet
        text_editor_string = "Az alapértelmezett szövegszerkesztő: " + \
                             text_editor

    # hozzáadjuk a tömbhöz
    to_return.append(text_editor_string)


    # az alapértelmezett dokumentumszerkesztő (word processor) lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "application/vnd.oasis.opendocument.text"], stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        word_processor_string = "Nincs alapértelmezett dokumentumszerkesztő."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        word_processor = temp[:desktop].title()

        # ha a sztring "Libreoffice Writer", átírjuk
        if word_processor == "Libreoffice-Writer":
            word_processor = "LibreOffice Writer"

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if word_processor.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            word_processor = word_processor[gnome + gnome_len:]

        # összeállítjuk a sztringet
        word_processor_string = "Az alapértelmezett dokumentumszerkesztő: " +\
                                word_processor

    # hozzáadjuk a tömbhöz
    to_return.append(word_processor_string)


    # az alapértelmezett zenelejátsztó lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "audio/mpeg"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        music_player_string = "Nincs alapértelmezett zenelejátsztó."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        music_player = temp[:desktop].title()

        # ha az értéke "Vlc", cseréljük
        if music_player == "Vlc":
            music_player = "VLC"

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if music_player.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            music_player = music_player[gnome + gnome_len:]

        # sztring összeállítása
        music_player_string = "Az alapértelmezett zenelejátsztó: " + \
                              music_player

    # hozzáadjuk a tömbhöz
    to_return.append(music_player_string)


    # az alapértelmezett videólejátsztó lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "audio/mp4"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        media_player_string = "Nincs alapértelmezett videólejátsztó."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        media_player = temp[:desktop].title()

        # ha az értéke "Vlc", cseréljük
        if media_player == "Vlc":
            media_player = "VLC"

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if media_player.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            media_player = media_player[gnome + gnome_len:]

        # sztring összeállítása
        media_player_string = "Az alapértelmezett videólejátsztó: " + \
                              media_player

    # hozzáadjuk a tömbhöz
    to_return.append(media_player_string)


    # az alapértelmezett képnézegető lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "image/jpeg"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        image_viewer_string = "Nincs alapértelmezett képmegjelenítő."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        image_viewer = temp[:desktop].title()

        # ha a sztring "Eom", átírjuk
        if image_viewer == "Eom":
            image_viewer = "Eye of MATE"

        # ha a sztring "Eog", átírjuk
        elif image_viewer == "Eog":
            image_viewer = "Eye of Gnome"

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if image_viewer.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            image_viewer = image_viewer[gnome + gnome_len:]

        # sztring összeállítása
        image_viewer_string = "Az alapértelmezett képmegjelenítő: " + \
                              image_viewer

    # hozzáadjuk a tömbhöz
    to_return.append(image_viewer_string)


    # az alapértelmezett PDF olvasó lekérdezése
    command = subprocess.Popen(["xdg-mime", "query", "default",
        "application/pdf"], stdin=subprocess.PIPE, stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    communicate = command.communicate()

    # levágjuk a communicate második elemét
    temp = communicate[0]

    # ha üres sztring, nincs alapértelmezett program
    if len(temp) == 0:
        pdf_reader_string = "Nincs alapértelmezett dokumentummegjelenítő."

    # ha nem üres sztring
    else:
        # megkeressük benne a ".desktop"-ot
        desktop = temp.find(".desktop")
        # addig tart a program neve, azt nagy kezdőbetűsítjük
        pdf_reader = temp[:desktop].title()

        # ha tartalmazza az "Org.Gnome."-ot, kivágjuk
        if pdf_reader.find("Org.Gnome.") != -1:
            gnome = temp.find("Org.Gnome.")
            gnome_len = len("Org.Gnome.") + 1
            pdf_reader = pdf_reader[gnome + gnome_len:]

        # sztring összeállítása
        pdf_reader_string = "Az alapértelmezett dokumentummegjelenítő: " +\
                            pdf_reader

    # hozzáadjuk a tömbhöz
    to_return.append(pdf_reader_string)

    # visszatérünk a tömbbel
    return to_return
