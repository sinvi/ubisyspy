#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A dmesg fájlba mentését végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import subprocess
import locale
import os
import time

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def dmesg():
    """Fájlba menti a dmesg parancs kimenetét."""

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/dmesg/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk és belépünk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)
        # töröljük a régebbi fájlokat
        os.system("rm * 2> /dev/null")

    # előállítjuk a mentési fájl nevét és útvonalát
    dmesg_copy = into_mkdir + "dmesg_" + time_stamp + ".txt"

    # lefuttatjuk a dmesget
    dmesg_log = subprocess.check_output(["dmesg"])
    # tömbe daraboljuk soronként
    dmesg_list = dmesg_log[:-1].splitlines()

    # megnyitjuk a fájlt
    with open(dmesg_copy, "w") as f:
        # soronként kiírjuk a dmesg_list-et a fájlba
        for line in dmesg_list:
            line += "\n"
            f.write(line)

    return dmesg_copy
