#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A napló bejegyzéseket olvassa ki.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
import os
import sqlite3 as s3
import subprocess
import sys
import time

# ugyanebből a könyvtárból importáljuk a log_db-t az adatbázis másolásához
import log_db as ldb


locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def get_ram_log():
    """A memória naplót olvassa be és dolgozza fel."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    ram = subprocess.Popen(["sar", "-r"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=ram.stdout,
        stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # a naplózás ideje az out nulladik eleme
    ram_log_time = out_list[0]

    # a szabad memória KB-ban az első elem
    kbmemfree = out_list[1]

    # a foglalt memória mérete KB-ban a második elem
    kbmemused = out_list[2]

    # a pufferelt memória mérete KB-ban a negyedik elem
    kbbuffers = out_list[4]

    # a gyorsítótárazott memória mérete KB-ban az ötödik elem
    kbcached = out_list[5]

    # hozzáadjuk őket a visszatérési tömbhöz
    to_return.append(ram_log_time)
    to_return.append(kbmemfree)
    to_return.append(kbmemused)
    to_return.append(kbbuffers)
    to_return.append(kbcached)

    # visszatérünk a tömbbel
    return to_return


def get_cpu_log():
    """A CPU naplót olvassa be és dolgozza fel."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    cpu = subprocess.Popen(["sar", "-u"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=cpu.stdout,
        stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó  sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # a naplózás ideje az out első eleme
    cpu_log_time = out_list[0]

    # a %user a harmadik eleme
    user = out_list[2]

    # a %nice a negyedik eleme
    nice = out_list[3]

    # a %system a ötödik eleme
    system = out_list[4]

    # az %iowait a hatodik eleme
    io = out_list[5]

    # az %idle a nyolcadik eleme
    idle = out_list[7]

    # hozzáadjuk őket a visszatérési tömbhöz
    to_return.append(cpu_log_time)
    to_return.append(user)
    to_return.append(nice)
    to_return.append(system)
    to_return.append(io)
    to_return.append(idle)

    # visszatérünk a tömbbel
    return to_return


def get_swap_log():
    """A swap naplót olvassa be és dolgozza fel."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    swap = subprocess.Popen(["sar", "-S"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=swap.stdout,
        stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # a naplózás ideje az out nulladik eleme
    swap_log_time = out_list[0]

    # a szabad swap KB-ban az első elem
    kbswpfree = out_list[1]

    # a foglalt swap mérete KB-ban a második elem
    kbswpused = out_list[2]

    # a gyorsítótárazott swap mérete KB-ban a negyedik elem
    kbswpcad = out_list[4]

    # hozzáadjuk őket a visszatérési tömbhöz
    to_return.append(swap_log_time)
    to_return.append(kbswpfree)
    to_return.append(kbswpused)
    to_return.append(kbswpcad)

    # visszatérünk a tömbbel
    return to_return


def get_io_log():
    """Az I/O naplót olvassa be és dolgozza fel."""

    to_return = []  # a visszatérési értékeknek

    # végrehajtjuk a paraméterül kapott parancsot, és elkapjuk a kimenetét,
    # hogy kiszedjük a dátumot
    io = subprocess.Popen(["sar", "-b"], stdout=subprocess.PIPE)
    # kigrepeljük az Átlag szót tartalmazó sorokat
    grep = subprocess.Popen(["grep", "-v", "Átlag"], stdin=io.stdout,
        stdout=subprocess.PIPE)
    # kiírjatjuk az utolsó sort
    tail = subprocess.Popen(["tail", "-1"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # az out tartalmazza a fenti parancs kimenetét
    out = tail.stdout.read()
    # mentés sortörés nélkül
    out = out.strip("\n")

    # feldaraboljuk az out-ot szavanként
    out_list = out.split()

    # a naplózás ideje az out nulladik eleme
    io_log_time = out_list[0]

    # az adatátvitelek száma másodpercenként az első elem
    tps = out_list[1]

    # az olvasási kérelmek száma másodpercenként a második elem
    rtps = out_list[2]

    # az írási kérelmek száma másodpercenként a harmadik elem
    wtps = out_list[3]

    # az eszközről olvasott teljes adatmennyiség block/s-ban az ötödik elem
    breads = out_list[4]

    # az eszközre írt teljes adatmennyiség block/s-ban az ötödik elem
    bwrtns = out_list[5]

    # hozzáadjuk őket a visszatérési tömbhöz
    to_return.append(io_log_time)
    to_return.append(tps)
    to_return.append(rtps)
    to_return.append(wtps)
    to_return.append(breads)
    to_return.append(bwrtns)

    # visszatérünk a tömbbel
    return to_return


def insert_logs_to_db():
    """A RAM, CPU, swap és I/O napló bejegyzéseket adja hozzá az
    adatbázishoz."""

    # ellenőrizzük, hogy létezik-e az adatbázis
    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az adatbázis elérési útjának sztringjét
    db = user_dir + "/.ubisyspy/grafikonok/naplozasi-adatok.sqlite"

    # ha nem létezik
    if not os.path.exists(db):
        # meghívjuk a függvényt létrehozni
        # ha nem nullával tért vissza, nem sikerült átmásolni
        if not ldb.create_user_log_db() == 0:
            # kettővel térünk vissza, hogy jelezzük a másolási hibát
            sys.exit(2)

    # ha létezik
    else:
        # kapcsolódunk az adatbázishoz
        log_db = s3.connect(db)
        cursor = log_db.cursor()

        # az automatikusan kezelt elsődleges kulcs ID-nak NULL/None értéket
        # kell visszaadni
        null = None

        # a napló készítésének napja
        log_date = time.strftime('%Y-%m-%d')

        # memória napló adatok
        ram = get_ram_log()
        log_time = str(ram[0])
        kbmemfree = int(ram[1])
        kbmemused = int(ram[2])
        kbbuffers = int(ram[3])
        kbcached = int(ram[4])

        # swap napló adatok
        swap = get_swap_log()
        kbswpfree = int(swap[1])
        kbswpused = int(swap[2])
        kbswpcad = int(swap[3])

        # I/O napló adatok
        io = get_io_log()
        tps = float(io[1].replace(",", "."))
        rtps = float(io[2].replace(",", "."))
        wtps = float(io[3].replace(",", "."))
        breads = float(io[4].replace(",", "."))
        bwrtns = float(io[5].replace(",", "."))

        # CPU napló adatok
        cpu = get_cpu_log()
        user = float(cpu[1].replace(",", "."))
        nice = float(cpu[2].replace(",", "."))
        system = float(cpu[3].replace(",", "."))
        io = float(cpu[4].replace(",", "."))
        idle = float(cpu[5].replace(",", "."))

        # adatok kiírása az adatbázisba
        cursor.execute('''INSERT INTO logs VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,
        ?,?,?,?,?,?)''', (null, log_date, log_time, kbmemfree, kbmemused,
        kbbuffers, kbcached, kbswpfree, kbswpused, kbswpcad, tps, rtps, wtps,
        breads, bwrtns, user, nice, system, io, idle))
        log_db.commit()
        # bezárjuk az adatbázist
        log_db.close()


if __name__ == '__main__':
    insert_logs_to_db()
