#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A naplók üres adatbázisát másolja át a felhasználóhoz.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import shutil
import locale
import os

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def create_user_log_db():
    """Az üres adatbázist a felhasználó UbiSysPy könyvtárába másolja."""

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/grafikonok/"

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)

    # előállítjuk a mentési fájl nevét és útvonalát
    backup_path = into_mkdir + "naplozasi-adatok.sqlite"

    # az aktuális script könyvtárának lekérdezése
    current_dir = os.path.dirname(os.path.abspath(__file__))
    # a script könyvtárának szülőkönyvtára
    original_path = current_dir + "/empty_db.sqlite"

    # átmásoljuk a felhasználó könyvtárába az üres adatbázist
    try:
        shutil.copy(original_path, backup_path)

    # ha IO hibát kapunk, nem sikerült
    except IOError:
        # hárommal térünk vissza, hogy jelezzük az IO hibát
        return 3

    # ha shutil hibát kapunk, szintén nem sikerült
    except shutil.Error:
        # néggyel térünk vissza, hogy jelezzük a shutil hibát
        return 4

    # ha OSError-t kapunk, valószínűleg létezik a fájl
    except OSError:
        # öttel térünk vissza, hogy jelezzük az OS hibát
        return 5

    # különben sikerült
    else:
        return 0
