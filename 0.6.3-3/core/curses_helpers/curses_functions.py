#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület kisegítő függvényei.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import curses
import locale
import subprocess
import os

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def get_param(fix_list):
    """
    Definiáljuk a get_param függvényt,
    ami a felhasználótól történő adatbekérést valósítja meg.

    Egyetlen paramétere, a fix_list egy három elemű tömb, amely tartalmazza
    a megjelenítendő menüpont nevét,
    a felhasználótól várt adat típusát és egy példát az elvárt bemenetre.
    """

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.border(0)  # margók beállítása
    # második színpár definíció
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)
    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, fix_list[0], curses.A_BOLD | curses.color_pair(2))
    # kiírjuk a szöveget
    screen.addstr(5, 4, fix_list[1])
    # kiírjuk a szöveget (bekértés szövege)
    screen.addstr(6, 4, fix_list[2])
    # kiírjuk a szöveget (vonal)
    screen.addstr(10, 10, "__________________________________________________")
    curses.echo()  # beírt karakterek megjelenítésének bekapcsolása
    curses.nocbreak()  # enter nélküli reagálás bekapcsolása
    screen.keypad(0)  # billentyűzet engedélyezése
    input_string = screen.getstr(10, 10, 100)  # bekérjük a sztringet
    curses.noecho()  # beírt karakterek megjelenítésének kikapcsolása
    curses.cbreak()  # enter nélküli reagálás kikapcsolása
    screen.keypad(1)  # billentyűzet kikapcsolása

    return input_string  # visszatérünk a bemenettel


def get_param_with_list(menu, prompt_list):
    """
    Definiáljuk a get_param_with_list függvényt,
    ami a felhasználótól történő adatbekérést valósítja meg.

    Első, menu paramétere a menüpont neve.
    Második paramétere, a prompt_list egy tömb, amely tartalmazza a
    megjelenítendő menüpont nevét és egyéb megjelenítendő információkat.
    A tömb utolsó eleme a bekérés szövege.
    Ezt követően kerül megjelenítésre a bekérő vonal.
    """

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.border(0)  # margók beállítása
    # második színpár definíció
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)
    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu, curses.A_BOLD | curses.color_pair(2))

    line = 5  # az ötödik sortól kezdjük kiírni
    # bejáruk a paramétertömböt
    for i in prompt_list:
        screen.addstr(line, 4, i)  # a line-adik sorba írjuk
        screen.refresh()  # képernyő frissítése
        # növeljük a line értékét, hogy a következő sorba ugorjunk
        line += 1

    line += 1  # hozzáadunk még egy sort
    # kiírjuk a szöveget (vonal)
    screen.addstr(line, 10, "________________________________________________")
    curses.echo()  # beírt karakterek megjelenítésének bekapcsolása
    curses.nocbreak()  # enter nélküli reagálás bekapcsolása
    screen.keypad(0)  # billentyűzet engedélyezése
    input_string = screen.getstr(line, 10, 100)  # bekérjük a sztringet
    curses.noecho()  # beírt karakterek megjelenítésének kikapcsolása
    curses.cbreak()  # enter nélküli reagálás kikapcsolása
    screen.keypad(1)  # billentyűzet kikapcsolása

    return input_string  # visszatérünk a bemenettel


def get_pass(menu_str, descr, prompt_str):
    """
    Definiáljuk a get_pass függvényt,
    ami a felhasználótól történő jelszóbekérést valósítja meg.

    Első paramétere, a menu_str egy több, amely tartalmazza a
    megjelenítendő menüpont nevét,a felhasználótól várt adat típusát
    és egy példát az elvárt bemenetre.
    Második paramétere, a descr, a bekérés magyarázata.
    Harmadik paramétere, a prompt_str a bekérés szövege-
    """

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.border(0)  # margók beállítása
    # második színpár definíció
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)
    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str, curses.A_BOLD | curses.color_pair(2))
    # kiírjuk a szöveget (magyarázat)
    screen.addstr(5, 4, descr)
    # kiírjuk a szöveget (bekérés szövege)
    screen.addstr(6, 4, prompt_str)
    # kiírjuk a szöveget (vonal)
    screen.addstr(10, 10, "__________________________________________________")
    # beírt karakterek megjelenítésének bekapcsolása
    # enter nélküli reagálás bekapcsolása
    # billentyűzet engedélyezése
    curses.noecho()
    curses.nocbreak()
    screen.keypad(0)
    input_string = screen.getstr(10, 10, 50)  # bekérjük a sztringet
    # beírt karakterek megjelenítésének kikapcsolása
    # enter nélküli reagálás kikapcsolása
    # billentyűzet kikapcsolása
    curses.noecho()
    curses.cbreak()
    screen.keypad(1)

    return input_string  # visszatérünk a bemenettel


def execute_cmd(menu_str, cmd_string, finish="A parancs végrehajtása sikeres."):
    """
    Definiáljuk az execute_cmd függvényt, amely a parancsokat hajtja végre.

    Első paramétere, a menu_str tartalmazza a megjelenítendő menü nevét.
    Második paramétere, a cmd_string tartalmazza a végrehajtandó parancsot.
    Harmadik, opcionális paramétere, a finish, tartalmazza a kiírandó üzenetet.
    """

    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
    screen.border(0)  # margók beállítása
    screen.refresh()  # képernyő frissítése

    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str[0], curses.A_BOLD | curses.color_pair(2))
    screen.refresh()  # képernyő frissítése

    a = os.system(cmd_string)  # végrehajtjuk a paraméterül kapott parancsot
    if a == 0:
        # kiírjuk a szöveget
        screen.addstr(5, 4, finish)
        screen.refresh()  # képernyő frissítése
    else:
        # kiírjuk a szöveget
        screen.addstr(5, 4, "A parancs végrehajtása sikertelen.")
        screen.refresh()  # képernyő frissítése

    # kiírjuk a szöveget
    string = "Nyomj entert a szülőmenübe való visszatéréshez."
    screen.addstr(8, 2, string)
    screen.refresh()  # képernyő frissítése
    poz = len(string.decode("utf-8"))
    # várjuk az entert a sor végén álló kurzorral
    screen.getstr(8, poz + 2)


def execute_cmd_with_list(menu_str, cmd_string, finish):
    """
    Definiáljuk az execute_cmd_with_list függvényt, amely a parancsokat
    hajtja végre, majd finish üzenetnek egy tömbön jár végig.

    Első paramétere, a menu_str tartalmazza a megjelenítendő menü nevét.
    Második paramétere, a cmd_string tartalmazza a végrehajtandó parancsot.
    A harmadik paramétere, a finish tömb, amely a kiírandó üzenetet tartalmazza.
    """

    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
    screen.border(0)  # margók beállítása
    screen.refresh()  # képernyő frissítése

    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str[0], curses.A_BOLD | curses.color_pair(2))
    screen.refresh()  # képernyő frissítése

    line = 5
    a = os.system(cmd_string)  # végrehajtjuk a paraméterül kapott parancsot
    if a == 0:
        # kiírjuk a szöveget
        for i in finish:  # bejárjuk a tömböt
            screen.addstr(line, 4, i)  # az ötödik sortól kezdjük kiírni
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1
    else:
        # kiírjuk a szöveget
        screen.addstr(line, 4, "A parancs végrehajtása sikertelen.")
        screen.refresh()  # képernyő frissítése

    # kiírjuk a szöveget
    string = "Nyomj entert a szülőmenübe való visszatéréshez."
    screen.addstr(line + 1, 2, string)
    screen.refresh()  # képernyő frissítése
    poz = len(string.decode("utf-8"))
    # várjuk az entert a sor végén álló kurzorral
    screen.getstr(8, poz + 2)


def print_string(menu_str, string, red=False,
        finish="Nyomj entert a szülőmenübe való visszatéréshez"):
    """
    Definiáljuk a print_string függvényt, amely sztringet ír ki.


    Az első paraméter megjeleníti a kiírnadó menüpont nevét.
    A második paraméter megjeleníti a felhasználónak szánt üzeneteket,
    miután korábban beolvasás történt a felhasználótól.
    A harmadik paramétere, a red, opcionális. Ha igaz, piros kiemelést
    használunk.
    A negyedik paramétere, a finish, opcionális. A művelet végén kiírnadó
    szöveg.
    """

    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
    screen.border(0)  # margók beállítása
    screen.refresh()  # képernyő frissítése

    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str, curses.A_BOLD | curses.color_pair(2))
    screen.refresh()
    # kiírjuk a szöveget
    if not red:
        screen.addstr(5, 5, string, curses.color_pair(1))
        screen.refresh()
    else:
        screen.addstr(5, 5, string, curses.A_BOLD | curses.color_pair(3))
        screen.refresh()
    # kiírjuk a szöveget
    screen.addstr(8, 2, finish)
    screen.refresh()  # képernyő frissítése
    # várjuk az entert a sor végén álló kurzorral
    poz = len(finish.decode("utf-8"))  # a unicode sztringek miatt kell a decode
    screen.getstr(8, poz + 2)


def print_list(menu_str, menu_list, red=False,
        finish="Nyomj entert a szülőmenübe való visszatéréshez"):
    """
    Kiírunk egy tömbnyi sztringet.

    Az első paraméter megjeleníti a kiírnadó menüpont nevét.
    A második paraméter megjeleníti a felhasználónak szánt üzeneteket.
    A harmadik paraméter, a red, piros háttért ír ki.
    A negyedik, finish,  megjeleníti a felhasználónak szánt vég üzenetet.
    """

    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
    screen.border(0)  # margók beállítása
    screen.refresh()  # képernyő frissítése

    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str, curses.A_BOLD | curses.color_pair(2))
    screen.refresh()

    # ha nem kérünk piros kiírást
    if not red:
        line = 5  # az ötödik sortól írjuk ki a tömb elemeit
        for i in menu_list:  # bejárjuk a tömböt
            screen.addstr(line, 4, i)  # az ötödik sortól kezdjük kiírni
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1
    # ha kérük piros kiírást
    else:
        # a tömb első elemét kiírjuk pirossal
        screen.addstr(5, 4, menu_list[0], curses.A_BOLD | curses.color_pair(3))
        screen.refresh()  # képernyő frissítése
        line = 6
        for i in menu_list[1:]:  # a tömb második elemétől írunk ki
            screen.addstr(line, 4, i)  # az line-adik sortól kezdjük kiírni
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1

    # kiírjuk a szöveget
    screen.addstr(line + 1, 2, finish)
    screen.refresh()  # képernyő frissítése
    # várjuk az entert a sor végén álló kurzorral
    poz = len(finish.decode("utf-8"))
    screen.getstr(line + 1, poz + 2)


def call_function(fun):
    """
    Definiáljuk a call_function függvényt, ami az almenük meghívásáért felelős.

    Egyetlen paramétere a fun, amely a meghívandó függvény neve.
    A menu(self) függvényben kerül meghívásra, hogy megjelenítse az almenüket.
    """

    fun()  # meghívjuk a paraméterül kapott függvényt


def exec_py(menu_str, py):
    """
    Definiáljuk az exec_py függvényt, amely egy másik Python script
    futtatását hajtja végre.

    Az első paramétere, a menu_str a megjelenítendő menüpont neve.
    A második paramétere, a py a meghívandó Python fájl.
    """

    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    screen.border(0)  # margók beállítása
    # kiírjuk a menüpont nevét
    screen.addstr(2, 2, menu_str, curses.A_BOLD | curses.color_pair(2))
    screen.refresh()  # képernyő frissítése

    # lefuttatjuk a paraméterül kapott scriptet
    a = subprocess.call(py, shell=False)
    if a == 0:
        print "\nA parancs végrehajtása sikeres"
    else:
        print "\nA parancs végrehajtása sikertelen"
    raw_input("Nyomj entert a szülőmenübe való visszatéréshez\n")


def print_function_without_args(menu_str, fun, red=False,
        finish="Nyomj entert a szülőmenübe való visszatéréshez"):
    """
    Definiáljuk a print_function_without_args függvényt, amely egy importált
    modul függvénye által visszaadott tömböt jár be és ír ki.

    Első paramétere, a menu_str a megjelenítendő menüpont neve.
    Második paramétere, a fun, annak a függvénynek a neve, melynek visszatérési
    értékét kiírjuk.
    Harmadik paramétere, a red, opcionális. Ha nem False, értékét pirossal
    emeljük ki.
    Negyedik paramétere, a finish, opcionális. A művelet végén kiírnadó szöveg.
    """

    screen = curses.initscr()  # új ablakot inicializál
    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)  # színpár def.

    screen.clear()  # tisztítjuk a képernyőt
    screen.border(0)  # margók beállítása
    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str, curses.A_BOLD | curses.color_pair(2))
    screen.refresh()  # képernyő frissítése

    # ha a red paraméter megadásra került
    if red:
        # a paraméter értékét írjuk ki pirossal kiemelve
        screen.addstr(5, 4, red, curses.A_BOLD | curses.color_pair(3))
        screen.refresh()  # képernyő frissítése
        line = 6  # 6. sortól folytatjuk a kiírást
        for i in fun():  # bejárjuk a tömböt
            screen.addstr(line, 4, i)  # kiírjuk a tömb elemeit is
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1

    else:  # ha a red=False
        line = 5  # az ötödik sortól írjuk ki a tömb elemeit
        for i in fun():  # bejárjuk a tömböt
            screen.addstr(line, 4, i)  # az ötödik sortól kezdjük kiírni
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1

    # kiírjuk a szöveget
    # screen.addstr(line+1, 2, "Nyomj entert a szülőmenübe való visszatéréshez")
    screen.addstr(line + 1, 2, finish)
    screen.refresh()  # képernyő frissítése
    # várjuk az entert a sor végén álló kurzorral
    poz = len(finish.decode("utf-8"))
    screen.getstr(line + 1, poz + 2)


def print_function_with_args(menu_str, fun, red=False,
        finish="Nyomj entert a szülőmenübe való visszatéréshez"):
    """
    Definiáljuk a print_function_without_args függvényt, amely egy importált
    modul függvénye által visszaadott tömböt jár be és ír ki.

    Első paramétere, a menu_str a megjelenítendő menüpont neve.
    Második paramétere, a fun, annak a függvénynek a neve, melynek visszatérési
    értékét kiírjuk.
    Harmadik paramétere, a red, opcionális. Ha nem False, értékét pirossal
    emeljük ki.
    Negyedik paramétere, a finish, opcionális. A művelet végén kiírnadó szöveg.
    """

    screen = curses.initscr()  # új ablakot inicializál
    curses.start_color()  # lehetővé teszi a színek használatát
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)  # színpár def.
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)  # színpár def.

    screen.clear()  # tisztítjuk a képernyőt
    screen.border(0)  # margók beállítása
    # kiírjuk a szöveget (menüpont neve)
    screen.addstr(2, 2, menu_str, curses.A_BOLD | curses.color_pair(2))
    screen.refresh()  # képernyő frissítése

    # ha a red paraméter megadásra került
    if red:
        # a paraméter értékét írjuk ki pirossal kiemelve
        screen.addstr(5, 4, red, curses.A_BOLD | curses.color_pair(3))
        screen.refresh()  # képernyő frissítése
        line = 6  # 6. sortól folytatjuk a kiírást
        for i in fun:  # bejárjuk a tömböt
            screen.addstr(line, 4, i)  # kiírjuk a tömb elemeit is
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1

    else:  # ha a red=False
        line = 5  # az ötödik sortól írjuk ki a tömb elemeit
        for i in fun:  # bejárjuk a tömböt
            screen.addstr(line, 4, i)  # az ötödik sortól kezdjük kiírni
            screen.refresh()  # képernyő frissítése
            # növeljük a line értékét, hogy a következő sorba ugorjunk
            line += 1

    # kiírjuk a szöveget
    # screen.addstr(line+1, 2, "Nyomj entert a szülőmenübe való visszatéréshez")
    screen.addstr(line + 1, 2, finish)
    screen.refresh()  # képernyő frissítése
    # várjuk az entert a sor végén álló kurzorral
    poz = len(finish.decode("utf-8"))
    screen.getstr(line + 1, poz + 2)
