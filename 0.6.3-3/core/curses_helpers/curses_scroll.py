#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület görgetésre alkalmas ablakát valósítja meg.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import curses
import locale
import math
import subprocess
import textwrap

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def curses_scroll(menu_str, path):
    """Egy görgetésre alkalmas ablakot hoz létre.

    Első paramétere, a menu_str a kiírandó menüpont neve.
    Második paramétere, a path, a kiírandó fájl elérési útja."""

    screen = curses.initscr()  # új ablakot inicializál
    screen.clear()  # tisztítjuk a képernyőt
    curses.noecho()  # beírt karakterek megjelenítésének kikapcsolása
    curses.cbreak()   # enter nélküli reagálás kikapcsolása
    curses.start_color()  # lehetővé teszi a színek használatát

    screen.keypad(1)  # billentyűzet kikapcsolása
    # színpár definiálás a cursesnek és az ablaknak
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_WHITE)
    curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_RED)
    curses.init_pair(4, curses.COLOR_YELLOW, curses.COLOR_BLUE)
    screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás (curses)
    screen.refresh()  # képernyő frissítése

    screen.border(0)  # margók beállítása nullára
    curses.curs_set(0)  # kurzol beállítása láthatatlanra

    # lekérdezzük a terminál szélességét és magasságát, hogy dinamikus legyen
    #  a megjeleníthető sorok száma és az ablak méretei
    terminal_rows, terminal_columns = subprocess.check_output(['stty',
        'size']).split()

    # megjelenítendő sorok maximális száma a terminál magassága mínusz 8
    max_rows_display = int(terminal_rows) - 8

    # az ablak magassága a terminál magassága mínusz 6
    subwindow_height = int(terminal_rows) - 6

    # az ablak szélessége a terminál szélessége
    subwindow_width = int(terminal_columns)

    # beállítjuk az ablak méreteit és margóit
    # subwindow = curses.newwin(18, 250, 6, 0)
    subwindow = curses.newwin(subwindow_height, subwindow_width, 6, 0)
    subwindow.box()  # ablak szegélyezése
    subwindow.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás (ablak)

    # tájékoztató sztingek
    navigate_message = u"Görgetéshez a nyíl billentyűket használhatod."
    quit_message = u"Kilépés az Esc billenytű lenyomásával lehetséges."

    # menü kiírása a cursesre
    screen.addstr(2, 2, menu_str.encode("utf-8"),
        curses.A_BOLD | curses.color_pair(4))
    # tájékoztató szöveg kiírása a cursesre
    screen.addstr(4, 2, navigate_message.encode("utf-8"), curses.color_pair(4))
    screen.addstr(5, 2, quit_message.encode("utf-8"), curses.color_pair(4))

    # inicializáljuk a tömböt
    file_content_list = []

    # megnyitjuk a paraméterül kapott fájlt
    with open(path, "r") as source_file:
        # soronként beolvassuk a fájlt egy tömbbe
        lines = source_file.read().splitlines()

        # bejárjuk a tömböt
        for line in lines:
            # soronként elmentjük egy tömbbe, az ablak szélességének megfelelően
            # darabolva a sorokat, 3-as "margóval"
            wrapped_lines = textwrap.wrap(line, subwindow_width - 3)

            # bejárjuk a feldarabolt sorokat tartalmazó listát
            for wrapped_line in wrapped_lines:
                # az elemeket hozzáadjuk a file_content_list-hez
                file_content_list.append(wrapped_line)

    # a sorok száma a file_content_list tömb elemeinek száma
    rows_number = len(file_content_list)

    # a sorok számából és a maximálisan megjeleníthető elemekből kiszámoljuk
    # a szükséges oldalak számát
    # az osztás miatt floattá kasztoljuk az inteket, majd vesszük a felfelé
    # kerekített első számot és egéssszé kasztoljuk
    pages_number = int(math.ceil(float(rows_number) / float(max_rows_display)))

    # inicializáljuk a változókat: pozíció, oldal, i
    current_position = 1
    current_page = 1
    i = 1

    # a  megjelenítendő sorok maximális számáig megyünk
    for i in range(1, max_rows_display + 1):
        # ha nincsenek kiírnadó elemek a tömbben
        if rows_number == 0:
            # kiírjuk a hibaüzenetet
            subwindow.addstr(1, 1, "Nincsenek megjeleníthető elemek",
               curses.A_BOLD | curses.color_pair(3))

        # ha vannak kiírnadó elemek
        else:
            # ha az i. elem az aktuális, kiemelve írjuk ki
            if i == current_position:
                subwindow.addstr(i, 2, file_content_list[i-1],
                    curses.color_pair(2))

            # ha nem az aktuális elem, rendesen írjuk ki
            else:
                subwindow.addstr(i, 2, file_content_list[i-1],
                    curses.color_pair(1))

            # ha i egyenlő a kiirandó elemek számával
            if i == rows_number:
                # kilépünk a for ciklusból
                break

    screen.refresh()  # frissítjük a cursest
    subwindow.refresh()  # frissítjük az ablakot

    # elkapjuk a felhasználó által leütött billentyűt
    x = screen.getch()
    # amíg nem 27 a kódja, ciklusban maradunk, különben kilépünk
    #  27 = Esc
    while x != 27:
        # ha a lefelé nyilat nyomja meg
        if x == curses.KEY_DOWN:
            # ha az első oldalon vagyunk
            if current_page == 1:
                # ha az aktuális pozíció kisebb, mint i
                if current_position < i:
                    # akkor lépjünk előre egyet
                    current_position += 1

                # ha az aktuális pozíció nem kisebb, mint i
                else:
                    # ha az oldalszám nagyobb, mint 1
                    if pages_number > 1:
                        # lapozzunk egy oldat
                        current_page += 1
                        # előző oldal
                        previous_page = current_page - 1
                        # a megjeleníthető sorok maximális száma és az előző
                        # oldalszám alapján megkapjuk az eddig megjelenített
                        # elemszámot
                        displayed_rows = max_rows_display * previous_page
                        # az új oldal első elemére állunk
                        current_position = 1 + displayed_rows

            # ha az aktuális oldal száma egyenlő az oldalak számával
            elif current_page == pages_number:
                # ha az aktuális pozíció kisebb, mint a maximálisan
                # megjeleníthető sorok száma
                if current_position < rows_number:
                    # előre lépünk egyet
                    current_position += 1

            # különben
            else:
                # előző oldal
                previous_page = current_page - 1
                # a megjeleníthető sorok maximális száma és az előző
                # oldalszám alapján megkapjuk az eddig megjelenített
                # elemszámot
                displayed_rows = max_rows_display * previous_page
                # ha a pozíció kisebb, mint az eddigi oldalakon megjelenített
                # elemek száma és a maximálisan megjeleníthető sorok száma
                if current_position < (max_rows_display + displayed_rows):
                    # előre lépünk egyet
                    current_position += 1

                # ha a pozíció nem kisebb, mint az eddigi oldalakon
                # megjelenített elemek száma
                else:
                    # lapozzunk egy oldat
                    current_page += 1
                    # előző oldal
                    previous_page = current_page - 1
                    # a megjeleníthető sorok maximális száma és az előző
                    # oldalszám alapján megkapjuk az eddig megjelenített
                    # elemszámot
                    displayed_rows = max_rows_display * previous_page
                    # az új oldal első elemére állunk
                    current_position = 1 + displayed_rows

        # ha a felfelé nyilat nyomja fel
        elif x == curses.KEY_UP:
            # ha az első oldalon állunk
            if current_page == 1:
                # ha az aktuális pozíció nagyobb, mint egy
                if current_position > 1:
                    # visszalépünk egyet
                    current_position -= 1

            # ha nem az első oldalon állunk
            else:
                # előző oldal
                previous_page = current_page - 1
                # a megjeleníthető sorok maximális száma és az előző
                # oldalszám alapján megkapjuk az eddig megjelenített
                # elemszámot
                displayed_rows = max_rows_display * previous_page
                # ha a pozíció nagyobb, mint az eddig megjelenített elem+1
                if current_position > (1 + displayed_rows):
                    # visszalépünk egyet
                    current_position -= 1

                # ha a pozíció nem nagyobb, mint az eddig megjelenített elem+1
                else:
                    # visszalépünk az előző oldalra
                    current_page -= 1
                    # előző oldal
                    previous_page = current_page - 1
                    # a megjeleníthető sorok maximális száma és az előző
                    # oldalszám alapján megkapjuk az eddig megjelenített
                    # elemszámot
                    displayed_rows = max_rows_display * previous_page
                    # az oldal legalsó elemére állunk
                    current_position = max_rows_display + displayed_rows

        # ha a balra nyilat nyomta meg
        elif x == curses.KEY_LEFT:
            # az nem az első oldalon állunk
            if current_page > 1:
                # visszalépünk egy oldalt
                current_page -= 1
                # előző oldal
                previous_page = current_page - 1
                # a megjeleníthető sorok maximális száma és az előző
                # oldalszám alapján megkapjuk az eddig megjelenített
                # elemszámot
                displayed_rows = max_rows_display * previous_page
                # az első elemre lépünk
                current_position = 1 + displayed_rows

        # ha a jobbra nyilat nyomta meg
        elif x == curses.KEY_RIGHT:
            # ha az aktuális oldal száma kisebb, mint az oldalak száma
            if current_page < pages_number:
                # lapozunk egyet
                current_page += 1
                # előző oldal
                previous_page = current_page - 1
                # a megjeleníthető sorok maximális száma és az előző
                # oldalszám alapján megkapjuk az eddig megjelenített
                # elemszámot
                displayed_rows = max_rows_display * previous_page
                # az első elemre lépünk
                current_position = 1 + displayed_rows

        # töröljük az ablakot
        subwindow.erase()
        # curses margó nullára állítása
        screen.border(0)
        # ablak margó nullára állítása
        subwindow.border(0)

        # előző oldal
        previous_page = current_page - 1
        # a megjeleníthető sorok maximális száma és az előző
        # oldalszám alapján megkapjuk az eddig megjelenített
        # elemszámot
        displayed_rows = max_rows_display * previous_page
        # az előző oldalig megjeleníthető plusz 1 sortól megyünk,
        # az előző oldalig megjeleníthető, plusz a maximálisan megjeleníthető
        # i+1-ig megyünk
        for i in range(1 + displayed_rows, max_rows_display + 1 +
                displayed_rows):
            # ha nincs kiírandó elem
            if rows_number == 0:
                # hibaüzenetet írunk ki
                # subwindow.addstr(1, 1, "Nincsenek megjeleníthető elemek",
                #    curses.A_BOLD | curses.color_pair(3))
                curses.endwin()
                exit()

            # ha van kiírandó elem
            else:
                # előző oldal
                previous_page = current_page - 1
                # a megjeleníthető sorok maximális száma és az előző
                # oldalszám alapján megkapjuk az eddig megjelenített
                # elemszámot
                displayed_rows = max_rows_display * previous_page
                # ha az i. elem az aktuális...
                if i + displayed_rows == current_position + displayed_rows:
                    # kiemelve írjuk ki
                    subwindow.addstr(i - displayed_rows, 2,
                        file_content_list[i-1], curses.color_pair(2))

                # ha nem az aktuális elem...
                else:
                    # rendesen írjuk ki
                    subwindow.addstr(i - displayed_rows, 2,
                        file_content_list[i-1], curses.color_pair(1))

                # ha i egyenlő a kiirandó elemek számával
                if i == rows_number:
                    # kilépünk a for ciklusból
                    break

        # frissítjük a cursest
        screen.refresh()
        # frissítjük az ablakot
        subwindow.refresh()
        # elkapjuk a felhasználó által leütött billentyűt
        x = screen.getch()

    # Esc-nél (27) kilépünk
    curses.endwin()
