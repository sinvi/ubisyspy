#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale

import curses
from curses_helpers import curses_functions as cf  # a curses függvények
from submenu import fifth_menu  # az ötödik menü
from submenu import first_menu  # az első menühöz kell
from submenu import fourth_menu  # a negyedik menü
from submenu import second_menu  # a második menü
from submenu import sixth_menu  # a hatodik menü
from submenu import third_menu  # a harmadik menü


locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def menu(self):
    """
    A menu(self) függvény definiálása, amely a főmenüt jeleníti meg.

    A self paraméter szükséges a curses.wrapper() miatt.
    """

    # x változó inicializálása
    x = 0

    # ha a kilépés elem számát adja meg a felhasználó, kilépünk,
    # addig pedig a ciklusban maradunk
    while x != ord('7'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # első színpár definíció
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # második színpár definíció
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # a menü elemei
        items = [u"UbiSysPy – Főmenü",
                 u"Szám billentyű megnyomásával válassz az alábbi menük közül:",
                 u"1) Rendszeradatok",
                 u"2) Csomagkezelés",
                 u"3) Rendszerbeállítások",
                 u"4) Biztonsági mentés",
                 u"5) Naplózás",
                 u"6) A programról",
                 u"7) Kilépés"]

        # kiírjuk a menü elemeit, a színpárok és attribútumok használatával
        screen.clear()  # kijelző törlése
        screen.border(0)  # margó beállítása
        # programnév és menüpont kiírása
        screen.addstr(2, 2, items[0].encode("utf-8"),
                      curses.A_BOLD | curses.color_pair(2))
        # felhasználói tájékoztató kiírása (a számvezérlésről)
        screen.addstr(5, 2, items[1].encode("utf-8"),
                      curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))
        screen.addstr(11, 4, items[6].encode("utf-8"))
        screen.addstr(12, 4, items[7].encode("utf-8"))
        screen.addstr(13, 4, items[8].encode("utf-8"))
        screen.refresh()  # kijelző frissítése, hogy a fentiek megjelenjenek

        x = screen.getch()  # várjuk a felhasználó bemenetét

        if x == ord('1'):  # ha 1-et ad meg a felhasználó,
            curses.endwin()  # vége a curses-nek és
            # meghívjuk a first nevű függvényt az almenükkel
            cf.call_function(first_menu.first_menu)

        elif x == ord('2'):  # ha 2-t ad meg a felhasználó,
            curses.endwin()  # vége a curses-nek és
            # meghívjuk a second nevű függvényt az almenükkel
            cf.call_function(second_menu.second_menu)

        elif x == ord('3'):  # ha 3-at ad meg a felhasználó,
            curses.endwin()  # vége a curses-nek és
            # meghívjuk a third nevű függvényt az almenükkel
            cf.call_function(third_menu.third_menu)

        elif x == ord('4'):  # ha 4-et ad meg a felhasználó,
            curses.endwin()  # vége a curses-nek és
            # meghívjuk a fourth nevű függvényt az almenükkel
            cf.call_function(fourth_menu.fourth_menu)

        elif x == ord('5'):  # ha 5-öt ad meg a felhasználó,
            curses.endwin()  # vége a curses-nek és
            # meghívjuk a fifth nevű függvényt az almenükkel
            cf.call_function(fifth_menu.fifth_menu)

        elif x == ord('6'):  # ha 6-ot ad meg a felhasználó,
            curses.endwin()  # vége a curses-nek és
            # meghívjuk a sixth nevű függvényt az almenükkel
            cf.call_function(sixth_menu.sixth_menu)

    curses.endwin()  # curses vége


if __name__ == "__main__":
    try:
        curses.wrapper(menu)  # curses és a menü indítása
    except KeyboardInterrupt:  # kezeljük a Ctrl-C-t
        curses.endwin()  # szabályos kilépés
