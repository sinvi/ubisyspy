#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

Az adatbázis és a grafikonok ütemezését végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import locale
import os
from crontab import CronTab

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def create_cron():
    """Cron bejegyzések hozzáadása a user cronhoz."""

    # felhasználó lekérdezése
    username = os.environ["USER"]

    # az aktuális script (cron.by) könyvtárának (graph) lekérdezése
    current_dir = os.path.dirname(os.path.abspath(__file__))
    # a script könyvtárának szülőkönyvtára (core)
    current_parent = os.path.dirname(current_dir)
    # a programkönyvtár szülőkönyvtára
    base_parent = os.path.dirname(current_parent)

    # az adatbázist frissítő py helyének megalkotása a szülőkönyvtárból
    db_py_path = current_parent + "/database/get_log_entries.py"

    # a grafikonokat frissítő py helyének megalkotása
    graph_py_path = current_dir + "/create_graphs.py"

    # virtualenv Python bináris helyének megalkotása a szülőkönyvtárból
    venv_py_path = base_parent + "/venv/bin/python"

    # adatbázis frissítő cron parancs megalkotása
    db_cron_command = venv_py_path + " " + db_py_path

    # grafikon frissítő cron parancs megalkotása
    graph_cron_command = venv_py_path + " " + graph_py_path

    # adatbázis frissítő cron komment megalkotása
    db_cron_comment = "UbiSysPy database autoupdate"

    # grafikon frissítő cron komment megalkotása
    graph_cron_comment = "UbiSysPy graph autoupdate"

    # adatbázis frissítő cron létrehozása
    db_cron = CronTab(user=username)
    # megadjuk a parancsot és a kommentet
    db_cron_job = db_cron.new(command=db_cron_command, comment=db_cron_comment)
    # 5 percenként fut
    db_cron_job.setall('*/5 * * * *')
    # kiírjuk a crontabba
    db_cron.write()

    # grafikon frissítő cron létrehozása
    graph_cron = CronTab(user=username)
    # megadjuk a parancsot és a kommentet
    graph_cron_job = graph_cron.new(command=graph_cron_command,
        comment=graph_cron_comment)
    # 5 percenként fut
    graph_cron_job.setall('*/5 * * * *')
    # kiírjuk a crontabba
    graph_cron.write()


def check_crons():
    """Ellenőrzi, hogy a cronok engedélyezve vannak-e."""

    # felhasználó lekérdezése
    username = os.environ["USER"]

    # új cron objektum
    cron = CronTab(user=username)

    db_job = False  # inicializáljuk a db_job-ot
    # megnézzük, hogy létezik-e adatbázis cron
    for job in cron.find_comment("UbiSysPy database autoupdate"):
        db_job = True

    graph_job = False   # inicializáljuk a graph_job-ot
    # megnézzük, hogy létezik-e grafikon cron
    for job in cron.find_comment("UbiSysPy graph autoupdate"):
        graph_job = True

    # ha mindkettő létezik, 0-val térünk vissza
    if db_job == True and graph_job == True:
        return 0

    # ha csak az egyik létezik, 2-vel térünk vissza
    elif db_job == True and graph_job == False or \
                            db_job == False and graph_job == True:
        return 2

    # ha egyik sem létezik, 1-gyel térünk vissza
    else:
        return 1


def delete_cron():
    """Cron jobok törlése."""

    # felhasználó lekérdezése
    username = os.environ["USER"]

    # új cron objektum
    cron = CronTab(user=username)

    # megkeressük az adatbázis cront
    db_comment = "UbiSysPy database autoupdate"
    for job in cron.find_comment(db_comment):
        # és letiltjuk
        cron.remove_all(comment=db_comment)

    # megkeressük a grafikon cront
    graph_comment = "UbiSysPy graph autoupdate"
    for job in cron.find_comment(graph_comment):
        # és letiltjuk
        cron.remove_all(comment=graph_comment)

    # kiírjuk a crontabba a letiltást
    cron.write()
