#!/usr/bin/env python
# coding:utf-8

"""
Copyright (c) 2016, Sinkovics Vivien

A napló bejegyzéseket olvassa ki.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
import os
import pygal as pg  # virtualenven belüli pippel telepítve a grafikonokhoz
import sqlite3 as s3  # virtualenven belüli pippel telepítve


locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def create_graphs():
    """"A grafikonokat készíti el."""

    # ellenőrizzük, hogy létezik-e az adatbázis
    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az adatbázis elérési útjának sztringjét
    db = user_dir + "/.ubisyspy/grafikonok/naplozasi-adatok.sqlite"
    # a grafikonok mentési helye
    graph_path = user_dir + "/.ubisyspy/grafikonok/"

    # ha nem létezik
    if not os.path.exists(db):
        # eggyel térünk vissza, hogy jelezzük a hibát
        return 1

    # ha létezik
    else:
        # kapcsolódunk az adatbázishoz
        log_db = s3.connect(db)
        cursor = log_db.cursor()

        # lekérjük az utolsó sort az adatbázisból
        cursor.execute("SELECT * FROM"
                       "(SELECT * FROM logs ORDER BY id DESC LIMIT 25)"
                       " ORDER BY id ASC")
        # elmentjük az utolsó sort
        # tuple-t kapunk vissza
        last_rows = cursor.fetchall()

        # tömbök létrehozása
        log_dateandtime_list = []
        gbmemfree_list = []
        gbmemused_list = []
        gbbuffers_list = []
        gbcached_list = []
        gbswpfree_list = []
        gbswpused_list = []
        gbswpcad_list = []
        tps_list = []
        rtps_list = []
        wtps_list = []
        breadmbs_list = []
        bwrtnmbs_list = []
        user_list = []
        nice_list = []
        system_list = []
        io_list = []
        idle_list = []

        # bejárjuk a tuple-t, hogy kinyerjük a listákat
        for row in last_rows:
            log_date = str(row[1])
            log_time = str(row[2])

            # összefűzzük a dátum és az idő sztringet
            log_dateandtime = log_date + " " + log_time
            log_dateandtime_list.append(log_dateandtime)

            # GB-ra váltjuk a memóriára vonatkozó értékeket
            # és a saját tömbjeikbe tesszük őket
            gbmemfree_list.append(float("%.2f" % (float(int(row[3])) / 1024
                                                  / 1024)))
            gbmemused_list.append(float("%.2f" % (float(int(row[4])) / 1024
                                                  / 1024)))
            gbbuffers_list.append(float("%.2f" % (float(int(row[5])) / 1024 /
                                                  1024)))
            gbcached_list.append(float("%.2f" % (float(int(row[6])) / 1024 /
                                                 1024)))

            # GB-ra váltjuk a swapra vonatkozó értékeket
            # és a saját tömbjeikbe tesszük őket
            gbswpfree_list.append(float("%.2f" % (float(int(row[7])) / 1024 /
                                                  1024)))
            gbswpused_list.append(float("%.2f" % (float(int(row[8])) / 1024 /
                                                  1024)))
            gbswpcad_list.append(float("%.2f" % (float(int(row[9])) / 1024 /
                                                 1024)))

            # MB/s-ra váltjuk az I/O-ra vonatkozó értékeket
            # és a saját tömbjeikbe tesszük őket
            tps_list.append(float(row[10]))
            rtps_list.append(float(row[11]))
            wtps_list.append(float(row[12]))
            breadmbs_list.append(float("%.2f" % (float(float(row[13])) / 2048)))
            bwrtnmbs_list.append(float("%.2f" % (float(float(row[14])) / 2048)))

            # CPU adatok saját tömbe helyezése
            user_list.append(float(row[15]))
            nice_list.append((row[16]))
            system_list.append(float(row[17]))
            io_list.append(float(row[18]))
            idle_list.append(float(row[19]))

        # elkészítjük a RAM-ra vonatkozó grafikont
        # vonal diagramot készítünk
        ram_chart = pg.Line(fill=True, x_label_rotation=-60)
        # címet adunk neki
        ram_chart.title = u"Memória napló grafikonja"
        # elnevezzük az Y tengelyt
        ram_chart.y_title = "GB"
        # elnevezzük az X tengelyt
        ram_chart.x_title = u"napló mentésének ideje"

        # értéket adunk az X tengelynek, méghozzá a napló készítés
        # idejét
        ram_chart.x_labels = log_dateandtime_list

        # felvesszük az egyes értékeket
        ram_chart.add(u"Szabad memória", gbmemfree_list)
        ram_chart.add(u"Foglalt memória", gbmemused_list)
        ram_chart.add(u"Pufferelt memória", gbbuffers_list)
        ram_chart.add(u"Gyorsítótárazott memória", gbcached_list)

        # rendereljük az SVG-t
        ram_path = graph_path + "ram_graph.svg"
        ram_chart.render_to_file(ram_path)

        # elkészítjük a swapra vonatkozó grafikont
        # vonal diagramot készítünk
        swap_chart = pg.Line(fill=True, x_label_rotation=-60)
        # címet adunk neki
        swap_chart.title = u"Swap napló grafikonja"
        # elnevezzük az Y tengelyt
        swap_chart.y_title = "GB"
        # elnevezzük az X tengelyt
        swap_chart.x_title = u"napló mentésének ideje"

        # értéket adunk az X tengelynek, méghozzá a napló készítés
        # idejét
        swap_chart.x_labels = log_dateandtime_list

        # felvesszük az egyes értékeket
        swap_chart.add(u"Szabad swap", gbswpfree_list)
        swap_chart.add(u"Foglalt swap", gbswpused_list)
        swap_chart.add(u"Gyorsítótárazott swap", gbswpcad_list)

        # rendereljük az SVG-t
        swap_path = graph_path + "swap_graph.svg"
        swap_chart.render_to_file(swap_path)

        # elkészítjük az I/O-ra vonatkozó grafikont
        # vonal diagramot készítünk
        io_mbs_chart = pg.Line(fill=True, x_label_rotation=-60)
        # címet adunk neki
        io_mbs_chart.title = u"I/O adatmennyiség grafikonja"
        # elnevezzük az Y tengelyt
        io_mbs_chart.y_title = "MB/sec"
        # elnevezzük az X tengelyt
        io_mbs_chart.x_title = u"napló mentésének ideje"

        # értéket adunk az X tengelynek, méghozzá a napló készítés
        # idejét
        io_mbs_chart.x_labels = log_dateandtime_list

        # felvesszük az egyes értékeket
        io_mbs_chart.add(u"Olvasott teljes adatmennyiség", breadmbs_list)
        io_mbs_chart.add(u"Írt teljes adatmennyiség", bwrtnmbs_list)

        # rendereljük az SVG-t
        io_mbs_path = graph_path + "io_mbs_graph.svg"
        io_mbs_chart.render_to_file(io_mbs_path)

        # elkészítjük az I/O-ra vonatkozó grafikont
        # vonal diagramot készítünk
        io_db_chart = pg.Line(fill=True, x_label_rotation=-60)
        # címet adunk neki
        io_db_chart.title = u"I/O kérelmek grafikonja"
        # elnevezzük az Y tengelyt
        io_db_chart.y_title = "darab"
        # elnevezzük az X tengelyt
        io_db_chart.x_title = u"napló mentésének ideje"

        # értéket adunk az X tengelynek, méghozzá a napló készítés
        # idejét
        io_db_chart.x_labels = log_dateandtime_list

        # felvesszük az egyes értékeket
        io_db_chart.add(u"Adatátvitelek", tps_list)
        io_db_chart.add(u"Olvasási kérelmek", rtps_list)
        io_db_chart.add(u"Írási kérelmek", wtps_list)

        # rendereljük az SVG-t
        io_db_path = graph_path + "io_db_graph.svg"
        io_db_chart.render_to_file(io_db_path)

        # elkészítjük a CPU-ra vonatkozó grafikont
        # vonal diagramot készítünk
        cpu_chart = pg.Line(fill=True, x_label_rotation=-60)
        # címet adunk neki
        cpu_chart.title = u"CPU terhelés grafikonja"
        # elnevezzük az Y tengelyt
        cpu_chart.y_title = u"százalék"
        # elnevezzük az X tengelyt
        cpu_chart.x_title = u"napló mentésének ideje"

        # értéket adunk az X tengelynek, méghozzá a napló készítés
        # idejét
        cpu_chart.x_labels = log_dateandtime_list

        # felvesszük az egyes értékeket
        cpu_chart.add(u"Felhasználói folyamatok", user_list)
        cpu_chart.add(u"Felhasználói folyamatok prioritással", nice_list)
        cpu_chart.add(u"Rendszerfolyamatok", system_list)
        cpu_chart.add(u"I/O várakozás", io_list)
        cpu_chart.add(u"Üresjárat", idle_list)

        # rendereljük az SVG-t
        cpu_path = graph_path + "cpu_graph.svg"
        cpu_chart.render_to_file(cpu_path)


if __name__ == '__main__':
    create_graphs()
