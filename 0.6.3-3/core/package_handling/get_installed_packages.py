#!/usr/bin/python
# coding:utf-8


"""
Copyright (c) 2016, Sinkovics Vivien

A telepített csomagok lekérdezését és a listából történő
programvisszaállítást végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import commands
import os
import subprocess
import time
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def installed_packages_list():
    """
    Lekérdezi és fájlba menti a rendszerre telepített összes csomag nevét.
    """

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    list_location = user_dir + "/.ubisyspy/telepitett_csomagok/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    if not os.path.exists(list_location):   # ha nem létezik az útvonal
        os.makedirs(list_location)  # létrehozzuk és
        os.chdir(list_location)  # belépünk, hogy ide kerüljön majd a végtermék

    else:   # különben
        os.chdir(list_location)  # belépünk és
        os.system("rm * 2> /dev/null")   # töröljük a régebbi fájlokat

    
    # előállítjuk a mentési fájl nevét és útvonalát
    installed_packages = list_location + "telepitett_csomagok_" + time_stamp \
                         + ".txt"

    # megnyitjuk az installed_packages fájlt olvasásra a subprocess I/O
    # átirányítás miatt
    iplist = open(installed_packages, "w")
    # összeállítjuk a parancsot
    command = "dpkg --get-selections"
    
    # végrehajtjuk a get-selections-t
    p = subprocess.Popen(command.split(), stdin=iplist,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    communicate = p.communicate()

    # a get-selections kimenetét
    for line in communicate:
        # soronként fájlba írjuk
        iplist.write(line)

    # visszaadjuk a csomaglistás fájl elérési útját
    return installed_packages


def check_apt():
    """
    Ellenőrizzük, az apt képes-e szimulálni.
    """

    # ha megfelel, True-val térünk vissza
    if commands.getstatusoutput("apt-get --simulate dselect-upgrade")[0] == 0:
        return True

    # ha nem felel meg, False-szal
    else:
        return False


def dselect(packagelist, passwd, simulate):
    """
    Ha azt apt képes szimulálni, szimulálunk és frissítünk, ha a felhasználó
    kéri.

    Az első, packagelist adja meg a függvénynek a használandó fájlt.
    A második, a passwd a felhasználó jelszava vagy ha értéke True,
    akkor jelzi a felhasználó root voltát.
    A harmadik,  simulate paraméter, True érték esetén szimulálást kér,
    False érték esetén pedig végrehajtást.
    """

    # ha a simulate True, csak szimulálunk
    if simulate == True:
        # ha a felhasználó root, nem kell sudo
        if passwd == True:
            # megnyitjuk a packagelist fájlt olvasásra a subprocess I/O
            # átirányítás miatt
            plist = open(packagelist, "r")
            # összeállítjuk a parancsot
            command = "dpkg --set-selections"
            # végrehajtjuk a set-selections-t
            p = subprocess.Popen(command.split(), stdin=plist,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

            # összeállítjuk a parancsot
            sim_command = "apt-get --simulate dselect-upgrade"
            # végrehajtjuk a szimulálást
            out = subprocess.Popen(sim_command.split(),
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            # kommunikálunk
            sim_communicate = out.communicate()
            # listába menjük a kimenetet
            out_list = sim_communicate[0]
            # a tömb utolsó eleme üres sztring, így arra nincs szükségünk
            out_list = out_list[:-1].split("\n")

        # ha a felhasználó nem root
        else:
            # megnyitjuk a packagelist fájlt olvasásra a subprocess I/O
            # átirányítás miatt
            plist = open(packagelist, "r")
            # összeállítjuk a parancsot
            command = "dpkg --set-selections"
            # végrehajtjuk a set-selections-t
            p = subprocess.Popen(["sudo", "-S"] + command.split(), stdin=plist,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # átadjuk a jelszót
            communicate = p.communicate(passwd + '\n')

            # összeállítjuk a parancsot
            sim_command = "apt-get --simulate dselect-upgrade"
            # végrehajtjuk a szimulálást
            out = subprocess.Popen(["sudo", "-S"] + sim_command.split(),
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            # átadjuk a jelszót
            sim_communicate = out.communicate(passwd + '\n')
            # listába menjük a kimenetet
            out_list = sim_communicate[0]
            # a tömb utolsó eleme üres sztring, így arra nincs szükségünk
            out_list = out_list[:-1].split("\n")

        # felülírjuk a felhasználó jelszavát
        passwd = os.urandom(len(passwd))

        # inicializáljuk, hogyha nem lenne új csomag
        new_packages = 0

        # inicializáljuk, hogyha nem lenne frissítendő csomag
        update_packages = 0

        # inicializáljuk, hogyha nem összegzés
        summary = 0

        # bejárjuk a listát és számoljuk az elemeket
        for i, j in enumerate(out_list):
            # ha az adott elemben megtalálható az "ÚJ csomagok" vagy a "NEW
            # packages" sztring
            if j.find("ÚJ csomagok") != -1 or j.find("NEW packages") != -1:
                # az adott elem indexét elmentjük
                new_packages = i

            # ha az adott elemben megtalálható az "frissítve" vagy a "will be
            #  upgraded" sztring
            elif j.find("frissítve") != -1 or j.find("will be upgraded") != -1:
                # az adott elem indexét elmentjük
                update_packages = i

            # ha az adott elemben megtalálható az "nem frissített" vagy a
            # "not upgraded" sztring
            elif j.find("nem frissített") != -1 or j.find("not upgraded") != -1:
                # az adott elem indexét elmentjük
                summary = i

        # sortörést teszünk a "Az alábbi ÚJ csomagok lesznek telepítve:"  után
        out_list[new_packages] = out_list[new_packages].strip() + "\n"

        # sortörést teszünk az "Az alábbi csomagok frissítve lesznek:"
        # elé és mögé
        out_list[update_packages] = "\n" + out_list[update_packages].strip() \
                                    + "\n"

        # sortörést teszünk az "X frissített, Y újonnan telepített,
        # Z eltávolítandó és T nem frissített" elé és mögé
        out_list[summary] = "\n" + out_list[summary].strip() + "\n"

        # ha se új, se frissítendő csomag nincs
        if new_packages == 0 and update_packages == 0:
            # visszatérünk az összegző sorral, mivel az mindig van
            # az összegző sor indexe a summary változóban van elmentve
            out_list = out_list[summary]

        # ha nincs új csomag, de frissítendő van
        elif new_packages == 0 and update_packages != 0:
            # visszatérünk a frissítendő csomagok sorától az összegzőig
            # a frissítendő csomagok sorát az update_packages változó
            # tartalmazza
            # az összegző sor indexe a summary változóban van elmentve
            out_list = out_list[update_packages:summary+1]

        # különben, ha van új és frissítendő csomag is, visszatérünk az új
        # csomagoktól az összegző sorig
        else:
            # visszatérünk az új csomagok sorától az összegző sorig
            # az új csomagok sorát az new_packages változó tartalmazza
            # az összegző sor indexe a summary változóban van elmentve
            out_list = out_list[new_packages:summary+1]

        # lekérdezzük a user Home könyvtárát
        user_dir = os.environ["HOME"]
        # létrehozzuk az elérési út sztringjét
        into_mkdir = user_dir + "/.ubisyspy/packages/"
        # időbélyeg a biztonsági mentéshez
        time_stamp = time.strftime('%Y-%m-%d_%T')

        # ha nem létezik...
        if not os.path.isdir(into_mkdir):
            # létrehozzuk
            os.makedirs(into_mkdir)
            # és belépünk
            os.chdir(into_mkdir)

        # ha létezik...
        else:
            # belépünk
            os.chdir(into_mkdir)
            # töröljük a régebbi fájlokat
            os.system("rm * 2> /dev/null")

        # előállítjuk a mentési fájl nevét és útvonalát
        packageslist = into_mkdir + "packageslist_" + time_stamp

        # megnyitjuk a fájlt
        with open(packageslist, "w") as f:
            # soronként kiírjuk az eredményeket a fájlba
            for line in out_list:
                f.write(line)

        return packageslist

    # ha a simulate False, nem csak szimulálunk, hanem végrehajtunk
    elif simulate == False:
        # ha a felhasználó root, nem kell sudo
        if passwd == True:
            # megnyitjuk a packagelist fájlt olvasásra a subprocess I/O
            # átirányítás miatt
            plist = open(packagelist, "r")
            # összeállítjuk a parancsot
            command = "dpkg --set-selections"
            # végrehajtjuk a set-selections-t
            p = subprocess.Popen(command.split(), stdin=plist,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()

            # összeállítjuk a parancsot
            sim_command = "apt-get -y dselect-upgrade"
            # végrehajtjuk a parancsot
            out = subprocess.Popen(sim_command.split(),
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            # kommunikálunk
            sim_communicate = out.communicate()
            # elmentjük a visszatérési értéket
            return_code = out.returncode

        # ha a felhasználó nem root
        else:
            # megnyitjuk a packagelist fájlt olvasásra a subprocess I/O
            # átirányítás miatt
            plist = open(packagelist, "r")
            # összeállítjuk a parancsot
            command = "dpkg --set-selections"
            # végrehajtjuk a set-selections-t
            p = subprocess.Popen(["sudo", "-S"] + command.split(), stdin=plist,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            # átadjuk a jelszót
            communicate = p.communicate(passwd + '\n')

            # összeállítjuk a parancsot
            sim_command = "apt-get -y dselect-upgrade"
            # végrehajtjuk a szimulálást
            out = subprocess.Popen(["sudo", "-S"] + sim_command.split(),
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            # átadjuk a jelszót
            sim_communicate = out.communicate(passwd + '\n')
            # elmentjük a visszatérési értéket
            return_code = out.returncode

        # felülírjuk a felhasználó jelszavát
        passwd = os.urandom(len(passwd))

        # ellenőrizzük a visszatérési értéket
        # ha nullával tér vissza, sikerült a művelet
        if return_code == 0:
            # igazzal térünk vissza
            return True

        # ha nem nullával tér, nem sikerült a művelet
        else:
            # hamissal térünk vissza
            return False


def important_program_list():
    """
    Fontos alkalmazások verzióit kérdezi le és írja.
    """

    # a programok neveit és csomagneveit tartalmazó szótár inicializálása
    important_programs_list = {
                            'Firefox': 'firefox',
                            'Chromium': 'chromium-browser',
                            'Thunderbird': 'thunderbird',
                            'VLC': 'vlc',
                            'LibreOffice': 'libreoffice',
                            'Unity': 'unity',
                            'Xfce': 'xfce4',
                            'Gnome3': 'gnome-session',
                            'KDE': 'plasma-desktop',
                            'MATE': 'mate-desktop',
                            }

    # lista a telepített és frissíthető programoknak
    installed_list = ["Telepítve:"]
    # lista a nem telepített programoknak
    not_installed_list = ["", "Nincs telepítve:"]
    # lista a nem elérhető programoknak
    non_available_list = ["", "Nem elérhető:"]
    # lista a visszatérési értékeknek a fenti három tömbből
    to_return = []

    # bejárjuk a szótárt
    for item in important_programs_list:
        # lekérjük az adott program állapotát
        status = subprocess.Popen("apt-cache policy " +
                                  important_programs_list[item] +
                                  " | grep 'Telepítve' | awk {'print $2'}",
            stdout=subprocess.PIPE, shell=True).communicate()

        # lekérjük az adott program jelölt verzióját
        candidate = subprocess.Popen("apt-cache policy " +
                                     important_programs_list[item] +
                                     " | grep 'Jelölt' | awk {'print $2'}",
            stdout=subprocess.PIPE, shell=True).communicate()

        # a jelölt verzió (candidate) tuple első eleme lesz a változó értéke
        candidate_trunk = candidate[0]

        # ha szerepel a visszatérési tuple első elemében a + jel
        if candidate_trunk.find("+") != -1:
            plus = candidate_trunk.find("+")
            # addig tart a verziószám
            candidate_trunk = candidate_trunk[:plus]
        # ha a - jel szerepel
        if candidate_trunk.find("-") != -1:
            minus = candidate_trunk.find("-")
            # addig tart a verziószám
            candidate_trunk = candidate_trunk[:minus]
        # ha szerepel benne a : jel
        if candidate_trunk.find(":") != -1:
            semicolon = candidate_trunk.find(":")
            # onnantól (plusz egy pozíció) kezdődik a verziószám
            candidate_trunk = candidate_trunk[semicolon+1:]

        # a status tuple első eleme lesz a változó értéke
        status_trunk = status[0]

        # ha a status tuple első elemében szerepel a + jel
        if status_trunk.find("+") != -1:
            plus = status_trunk.find("+")
            # addig tart a verziószám
            status_trunk = status_trunk[:plus]
        # ha a - jel szerepel
        if status_trunk.find("-") != -1:
            minus = status_trunk.find("-")
            # addig tart a verziószám
            status_trunk = status_trunk[:minus]
        # ha szerepel benne a : jel
        if status_trunk.find(":") != -1:
            semicolon = status_trunk.find(":")
            # onnantól (plusz egy pozíció) kezdődik a verziószám
            status_trunk = status_trunk[semicolon+1:]

        # ha a status[0] üres vagy sortörés vagy a candidate[0] = (nincs)
        # nem érhető el az adott program
        if len(status[0]) == 0 \
                or status[0].strip("\n") == "N: Ez a csomag nem található:"  \
                or candidate[0].strip("\n") == "(nincs)":
            # sztring összeállítása
            non_available = item + ": " + "nem elérhető"
            # hozzáadása a listához
            non_available_list.append(non_available)

        # ha a status[0] nem = (nincs) és a status[0] nem = candidate[0]
        # a program telepített, de van frissebb elérhető verziója is
        elif status[0].strip("\n") != "(nincs)" \
                and status[0].strip("\n") != candidate[0].strip("\n"):
            # sztring összeállítása
            update = item + ": " + "A telepített verzió: " + status_trunk \
                     + ", ám frissebb verzió érhető el: " \
                     + candidate_trunk.strip("\n")
            # hozzáadása a listához
            installed_list.append(update)

        # ha a status[0] nem = (nincs)
        elif status[0].strip("\n") == "(nincs)":
            # sztring összeállítása
            not_installed = item + ": " \
                            + status[0].strip("\n").replace("(nincs)",
                "A telepíthető verzió: " + candidate_trunk.strip("\n"))
            # hozzáadása a listához
            not_installed_list.append(not_installed)

        # különben
        else:
            # sztring összeállítása
            installed = item + ": " + "A telepített verzió: " + \
                        status_trunk.strip("\n")
            # hozzáadása a listához
            installed_list.append(installed)

    # ha a tömb tartalmaz legalább két elemet, van telepített program
    if len(installed_list) >= 2:
        # megkeressük a tömbben
        for item in installed_list:
            # és hozzáadjuk a visszatérési tömbhöz
            to_return.append(item)

    # ha a tömb tartalmaz legalább három elemet, van nem telepített program
    if len(not_installed_list) >= 3:
        # megkeressük a tömbben
        for item in not_installed_list:
            # és hozzáadjuk a visszatérési tömbhöz
            to_return.append(item)

    # ha a tömb tartalmaz legalább három elemet, van nem elérhető program
    if len(non_available_list) >= 3:
        # megkeressük a tömbben
        for item in non_available_list:
            # és hozzáadjuk a visszatérési tömbhöz
            to_return.append(item)

    # visszatérünk az összemergélt to_return tömbbel
    return to_return
