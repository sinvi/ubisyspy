#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A tükrök és PPA-k kezelését végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import shutil
import os
import time
import subprocess
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def get_current_mirror():
    """Az aktuális tükröt kérdezi le."""

    to_return = []  # tömb a visszatérési értékeknek

    # inicializálunk
    mirror_line = ""
    deb = -1

    # megnyitjuk olvasása a sources.listet
    with open("/etc/apt/sources.list", "r") as f:
        for line in f:
            line = line.strip()
            # megkeressük az első nem kikommentezett sort, nem üres
            if len(line) != 0 and line[0] != "#":
                # abban sorban van az aktuális tükör
                mirror_line = line
                break

    # kivágjuk a sorból a tükröt magát
    # ha a sor "deb "-bel kezdődik, a 4. karaktertől kezdődik a tükör
    if mirror_line.find("deb ") != -1:
        deb = 4

    # ha a sor "deb-src "-vel kezdődik, a 9. karaktertől
    elif mirror_line.find("deb-src ") != -1:
        deb = 9

    # a deb-től írjuk ki a sort
    mirror_line = mirror_line[deb:]

    # megkeressük a "/ "-t, mert ezután már a verzió kódnév szerepel
    if mirror_line.find("/ ") != -1:
        codename = mirror_line.find("/ ")

    # ha nincs, " "-t keresünk
    else:
        codename = mirror_line.find(" ")

    # a tükör a 4. karaktertől a codename-ig tart
    current_mirror = mirror_line[:codename]

    to_return.append("A jelenleg használt tükröd:")
    to_return.append(current_mirror)
    return to_return


def set_mirror(current, new, passwd):
    """Átállítja a tükröt.

    Első, current paramétere az aktuális tükör.
    Második, new paramétere a beállítandó tükör.
    A harmadik paraméter, a passwd a felhasználó jelszava vagy
    True érték esetén a felhasználó root voltát jelzi.
    """

    to_return = []  # a visszatérési értékeknek

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/sources/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)

    # előállítjuk a mentési fájl nevét és útvonalát
    backup_path = into_mkdir + "sources_" + time_stamp + ".bak"

    # kiírjuk a mentés helyét
    to_return.append("A /etc/apt/sources.list mentési helye:")
    to_return.append("{path}".format(path=backup_path))

    original_path = "/etc/apt/sources.list"

    try:
        # készítünk egy mentést a /etc/apt/sources-ről
        shutil.copy(original_path, backup_path)

    # ha IO hibát kapunk, nem sikerült
    except IOError:
        # hárommal térünk vissza, hogy jelezzük az IO hibát
        return 3

    # ha shutil hibát kapunk, szintén nem sikerült
    except shutil.Error:
        # néggyel térünk vissza, hogy jelezzük a shutil hibát
        return 4

    # ha OSError-t kapunk, valószínűleg létezik a fájl
    except OSError:
        # öttel térünk vissza, hogy jelezzük az OS hibát
        return 5

    # különben
    else:
        # készítünk egy ideiglenes állományt a módosításoknak
        temp_path = into_mkdir + "replaced_sources_" + time_stamp

        # megnyitjuk írásra (w+) az ideiglenes állományt
        with open(temp_path, "w+") as temp:
            # megnyitjuk olvasásra a /etc/apt/sources.list-et
            with open(original_path, "r") as source:
                # végigmegyünk a sources.list sorain
                for line in source:
                    # ha  nem üres a sor, és nem #-vel kezdődik
                    if len(line) != 0 and line[0] != "#":
                        # megnézzük, szerepel-e benne a jelenlegi tükör
                        if line.find(current) != -1:
                            # ha igen, kicseréljük a sort
                            new_line = line.replace(current, new)
                            # és azt írjuk az ideiglenes fájlba
                            temp.write(new_line)
                        # ha nem
                        else:
                            # akkor ez eredeti sort írjuk vissza
                            temp.write(line)
                    # különben
                    else:
                        # visszaírjuk az eredeti sort
                        temp.write(line)

    # ha a felhasználó root
    if passwd == True:
        # módosított sources visszamásolása a /etc/apt/-ba
        command = subprocess.Popen(["cp", temp_path, original_path],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        exitcode = command.returncode

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(["chown", "root:root", original_path],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2

    # ha a felhasználó nem root
    else:
        # módosított sources visszamásolása a /etc/apt/-ba
        command = subprocess.Popen(
            ["sudo", "-S", "cp", temp_path, original_path],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        exitcode = command.returncode

        # felülírjuk a felhasználó jelszavát
        passwd = os.urandom(len(passwd))

        # ha nullával tér vissza
        if exitcode == 0:
            # tulajdonos átállítása rootra
            command = subprocess.Popen(
                ["sudo", "-S", "chown", "root:root", original_path],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate(passwd + '\n')
            exit_code = command.returncode

            # ha nullával tér vissza, sikeres a chown is
            if exit_code == 0:
                return 0

            # ha nem sikerül a chown
            else:
                # eggyel térünk vissza
                return 1

        # ha nem sikerült a másolás
        else:
            # kettővel térünk visza
            return 2


def update_packagelist(passwd):
    """Frissít a csomaglistát.

    Paramétere, a passwd a felhasználó jelszava
    vagy True érték esetén a felhasználó root voltát jelzi.
    """

    # ha a felhasználó root
    if passwd == True:
        command = subprocess.Popen(["apt-get", "update"], stdin=subprocess.PIPE,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        communicate = command.communicate()
        returncode = command.returncode

    # ha a felhasználó nem root
    else:
        # csomaglista frissítése
        command = subprocess.Popen(["sudo", "apt-get", "update"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        returncode = command.returncode

        # felülírjuk a felhasználó jelszavát
        passwd = os.urandom(len(passwd))

    # ha nullával tért vissza, sikeres volt
    if returncode == 0:
        return True

    # különben sikertelen
    else:
        return False


def sys_upgrade(passwd):
    """Telepíti az elérhető frissítéseket.

    Paramétere, a passwd a felhasználó jelszava
    vagy True érték esetén a felhasználó root voltát jelzi."""

    # frissítjük a csomaglistát, ellenőrizzük a visszatérési értéket
    # ha True-val tér vissza, sikeres volt
    if update_packagelist(passwd):
        # ha a felhasználó root
        if passwd == True:
            # frissítjük a csomagokat
            command = subprocess.Popen(["apt-get", "dist-upgrade", "-y", "-q"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            returncode = command.returncode

            # ha nullával tért vissza, sikeres volt
            if returncode == 0:
                # nullával térünk vissza
                return 0

            # különben sikertelen
            else:
                # eggyel térünk vissza
                return 1

        # ha a felhasználó nem root
        else:
            # frissítjük a csomagokat
            command = subprocess.Popen(
                ["sudo", "-S", "apt-get", "dist-upgrade", "-y", "-q"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate(passwd + '\n')
            returncode = command.returncode

            # felülírjuk a felhasználó jelszavát
            passwd = os.urandom(len(passwd))

            # ha nullával tért vissza, sikeres volt
            if returncode == 0:
                # nullával térünk vissza
                return 0

            # különben sikertelen
            else:
                # eggyel térünk vissza
                return 1

    # ha a csomaglista frissítése sikertelen
    else:
        # kettővel térünk vissza
        return 2


def get_ppas():
    """Lekérdezi a használt PPA-kat."""

    to_return = ["A jelenleg használt PPA-k:"]  # a visszatérési értékeknek

    # lekérjük a használt tükröt, amely a get_current_mirror() első eleme
    current_mirror = get_current_mirror()[1]

    # összepipe-oljuk a parancsokat
    policy = subprocess.Popen(["apt-cache", "policy"], stdout=subprocess.PIPE)
    # szűrűnk a http-re
    grep = subprocess.Popen(["grep", "http"], stdin=policy.stdout,
        stdout=subprocess.PIPE)
    # az URL-t tartalmazó oszlopot írjuk ki
    awk = subprocess.Popen(["awk", "{print $2}"], stdin=grep.stdout,
        stdout=subprocess.PIPE)
    # kiszűrjük az ismétlődéseket
    sort = subprocess.Popen(["sort", "-u"], stdin=awk.stdout,
        stdout=subprocess.PIPE)
    # elmentjük a kimenetet
    output = sort.stdout.read()

    # tömbbe daraboljuk a kimeneti sztringet
    output_list = output.split()

    # aktuális tükör a get_current_mirror() első eleme
    cm = get_current_mirror()[1]

    # bejárjuk a tömböt
    for item in output_list:
        # ha az elem nem az aktális tükör, a security vagy az archive...
        if item.find(cm) == -1 and item.find("security.ubuntu") == -1 and \
                item.find("archive.canonical") == -1:
            # hozzáadjuk a tömbhöz
            to_return.append(item)

    # ha csak egy, a kezdőelem szerepel a tömbben, nincs PPA
    if len(to_return) == 1:
        to_return.append("Nincs használatban PPA.")

    # visszatérünk
    return to_return


def save_ppa_list():
    """Fájlba írjuk PPA-k listáját."""

    to_return = []

    # lekérdezzük a user Home könyvtárát
    user_dir = os.environ["HOME"]
    # létrehozzuk az elérési út sztringjét
    into_mkdir = user_dir + "/.ubisyspy/rendszeradatok/"
    # időbélyeg a biztonsági mentéshez
    time_stamp = time.strftime('%Y-%m-%d_%T')

    # ha nem létezik, létrehozzuk
    if not os.path.isdir(into_mkdir):
        os.makedirs(into_mkdir)
        os.chdir(into_mkdir)

    # ha létezik, belépünk
    else:
        os.chdir(into_mkdir)
        os.system("rm * 2> /dev/null")  # töröljük a régebbi fájlokat

    # kiírjuk a mentés helyét
    to_return.append("A mentés helye a {dir} könyvtár.".format(dir =
    into_mkdir))
    # előállítjuk a mentési fájl nevét és útvonalát
    copy_sys_details = into_mkdir + "PPA_list_" + time_stamp + ".txt"

    # megnyitjuk a fájlt
    with open(copy_sys_details, "w") as f:
        # soronként kiírjuk az get_ppas() visszatérési értékét a fájlba
        for line in get_ppas():
            line += "\n"
            f.write(line)

    return to_return


def add_ppa(ppa, passwd):
    """Felveszi a felhasználó által megadott PPA-t.

    Első paramétere, a ppa, a hozzáadnadó PPA neve.
    Második paramétere, a passwd a felhasználó jelszava
    vagy True érték esetén a felhasználó root voltát jelzi.
    """

    # ha a felhasználó root
    if passwd == True:
        # végrehajtjuk a PPA felvételét
        # az add-apt-repository elvileg hozzáadja a publikus kulcsot is
        command = subprocess.Popen(["add-apt-repository", "-y", ppa.strip()],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        returncode = command.returncode

    # ha a felhasználó nem root
    else:
        # végrehajtjuk a PPA felvételét
        # az add-apt-repository elvileg hozzáadja a publikus kulcsot is
        command = subprocess.Popen(
            ["sudo", "-S", "add-apt-repository", "-y", ppa.strip()],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate(passwd + '\n')
        returncode = command.returncode

    # ha a visszatérési értéke 0, sikerült
    if returncode == 0:
        # frissítjük a csomaglistát
        # ha nincs pubkey hiba
        # ha sikerült a csomaglista frissítés
        if update_packagelist(passwd):
            # 0-val térünk vissza
            return 0

        # ha nem sikerült
        else:
            # 1-gyel térünk vissza
            return 1

    # ha a PPA felvétele nem sikerült
    else:
        # 2-vel térünk vissza
        return 2
