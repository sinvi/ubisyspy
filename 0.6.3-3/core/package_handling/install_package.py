#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A csomagok telepítését végzi.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"


import subprocess
import os
import locale

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def install_package(package, passwd):
    """
    Az install_package megadott csomagot telepít.

    A package paraméter adja meg a telepítendő csomagot.
    A passwd paraméter a felhasználó jelszava, vagy True érték esetén
    a felhasználó root voltát jelzi.
    """


    # a felhasználó root
    if passwd == True:
        # frissítjük a csomaglistát
        command = subprocess.Popen(["apt-get", "update", "-y"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        update_return = command.returncode

        # ha sikeresen frissült, telepítjük a programokat
        if update_return == 0:
            # telepítjük a programot
            command = subprocess.Popen(["apt-get", "install", "-y", package],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            returncode = command.returncode

            if returncode == 0:
                # a program sikeresen települt
                return 0

            else:
                # a program telepítése sikertelen
                return 1

        # különben nem sikertelen a csomaglista frissítés
        else:
            return 2


    # a felhasználó nem root
    else:
        # sudo jelszó ellenőrzése
        sudo = subprocess.Popen(["sudo", "-S", "echo", "teszt"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        sudo_communicate = sudo.communicate(passwd + '\n')
        sudo_return = sudo.returncode

        # ha a sudo hitelesítés sikeres
        if sudo_return == 0:
            # frissítjük a csomaglistát
            command = subprocess.Popen(["sudo", "-S", "apt-get", "update",
                "-y"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            update_return = command.returncode

            # ha sikeresen frissült, telepítjük a programokat
            if update_return == 0:
                # telepítjük a programot
                command = subprocess.Popen(["sudo", "-S", "apt-get",
                    "install", "-y", package],
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE)
                communicate = command.communicate()
                returncode = command.returncode

                if returncode == 0:
                    # a program sikeresen települt
                    return 0

                else:
                    # a program telepítése sikertelen
                    return 1

            # különben sikertelen a csomaglista frissítés
            else:
                return 2

        # ha a sudo hitelesítés sikertelen
        else:
            return 3


def install_important_packages(list_file, passwd):
    """Fájlban lévő lista alapján telepíti a fontosabb csomagokat.

    Első paramétere, a list_file, a listát tartalmazó fájl elérési útja.
    A passwd paraméter a felhasználó jelszava, vagy True érték esetén
    a felhasználó root voltát jelzi."""


    # beolvassuk a fájlt
    with open(list_file) as f:
        # a sorait tömbbe mentjük
        program_list = f.read().splitlines()

    # a tömb elejére fűzzük az alábbi elemeket
    program_list.insert(0, "apt-get")
    program_list.insert(1, "install")
    program_list.insert(2, "-y")
    program_list.insert(3, "-q=2")

    program_list_sudo = program_list

    # hozzáfűzzük a sudo-t a nem root felhasználónak
    program_list_sudo.insert(0, "sudo")
    program_list_sudo.insert(1, "-S")

    # lekérdezzük az ubuntu-restricted-extras metacsomag tartalmát
    show = subprocess.Popen(["apt-cache", "show",
        "ubuntu-restricted-extras"], stdout=subprocess.PIPE)
    grep = subprocess.Popen(["grep", "Recommends:"], stdin=show.stdout,
        stdout=subprocess.PIPE)
    out = grep.stdout.read()

    # tömbbé alakítjuk a kivágott Recommends sort
    temp_list = out.split()

    # bejárjuk a tömböt
    for item in temp_list:
        # ha az elem nem tartalmazza a "Recommends"-t és az "mscorefonts"-t
        if item.find("Recommends") == -1 and item.find("mscorefonts") == -1:
            # kivesszük a vesszőket
            item = item.replace(",", "")

            # ha az elem tartalmazza a libavcodec-extra-t
            # pl. libavcodec-extra-54
            if item.find("libavcodec-extra") != -1:
                # azt adjuk hozzá, a nevet követő szám nélkül
                new_item = "libavcodec-extra"
                program_list.append(new_item)

            # különben
            else:
                # hozzáadjuk a program_list tömbhöz
                program_list.append(item)

    # lekérdezzük az ubuntu-restricted-addons metacsomag tartalmát
    show = subprocess.Popen(["apt-cache", "show", "ubuntu-restricted-addons"],
        stdout=subprocess.PIPE)
    grep = subprocess.Popen(["grep", "Recommends:"], stdin=show.stdout,
        stdout=subprocess.PIPE)
    out = grep.stdout.read()

    # tömbbé alakítjuk a kivágott Recommends sort
    temp_list = out.split()

    # bejárjuk a tömböt
    for item in temp_list:
        # ha az elem nem tartalmazza a "Recommends"-t és a "flash"-t
        if item.find("Recommends") == -1 and item.find("flash") == -1:
            # kivesszük a vesszőket
            item = item.replace(",", "")
            # hozzáadjuk a program_list tömbhöz
            program_list.append(item)


    # a felhasználó root
    if passwd == True:
        # frissítjük a csomaglistát
        command = subprocess.Popen(["apt-get", "update", "-y"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        update_return = command.returncode

        # ha sikeresen frissült, telepítjük a programokat
        if update_return == 0:
            # telepítjük a programot
            command = subprocess.Popen(program_list, stdin=subprocess.PIPE,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            communicate = command.communicate()
            returncode = command.returncode

            if returncode == 0:
                # a program sikeresen települt
                return 0

            else:
                # a program telepítése sikertelen
                return 1

        # különben nem sikertelen a csomaglista frissítés
        else:
            return 2

    # a felhasználó nem root
    else:
        # sudo jelszó ellenőrzése
        sudo = subprocess.Popen(["sudo", "-S", "echo", "teszt"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        sudo_communicate = sudo.communicate(passwd + '\n')
        sudo_return = sudo.returncode

        # ha a sudo hitelesítés sikeres
        if sudo_return == 0:
            # frissítjük a csomaglistát
            command = subprocess.Popen(["sudo", "-S", "apt-get", "update",
                "-y"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            update_return = command.returncode

            # ha sikeresen frissült, telepítjük a programokat
            if update_return == 0:
                # futtatjuk a program telepítést
                command = subprocess.Popen(program_list_sudo,
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE)
                communicate = command.communicate(passwd + '\n')
                returncode = command.returncode

                # felülírjuk a felhasználó jelszavát
                passwd = os.urandom(len(passwd))

                if returncode == 0:
                    # a program sikeresen települt
                    return 0

                else:
                    # a program telepítése sikertelen
                    return 1

            # különben sikertelen a csomaglista frissítés
            else:
                return 2

        # ha a sudo hitelesítés sikertelen
        else:
            return 3


def install_package_from_file(list_file, passwd):
    """Fájlban lévő lista alapján telepít csomagokat.

    Első paramétere, a list_file, a listát tartalmazó fájl elérési útja.
    A passwd paraméter a felhasználó jelszava, vagy True érték esetén
    a felhasználó root voltát jelzi."""

    # beolvassuk a fájlt
    with open(list_file) as f:
        # a sorait tömbbe mentjük
        program_list = f.read().splitlines()

    # a tömb elejére fűzzük az alábbi elemeket
    program_list.insert(0, "apt-get")
    program_list.insert(1, "install")
    program_list.insert(2, "-y")
    program_list.insert(3, "-q=2")

    program_list_sudo = program_list

    # hozzáfűzzük a sudo-t a nem root felhasználónak
    program_list_sudo.insert(0, "sudo")
    program_list_sudo.insert(1, "-S")

    # a felhasználó root
    if passwd == True:
        # frissítjük a csomaglistát
        command = subprocess.Popen(["apt-get", "update", "-y"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        communicate = command.communicate()
        update_return = command.returncode

        # ha sikeresen frissült, telepítjük a programokat
        if update_return == 0:
            # telepítjük a programot
            command = subprocess.Popen(program_list, stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            returncode = command.returncode

            if returncode == 0:
                # a program sikeresen települt
                return 0

            else:
                # a program telepítése sikertelen
                return 1

        # különben nem sikertelen a csomaglista frissítés
        else:
            return 2

    # a felhasználó nem root
    else:
        # sudo jelszó ellenőrzése
        sudo = subprocess.Popen(["sudo", "-S", "echo", "teszt"],
            stdin=subprocess.PIPE, stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        sudo_communicate = sudo.communicate(passwd + '\n')
        sudo_return = sudo.returncode

        # ha a sudo hitelesítés sikeres
        if sudo_return == 0:
            # frissítjük a csomaglistát
            command = subprocess.Popen(["sudo", "-S", "apt-get", "update",
                "-y"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            communicate = command.communicate()
            update_return = command.returncode

            # ha sikeresen frissült, telepítjük a programokat
            if update_return == 0:
                # futtatjuk a program telepítést
                command = subprocess.Popen(program_list_sudo,
                    stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE)
                communicate = command.communicate(passwd + '\n')
                returncode = command.returncode

                # felülírjuk a felhasználó jelszavát
                passwd = os.urandom(len(passwd))

                if returncode == 0:
                    # a program sikeresen települt
                    return 0

                else:
                    # a program telepítése sikertelen
                    return 1

            # különben sikertelen a csomaglista frissítés
            else:
                return 2

        # ha a sudo hitelesítés sikertelen
        else:
            return 3
