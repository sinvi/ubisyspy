#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület második menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
import os

import curses
from curses_helpers import curses_functions as cf  # curses függvények
from package_handling import get_installed_packages as gip
from package_handling import install_package as ip
from package_handling import mirrors_ppas as mp  # tükör és PPA kezelés
from system import sudo


locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def second_menu():
    """A második menü almenüjét és azok menüpontjait valósítja meg."""

    x = 0
    # amíg a felhasználó által megadott szám nem 7, ami az almenüpontok száma
    while x != ord('7'):
        screen = curses.initscr()   # új ablakot inicializál
        curses.start_color()    # lehetővé teszi a színek használatát
        # színpár def.
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # színpár def.
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()    # képernyő frissítése

        # az almenü elemei
        items = [u"UbiSysPy – Csomagkezelés menü",
                 u"Szám billentyű megnyomásával válassz az alábbi "
                 u"menüpontok közül:",
                 u"1) Alapvető programok telepítése",
                 u"2) Programok telepítése lista alapján",
                 u"3) Fontosabb programok állapotának lekérdezése",
                 u"4) Jelenleg használt tükör lekérdezése és módosítása",
                 u"5) Jelenleg használt PPA-k lekérdezése",
                 u"6) PPA hozzáadása",
                 u"7) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, items[0].encode("utf-8"), curses.A_BOLD |
                      curses.color_pair(2))
        screen.addstr(5, 2, items[1].encode("utf-8"), curses.A_UNDERLINE |
                      curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))
        screen.addstr(11, 4, items[6].encode("utf-8"))
        screen.addstr(12, 4, items[7].encode("utf-8"))
        screen.addstr(13, 4, items[8].encode("utf-8"))

        screen.refresh()

        x = screen.getch()  # várjuk a felhasználó bemenetét

        if x == ord('1'):   # ha 1, végrehajtjuk a parancsot
            curses.endwin()

            # menüpont neve és kiírandó sorok
            item = "UbiSysPy – Csomagkezelés menü – Alapvető programok telepítése"
            items = ["Az alábbi programok kerülnek telepítésre:",
            "Ubuntu korlátozott extrák (Flash és msttcorefonts nélkül), "
            "Firefox,",
            "Chromium, Thunderbird, LibreOffice, Pluma, Nemo, p7zip (teljes),",
            "KeepassX, bpython, gParted, Synaptic, gDebi, VLC, GIMP, Meld, "
            "htop,",
            "iotop, Inkscape, qBittorrent, Lemezek, Simple Scan, Atril, "
            "Psensor,",
            "pwgen, Aknakereső, Sudoku, MATE segédprogramok.",
            "A telepítés hosszabb időt is igénybe vehet.",
            "",
            "Szeretnéd folytatni? (igen/nem)"]

            # válasz bekérése
            answer = cf.get_param_with_list(item, items)

            # ha nem válaszolt
            if not answer:
                message = "Hiba: Nem adtál meg választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha a válasz nem
            elif answer == "nem" or answer == "Nem":
                message = "Nem folytatjuk a műveletet."
                # kiírjuk az üzenetet
                cf.print_string(item, message)

            # ha igennel válaszolt
            elif answer == "igen" or answer == "Igen":
                # az aktuális script könyvtárának lekérdezése
                current_dir = os.path.dirname(os.path.abspath(__file__))
                # a script könyvtárának szülőkönyvtára
                current_parent = os.path.dirname(current_dir)
                # a telepítendő csomagokat tartalmazó fájl helyének
                # megalkotása a szülő könyvtárból
                file_path = \
                    current_parent + "/package_handling/important_programs.txt"

                # root ellenőrzés
                return_value = sudo.check_root()

                # ha nullával tér vissza, a felhasználó root
                if return_value == 0:
                    # meghívjuk a telepítő függvényvt
                    return_code = ip.install_important_packages(file_path, True)

                    # ha nullával tér vissza, sikeres volt
                    if return_code == 0:
                        message = "A programok telepítése sikeres volt."
                        # kiírjuk az üzenetet
                        cf.print_string(item, message)

                    # ha kettővel tér vissza, sikertelen a csomaglista frissítés
                    elif return_code == 2:
                        message = "A csomaglista frissítése sikertelen."
                        # kiírjuk az üzenetet
                        cf.print_string(item, message, red=True)

                    # különben:
                    else:
                        message = "A programok telepítése sikertelen."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif return_value == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif return_value == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # különben
                    else:
                        # meghívjuk a telepítő függvényvt
                        return_code = ip.install_important_packages(
                            file_path, passwd)

                        # ha nullával tér vissza, sikeres volt
                        if return_code == 0:
                            message = "A programok telepítése sikeres volt."
                            # kiírjuk az üzenetet
                            cf.print_string(item, message)

                        # ha eggyel tér vissza, sikertelen
                        elif return_code == 1:
                            message = "A programok telepítése sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha kettővel tér vissza, sikertelen a csomaglista
                        # frissítés
                        elif return_code == 2:
                            message = "A csomaglista frissítése sikertelen."
                            # kiírjuk az üzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, auth. hiba
                        elif return_code == 3:
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))


            # ha a válasz ismeretlen
            else:
                message = "Hiba: Nem adtál meg felismerhető választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)


        elif x == ord('2'):
            curses.endwin()
            # menüpont neve
            item = "UbiSysPy – Csomagkezelés menü – Programok telepítése lista " \
                   "alapján"
            # kiírndó szöveg
            items = ["Lehetőséged van lista alapján történő csomagtelepítésre.",
            "Ehhez szükséged van egy szöveges fájlra, amelyben soronként egy",
            "darab csomagnév szerepel.",
            "Fontos, hogy ne programneveket, hanem csomagneveket tartalmazzon",
            "a fájl (pl. eom Eye of MATE helyett)."]
            # záró üzenet
            finish = "Nyomj entert a fájl helyének megadásához"

            # tájékoztató szöveg a fájlhoz
            cf.print_list(item, items, finish=finish)

            # a menüpont elemei és a leírás
            items_path = ["UbiSysPy – Csomagkezelés menü – Programok telepítése "
                          "lista alapján",
                          "Add meg, hol van a telepítendő alkalmazások "
                          "listája.",
                          "Például: "
                          "/home/user/Asztal/csomagjaim.txt"]

            # bekérjük az útvonalat a felhasználótól
            file_path = cf.get_param(items_path)

            # ha a sztring üres (=False), nincs elérési út
            if not file_path:
                message = "Hiba: Nem adtál meg elérési utat."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha a megadott úton nincs fájl
            elif not os.path.isfile(file_path):
                # hibaüzenet összeillesztése
                message = ["Hiba: A megadott elérési úton nincs fájl:",
                           file_path]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            # különben
            else:
                # root ellenőrzés
                return_value = sudo.check_root()

                # ha nullával tér vissza, a felhasználó root
                if return_value == 0:
                    # meghívjuk a telepítő függvényvt
                    return_code = ip.install_package_from_file(file_path, True)

                    # ha nullával tér vissza, sikeres volt
                    if return_code == 0:
                        message = "A programok telepítése sikeres volt."
                        # kiírjuk az üzenetet
                        cf.print_string(item, message)

                    # ha kettővel tér vissza, sikertelen a csomaglista frissítés
                    elif return_code == 2:
                        message = "A csomaglista frissítése sikertelen."
                        # kiírjuk az üzenetet
                        cf.print_string(item, message, red=True)

                    # különben:
                    else:
                        message = "A programok telepítése sikertelen."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif return_value == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif return_value == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # különben
                    else:
                        # meghívjuk a telepítő függvényvt
                        return_code = ip.install_package_from_file(file_path,
                            passwd)

                        # ha nullával tér vissza, sikeres volt
                        if return_code == 0:
                            message = "A programok telepítése sikeres volt."
                            # kiírjuk az üzenetet
                            cf.print_string(item, message)

                        # ha eggyel tér vissza, sikertelen
                        elif return_code == 1:
                            message = "A programok telepítése sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha kettővel tér vissza, sikertelen a csomaglista
                        # frissítés
                        elif return_code == 2:
                            message = "A csomaglista frissítése sikertelen."
                            # kiírjuk az üzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, auth. hiba
                        elif return_code == 3:
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))


        elif x == ord('3'):
            curses.endwin()
            item = "UbiSysPy – Csomagkezelés menü – Fontosabb programok " \
                   "állapotának lekérdezése"

            # lekérdezzük a fontosabb programok állapotát, verzióját
            cf.print_function_without_args(item, gip.important_program_list)

        elif x == ord('4'):
            item = "UbiSysPy – Csomagkezelés menü – Jelenleg használt tükör " \
                   "lekérdezése"
            curses.endwin()

            finish = "Nyomj entert a folytatáshoz"
            # lekérdezzük és kiírjuk  a jelenlegi tükröt
            cf.print_function_without_args(item, mp.get_current_mirror,
                finish=finish)

            # a jelenlegi tükör a tömb második eleme, elmentjük
            current = mp.get_current_mirror()[1]

            items = ["UbiSysPy – Csomagkezelés menü – Jelenleg használt tükör "
                     "módosítása",
                     "Lecserélheted a jelenlegi tükröt, ha problémád van vele.",
                     "Szeretnéd módosítani az aktuális tükröt? (igen/nem)"]
            # megkérdezzük, hogy lecseréli-e
            answer = cf.get_param(items)

            # ha nem válaszolt
            if not answer:
                message = "Hiba: Nem adtál meg választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha igennel válaszolt
            elif answer == "igen" or answer == "Igen":
                item = "UbiSysPy – Csomagkezelés menü – Jelenleg használt tükör " \
                       "módosítása"
                items = ["A választható lehetőségek:",
                         "1) http://ftp.freepark.org/ubuntu",
                         "2) http://ftp.kfki.hu/linux/ubuntu",
                         "3) http://ubuntu.sth.sze.hu/ubuntu",
                         "4) http://archive.ubuntu.com/ubuntu",
                         "5) automatikus tükörválasztás",
                         "",
                         "Add meg a kiválasztott új tükör sorszámát:"]

                # felhasználó és sudo jog ellenőrzése
                # ha 0-val tér vissza, a user root
                if sudo.check_root() == 0:
                    # bekérjük az új tükröt
                    mirror = cf.get_param_with_list(item, items)

                    # ha nem adott meg tükröt
                    if not mirror:
                        message = "Nem választottál tükröt."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    elif mirror == "1":
                        new = "http://ftp.freepark.org/ubuntu"

                        # meghívjuk az mp.set_mirrort a régi és az új
                        # tükörrel
                        return_code = mp.set_mirror(current, new, True)

                        # ha kettővel tért vissza, nem sikerült a
                        # sources.list visszamásolása
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/apt/sources.list " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/apt/sources.list " \
                                      "jogosultságaink beállítása " \
                                      "sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            message = ["A tükör sikeresen módosításra "
                                       "került.",
                            "A változtatások életbe lépéséhez "
                            "szükséges a csomaglista frissítése.",
                            "Ez néhány percet is igénybe vehet."]

                            finish = "Nyomj entert a csomaglista " \
                                     "frissítéséhez"
                            # akkor kiírjuk a sikert
                            cf.print_list(item, message, finish=finish)

                            # és frissítjük a csomaglistát
                            # ha az sikeres volt
                            if mp.update_packagelist(True):
                                message = "A csomaglista frissítése " \
                                          "sikeres volt."
                                # kiírjuk annak sikerét
                                cf.print_string(item, message)

                            # ha nem
                            else:
                                message = "A csomaglista frissítése " \
                                          "sikertelen volt."

                                # kiírjuk a sikertelenséget
                                cf.print_string(item, message, red=True)

                        # ha nem sikerült a tükör módosítása
                        else:
                            message = "A tükör módosítása sikertelen."
                            # kiírjuk a sikertelenséget
                            cf.print_string(item, message, red=True)

                    elif mirror == "2":
                        new = "http://ftp.kfki.hu/linux/ubuntu"

                        # meghívjuk az mp.set_mirrort a régi és az új
                        # tükörrel
                        return_code = mp.set_mirror(current, new, True)

                        # ha kettővel tért vissza, nem sikerült a
                        # sources.list visszamásolása
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/apt/sources.list " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/apt/sources.list " \
                                      "jogosultságaink beállítása " \
                                      "sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            message = ["A tükör sikeresen módosításra "
                                       "került.",
                            "A változtatások életbe lépéséhez "
                            "szükséges a csomaglista frissítése.",
                            "Ez néhány percet is igénybe vehet."]

                            finish = "Nyomj entert a csomaglista " \
                                     "frissítéséhez"
                            # akkor kiírjuk a sikert
                            cf.print_list(item, message, finish=finish)

                            # és frissítjük a csomaglistát
                            # ha az sikeres volt
                            if mp.update_packagelist(True):
                                message = "A csomaglista frissítése " \
                                          "sikeres volt."
                                # kiírjuk annak sikerét
                                cf.print_string(item, message)

                            # ha nem
                            else:
                                message = "A csomaglista frissítése " \
                                          "sikertelen volt."

                                # kiírjuk a sikertelenséget
                                cf.print_string(item, message, red=True)

                        # ha nem sikerült a tükör módosítása
                        else:
                            message = "A tükör módosítása sikertelen."
                            # kiírjuk a sikertelenséget
                            cf.print_string(item, message, red=True)

                    elif mirror == "3":
                        new = "http://ubuntu.sth.sze.hu/ubuntu"

                        # meghívjuk az mp.set_mirrort a régi és az új
                        # tükörrel
                        return_code = mp.set_mirror(current, new, True)

                        # ha kettővel tért vissza, nem sikerült a
                        # sources.list visszamásolása
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/apt/sources.list " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/apt/sources.list " \
                                      "jogosultságaink beállítása " \
                                      "sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            message = ["A tükör sikeresen módosításra "
                                       "került.",
                            "A változtatások életbe lépéséhez "
                            "szükséges a csomaglista frissítése.",
                            "Ez néhány percet is igénybe vehet.",]

                            finish = "Nyomj entert a csomaglista " \
                                     "frissítéséhez"
                            # akkor kiírjuk a sikert
                            cf.print_list(item, message, finish=finish)

                            # és frissítjük a csomaglistát
                            # ha az sikeres volt
                            if mp.update_packagelist(True):
                                message = "A csomaglista frissítése " \
                                          "sikeres volt."
                                # kiírjuk annak sikerét
                                cf.print_string(item, message)

                            # ha nem
                            else:
                                message = "A csomaglista frissítése " \
                                          "sikertelen volt."
                                # kiírjuk a sikertelenséget
                                cf.print_string(item, message, red=True)

                        # ha nem sikerült a tükör módosítása
                        else:
                            message = "A tükör módosítása sikertelen."
                            # kiírjuk a sikertelenséget
                            cf.print_string(item, message, red=True)

                    elif mirror == "4":
                        new = "http://archive.ubuntu.com/ubuntu"

                        # meghívjuk az mp.set_mirrort a régi és az új
                        # tükörrel
                        return_code = mp.set_mirror(current, new, True)

                        # ha kettővel tért vissza, nem sikerült a
                        # sources.list visszamásolása
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/apt/sources.list " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/apt/sources.list " \
                                      "jogosultságaink beállítása " \
                                      "sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            message = ["A tükör sikeresen módosításra "
                                       "került.",
                            "A változtatások életbe lépéséhez "
                            "szükséges a csomaglista frissítése.",
                            "Ez néhány percet is igénybe vehet."]
                            finish = "Nyomj entert a csomaglista " \
                                     "frissítéséhez"
                            # akkor kiírjuk a sikert
                            cf.print_list(item, message, finish=finish)

                            # és frissítjük a csomaglistát
                            # ha az sikeres volt
                            if mp.update_packagelist(True):
                                message = "A csomaglista frissítése " \
                                          "sikeres volt."
                                # kiírjuk annak sikerét
                                cf.print_string(item, message)

                            # ha nem
                            else:
                                message = "A csomaglista frissítése " \
                                          "sikertelen volt."
                                # kiírjuk a sikertelenséget
                                cf.print_string(item, message, red=True)

                        # ha nem sikerült a tükör módosítása
                        else:
                            message = "A tükör módosítása sikertelen."
                            # kiírjuk a sikertelenséget
                            cf.print_string(item, message, red=True)

                    elif mirror == "5":
                        new = "mirror://mirrors.ubuntu.com/mirrors.txt"

                        # meghívjuk az mp.set_mirrort a régi és az új
                        # tükörrel
                        return_code = mp.set_mirror(current, new, True)

                        # ha kettővel tért vissza, nem sikerült a
                        # sources.list visszamásolása
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/apt/sources.list " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/apt/sources.list " \
                                      "jogosultságaink beállítása " \
                                      "sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            message = ["A tükör sikeresen módosításra "
                                       "került.",
                            "A változtatások életbe lépéséhez "
                            "szükséges a csomaglista frissítése.",
                            "Ez néhány percet is igénybe vehet."]
                            finish = "Nyomj entert a csomaglista " \
                                     "frissítéséhez"
                            # akkor kiírjuk a sikert
                            cf.print_list(item, message, finish=finish)

                            # és frissítjük a csomaglistát
                            # ha az sikeres volt
                            if mp.update_packagelist(True):
                                message = "A csomaglista frissítése " \
                                          "sikeres volt."
                                # kiírjuk annak sikerét
                                cf.print_string(item, message)

                            # ha nem
                            else:
                                message = "A csomaglista frissítése " \
                                        "sikertelen volt."
                                # kiírjuk a sikertelenséget
                                cf.print_string(item, message, red=True)

                        # ha nem sikerült a tükör módosítása
                        else:
                            message = "A tükör módosítása sikertelen."
                            # kiírjuk a sikertelenséget
                            cf.print_string(item, message, red=True)

                    else:
                        message = "Hiba: Nem adtál meg felismerhető választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)


                # ha 2-vel tér vissza, nincs sudo joga
                elif sudo.check_root() == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif sudo.check_root() == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha adott meg jelszót, teszteljük
                    else:
                        # jelszó tesztelése
                        # ha Hamissal tér vissza, hibás a jelszó
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                        # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            # bekérjük az új tükröt
                            mirror = cf.get_param_with_list(item, items)

                            finish = "Nyomj entert a csomaglista frissítéséhez"

                            # ha nem adott meg tükröt
                            if not mirror:
                                message = "Nem választottál tükröt."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            elif mirror == "1":
                                new = "http://ftp.freepark.org/ubuntu"

                                # meghívjuk az mp.set_mirrort a régi és az új
                                # tükörrel
                                return_code = mp.set_mirror(current, new,
                                    passwd)

                                # ha kettővel tért vissza, nem sikerült a
                                # sources.list visszamásolása
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/apt/sources.list " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/apt/sources.list " \
                                              "jogosultságaink " \
                                              "beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    message = ["A tükör sikeresen módosításra "
                                               "került.",
                                    "A változtatások életbe lépéséhez "
                                    "szükséges a csomaglista frissítése.",
                                    "Ez néhány percet is igénybe vehet."]

                                    finish = "Nyomj entert a csomaglista " \
                                             "frissítéséhez"
                                    # akkor kiírjuk a sikert
                                    cf.print_list(item, message, finish=finish)

                                    # és frissítjük a csomaglistát
                                    # ha az sikeres volt
                                    if mp.update_packagelist(passwd):
                                        message = "A csomaglista frissítése " \
                                                  "sikeres volt."
                                        # kiírjuk annak sikerét
                                        cf.print_string(item, message)

                                    # ha nem
                                    else:
                                        message = "A csomaglista frissítése  " \
                                                  "sikertelen volt."

                                        # kiírjuk a sikertelenséget
                                        cf.print_string(item, message, red=True)

                                # ha nem sikerült a tükör módosítása
                                else:
                                    message = "A tükör módosítása sikertelen."
                                    # kiírjuk a sikertelenséget
                                    cf.print_string(item, message, red=True)

                            elif mirror == "2":
                                new = "http://ftp.kfki.hu/linux/ubuntu"

                                # meghívjuk az mp.set_mirrort a régi és az új
                                # tükörrel
                                return_code = mp.set_mirror(current, new,
                                    passwd)

                                # ha kettővel tért vissza, nem sikerült a
                                # sources.list visszamásolása
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/apt/sources.list " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/apt/sources.list " \
                                              "jogosultságaink " \
                                              "beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    message = ["A tükör sikeresen módosításra "
                                               "került.",
                                    "A változtatások életbe lépéséhez "
                                    "szükséges a csomaglista frissítése.",
                                    "Ez néhány percet is igénybe vehet."]

                                    finish = "Nyomj entert a csomaglista " \
                                             "frissítéséhez"
                                    # akkor kiírjuk a sikert
                                    cf.print_list(item, message, finish=finish)

                                    # és frissítjük a csomaglistát
                                    # ha az sikeres volt
                                    if mp.update_packagelist(passwd):
                                        message = "A csomaglista frissítése " \
                                                  "sikeres volt."
                                        # kiírjuk annak sikerét
                                        cf.print_string(item, message)

                                    # ha nem
                                    else:
                                        message = "A csomaglista frissítése " \
                                                  "sikertelen volt."

                                        # kiírjuk a sikertelenséget
                                        cf.print_string(item, message, red=True)

                                # ha nem sikerült a tükör módosítása
                                else:
                                    message = "A tükör módosítása sikertelen."
                                    # kiírjuk a sikertelenséget
                                    cf.print_string(item, message, red=True)

                            elif mirror == "3":
                                new = "http://ubuntu.sth.sze.hu/ubuntu"

                                # meghívjuk az mp.set_mirrort a régi és az új
                                # tükörrel
                                return_code = mp.set_mirror(current, new,
                                    passwd)

                                # ha kettővel tért vissza, nem sikerült a
                                # sources.list visszamásolása
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/apt/sources.list " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/apt/sources.list " \
                                              "jogosultságaink " \
                                              "beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    message = ["A tükör sikeresen módosításra "
                                               "került.",
                                    "A változtatások életbe lépéséhez "
                                    "szükséges a csomaglista frissítése.",
                                    "Ez néhány percet is igénybe vehet.",]

                                    finish = "Nyomj entert a csomaglista " \
                                             "frissítéséhez"
                                    # akkor kiírjuk a sikert
                                    cf.print_list(item, message, finish=finish)

                                    # és frissítjük a csomaglistát
                                    # ha az sikeres volt
                                    if mp.update_packagelist(passwd):
                                        message = "A csomaglista frissítése " \
                                                  "sikeres volt."
                                        # kiírjuk annak sikerét
                                        cf.print_string(item, message)

                                    # ha nem
                                    else:
                                        message = "A csomaglista frissítése " \
                                                  "sikertelen volt."
                                        # kiírjuk a sikertelenséget
                                        cf.print_string(item, message, red=True)

                                # ha nem sikerült a tükör módosítása
                                else:
                                    message = "A tükör módosítása sikertelen."
                                    # kiírjuk a sikertelenséget
                                    cf.print_string(item, message, red=True)

                            elif mirror == "4":
                                new = "http://archive.ubuntu.com/ubuntu"

                                # meghívjuk az mp.set_mirrort a régi és az új
                                # tükörrel
                                return_code = mp.set_mirror(current, new,
                                    passwd)

                                # ha kettővel tért vissza, nem sikerült a
                                # sources.list visszamásolása
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/apt/sources.list " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/apt/sources.list " \
                                              "jogosultságaink " \
                                              "beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    message = ["A tükör sikeresen módosításra "
                                               "került.",
                                    "A változtatások életbe lépéséhez "
                                    "szükséges a csomaglista frissítése.",
                                    "Ez néhány percet is igénybe vehet."]
                                    finish = "Nyomj entert a csomaglista " \
                                             "frissítéséhez"
                                    # akkor kiírjuk a sikert
                                    cf.print_list(item, message, finish=finish)

                                    # és frissítjük a csomaglistát
                                    # ha az sikeres volt
                                    if mp.update_packagelist(passwd):
                                        message = "A csomaglista frissítése " \
                                                  "sikeres volt."
                                        # kiírjuk annak sikerét
                                        cf.print_string(item, message)

                                    # ha nem
                                    else:
                                        message = "A csomaglista frissítése " \
                                                  "sikertelen volt."
                                        # kiírjuk a sikertelenséget
                                        cf.print_string(item, message, red=True)

                                # ha nem sikerült a tükör módosítása
                                else:
                                    message = "A tükör módosítása sikertelen."
                                    # kiírjuk a sikertelenséget
                                    cf.print_string(item, message, red=True)

                            elif mirror == "5":
                                new = "mirror://mirrors.ubuntu.com/mirrors.txt"

                                # meghívjuk az mp.set_mirrort a régi és az új
                                # tükörrel
                                return_code = mp.set_mirror(current, new,
                                    passwd)

                                # ha kettővel tért vissza, nem sikerült a
                                # sources.list visszamásolása
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/apt/sources.list " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/apt/sources.list " \
                                              "jogosultságaink " \
                                              "beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    message = ["A tükör sikeresen módosításra "
                                               "került.",
                                    "A változtatások életbe lépéséhez "
                                    "szükséges a csomaglista frissítése.",
                                    "Ez néhány percet is igénybe vehet."]
                                    finish = "Nyomj entert a csomaglista " \
                                             "frissítéséhez"
                                    # akkor kiírjuk a sikert
                                    cf.print_list(item, message, finish=finish)

                                    # és frissítjük a csomaglistát
                                    # ha az sikeres volt
                                    if mp.update_packagelist(passwd):
                                        message = "A csomaglista frissítése " \
                                                  "sikeres volt."
                                        # kiírjuk annak sikerét
                                        cf.print_string(item, message)

                                    # ha nem
                                    else:
                                        message = "A csomaglista frissítése " \
                                                "sikertelen volt."
                                        # kiírjuk a sikertelenséget
                                        cf.print_string(item, message, red=True)

                                # ha nem sikerült a tükör módosítása
                                else:
                                    message = "A tükör módosítása sikertelen."
                                    # kiírjuk a sikertelenséget
                                    cf.print_string(item, message, red=True)

                            else:
                                message = "Hiba: Nem adtál meg felismerhető " \
                                          "választ."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))

            # ha a válasz nem
            elif answer == "nem" or answer == "Nem":
                message = "Nem cseréljük le a tükröt."
                # kiírjuk az üzenetet
                cf.print_string(item, message)

            # ha a válasz ismeretlen
            else:
                message = "Hiba: Nem adtál meg felismerhető választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)


        elif x == ord('5'):
            curses.endwin()
            item = "UbiSysPy – Csomagkezelés menü – Jelenleg használt PPA-k " \
                   "lekérdezése"
            cf.print_function_without_args(item, mp.get_ppas)


        elif x == ord('6'):
            curses.endwin()
            item = "UbiSysPy – Csomagkezelés menü – PPA hozzáadása"
            items = ["Add meg a hozzáadni kívánt PPA-t "
                    "ppa:user/ppa-name formában,",
                    "hogy a tárolóból nem elérhető csomagot telepíthess.",
                    "Például: ppa:ubuntu-mozilla-security/ppa",
                    "A PPA hozzáadása és a csomaglista frissítése "
                    "eltarhat egy ideig."]

            # felhasználó és sudo jog ellenőrzése
            return_value = sudo.check_root()

            # ha 0-val tér vissza, a user root
            if return_value == 0:
                # bekérjük a PPA-t
                ppa = cf.get_param_with_list(item, items)

                # ha nem adott meg PPA-t:
                if not ppa:
                    message = "Hiba: Nem adtál meg jelszót."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha adott meg PPA-t:
                else:
                    # PPA név ellenőrzése
                    if ppa.find("ppa") == -1 or ppa.find(":") == -1 or \
                                    ppa.find("/") == -1:
                        message = "A PPA formátuma nem felismerhető."
                        cf.print_string(item, message, red=True)

                    # ha megfelel a PPA formátuma
                    else:
                        # meghívjuk az add_ppa()-t
                        returnvalue = mp.add_ppa(ppa, True)
                        # ha 0-val tér vissza, sikeres volt a művelet
                        if returnvalue == 0:
                            message = "A PPA hozzáadása és a csomaglista " \
                                      "frissítése sikeres."
                            cf.print_string(item, message)

                        # ha 1-gyel tér vissza, nem sikerült az update
                        elif returnvalue == 1:
                            message = "A PPA hozzádása után a " \
                                      "csomaglista frissítése sikertelen."
                            cf.print_string(item, message)

                        # ha 2-vel tér vissza, sikertelen a hozzáadás
                        else:
                            message = "A PPA hozzáadása sikertelen."
                            cf.print_string(item, message, red=True)

            # ha 2-vel tér vissza, nincs sudo joga
            elif return_value == 2:
                message = "Nincs sudo jogod, így a művelet nem folytatható."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha 1-gyel tér vissza, van sudo joga
            elif return_value == 1:
                # bekérő szöveg
                prompt_str = "Add meg a jelszavad:"
                # leírás a felhasználónak
                descr = "A következő művelethez root jogra lesz szükséged."
                # jelszó bekérése a tükörváltáshoz
                passwd = cf.get_pass(item, descr, prompt_str)

                # ha a felhasználó nem adott meg jelszót
                if not passwd:
                    message = "Hiba: Nem adtál meg jelszót."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha adott meg jelszót, teszteljük
                else:
                    # jelszó tesztelése
                    # ha Hamissal tér vissza, hibás a jelszó
                    if not sudo.sudo_test(passwd):
                        message = "Hitelesítési hiba. Elírhattad a jelszavadat."
                        cf.print_string(item, message, red=True)

                    # ha igazzal tér vissza, rendben van a jelszó
                    else:
                        # bekérjük a PPA-t
                        ppa = cf.get_param_with_list(item, items)

                        # ha nem adott meg PPA-t:
                        if not ppa:
                            message = "Hiba: Nem adtál meg jelszót."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha adott meg PPA-t:
                        else:
                            # PPA név ellenőrzése
                            if ppa.find("ppa") == -1 or ppa.find(":") == -1 or \
                                            ppa.find("/") == -1:
                                message = "A PPA formátuma nem felismerhető."
                                cf.print_string(item, message, red=True)

                            # ha megfelel a PPA formátuma
                            else:
                                # meghívjuk az add_ppa()-t
                                returnvalue = mp.add_ppa(ppa, passwd)
                                # ha 0-val tér vissza, sikeres volt a művelet
                                if returnvalue == 0:
                                    message = "A PPA hozzáadása és a " \
                                              "csomaglista frissítése sikeres."
                                    cf.print_string(item, message)

                                # ha 1-gyel tér vissza, nem sikerült az update
                                elif returnvalue == 1:
                                    message = "A PPA hozzádása után a " \
                                              "csomaglista frissítése " \
                                              "sikertelen."
                                    cf.print_string(item, message)

                                # ha 2-vel tér vissza, sikertelen a hozzáadás
                                else:
                                    message = "A PPA hozzáadása sikertelen."
                                    cf.print_string(item, message, red=True)

                # felülírjuk a felhasználó jelszavát
                passwd = os.urandom(len(passwd))
