#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület első menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
import os
import shutil
import curses

from curses_helpers import curses_scroll as cs
from curses_helpers import curses_functions as cf  # curses függvények
from dependency import depscanner as d  # függőség ellenőrzés
from package_handling import get_installed_packages as gip
from package_handling import install_package as ip
from system import memoria  # memória információ
from system import smart  # SMART lekérdezés
from system import sys_details as sd   # rendszeradat lekérdezés
from system import sudo

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def first_menu():
    """
    Az első menüt valósítja meg és azok menüpontjait hívja meg máshonnan.
    """

    x = 0
    # amíg a felhasználó által megadott szám nem 7, ami az almenüpontok száma
    while x != ord('7'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # első színpár definiálás
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # második színpár definiálás
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # az almenü elemei
        items = ["UbiSysPy – Rendszeradatok menü",
                 "Szám billentyű megnyomásával válassz az alábbi menüpontok "
                 "közül:",
                 "1) Rendszeradatok lekérdezése",
                 "2) Rendszeradatok mentése",
                 "3) Telepített alkalmazások listájának lekérdezése",
                 "4) Alkalmazások visszaállítása listából",
                 "5) Adathordozók adatainak lekérdezése",
                 "6) Memóriaadatok lekérdezése",
                 "7) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()  # tisztítjuk a képernyőt
        screen.border(0)  # margók beállítása
        # a programnév és a menüpont kiírása
        screen.addstr(2, 2, items[0], curses.A_BOLD | curses.color_pair(2))
        # felhasználói tájékoztató kiírása a számvezérlésről
        screen.addstr(5, 2, items[1], curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2])
        screen.addstr(8, 4, items[3])
        screen.addstr(9, 4, items[4])
        screen.addstr(10, 4, items[5])
        screen.addstr(11, 4, items[6])
        screen.addstr(12, 4, items[7])
        screen.addstr(13, 4, items[8])
        screen.refresh()  # frissítünk, hogy megjelenjenek a fenit elemek

        x = screen.getch()  # várjuk a felhasználó bemenetét

        if x == ord('1'):  # ha a felhasználó az 1-es almenüt választja
            curses.endwin()  # befejezzük a cursest
            # a menüpont neve
            item = "UbiSysPy – Rendszeradatok menü – Rendszeradatok lekérdezése"
            # meghívjuk a print_function_without_args()-t, hogy írja ki a
            # rendszeradatokat a sys.details.py results() függvényét használva.
            cf.print_function_without_args(item, sd.results)

        elif x == ord('2'):  # ha a felhasználó 2-es almenüt választja
            # lekérdezzük a mentett rendszeradatok elérési útját
            source_path = sd.write_results()
            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – Rendszeradatok mentése"
            items_path = [
                "UbiSysPy – Rendszeradatok menü – Rendszeradatok mentése",
                "Add meg, hová szeretnéd menteni a rendszeradatokat.",
                "Például: /home/felhasználó/könyvtár/"]

            # bekérjük a mentési útvonalat a felhasználótól
            path = cf.get_param(items_path)

            if not path:  # ha az útvonal nem került megadásra
                message = "Hiba: Nem adtál meg elérési utat."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            elif not os.path.isdir(path):  # ha a könyvtár nem létezik
                curses.endwin()  # befejezzük a cursest
                # hibaüzenet összeillesztése
                message = ["Hiba: A megadott elérési út NEM létezik:", path]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            else:  # ha létezik a megadott könyvtár

                # ha a felhasználónak nincs írási joga a megadott útvonalra
                if not os.access(path, os.W_OK):
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM írható:", path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha a felhasználónak van írási joga
                    # a menüpont elemei
                    items_name = [
                        "UbiSysPy – Rendszeradatok menü – Rendszeradatok mentése",
                        "Add meg, milyen néven szeretnéd menteni a "
                        "rendszeradatokat.",
                        "Alapértelmezetten: rendszeradatok_időbélyeg.txt"]

                    # bekérjük a mentési nevet a felhasználótól
                    name = cf.get_param(items_name)
                    curses.endwin()  # befejezzük a cursest

                    # a mentett rendszeradok fájljának neve
                    file_name = os.path.basename(source_path)

                    # ha a felhasználó nem adott meg fájlnevet
                    if name == "":
                        # akkor a megadott útvonal és az eredeti fájlnév
                        full_path = os.path.join(path, file_name)

                    # ha a felhasználó adott meg fájlnevet, azt használjuk
                    else:
                        # összeillesztjük a cél útvonalat és a fájlnevet
                        # ha adott meg fájlnevet, akkor összefűzzük
                        # a megadott útvonallal
                        full_path = os.path.join(path, name)

                    # megpróbáljuk elvégezni a másolást
                    try:
                        shutil.copy(source_path, full_path)

                    # ha IO hibát kapunk, nem sikerült
                    except IOError:
                        message = ["Az alábbi fájl létrehozása sikertelen "
                                   "(I/O hiba):",
                                  full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha shutil hibát kapunk, szintén nem sikerült
                    except shutil.Error:
                        message = ["Az alábbi fájl létrehozása "
                                   "sikertelen:", full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha OSError-t kapunk, valószínűleg létezik a fájl
                    except OSError:
                        message = ["Az alábbi fájl létrehozása "
                                   "sikertelen:", full_path,
                            "Lehetséges, hogy a megadott fájl már "
                            "létezik."]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # különben
                    else:
                        # tömbbe tessszük a kiírni valót (szöveg és fájlnév)
                        message = ["Az alábbi fájl sikeresen létrejött:",
                                  full_path]
                        # kiírjuk, hová másoltuk a fájlt
                        cf.print_list(item, message)

        elif x == ord('3'):  # ha a felhasználó 3-as almenüt választja
            item = "UbiSysPy – Rendszeradatok menü – Telepített alkalmazások " \
                   "lekérdezése"

            # finish = "Nyomj entert a mentés helyének megadásához."
            curses.endwin()
            # telepített csomagok listájának lekérdezése
            package_list_path = gip.installed_packages_list()

            # a menüpont elemei
            items_path = [
                "UbiSysPy – Rendszeradatok menü – Telepített alkalmazások "
                "lekérdezése",
                "Add meg, hová szeretnéd menteni a telepített alkalmazások "
                "listáját.", "Például: /home/felhasználó/könyvtár/"]

            # bekérjük a mentési útvonalat a felhasználótól
            path = cf.get_param(items_path)

            if not path:  # ha az útvonal nem került megadásra
                message = "Hiba: Nem adtál meg elérési utat."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            elif not os.path.isdir(path):  # ha a könyvtár nem létezik
                curses.endwin()  # befejezzük a cursest
                # hibaüzenet összeillesztése
                message = ["Hiba: A megadott elérési út NEM létezik:", path]
                cf.print_list(item, message, red=True)

            else:  # ha létezik a megadott könyvtár

                # ha a felhasználónak nincs írási joga a megadott útvonalra
                if not os.access(path, os.W_OK):
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM írható:", path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha a felhasználónak van írási joga
                    # a menüpont elemei
                    items_name = ["UbiSysPy – Rendszeradatok menü – Telepített "
                                  "alkalmazások lekérdezése",
                                  "Add meg, milyen néven szeretnéd menteni a "
                                  "rendszeradatokat.", "Alapértelmezetten: "
                                  "telepitett-csomagok_időbélyeg.txt"]

                    # bekérjük a mentési nevet a felhasználótól
                    name = cf.get_param(items_name)
                    curses.endwin()  # befejezzük a cursest

                    # a csomaglista fájlnevének lekérdezése
                    file_name = os.path.basename(package_list_path)

                    # ha a felhasználó nem adott meg fájlnevet
                    if name == "":
                        # akkor a megadott útvonal és az eredeti fájlnév
                        full_path = os.path.join(path, file_name)

                    # különben
                    else:
                        # ha adott meg fájlnevet, akkor összefűzzük a
                        # megadott útvonallal
                        full_path = os.path.join(path, name)

                    # megpróbáljuk elvégezni a másolást
                    try:
                        shutil.copy(package_list_path, full_path)

                    # ha IO hibát kapunk, nem sikerült
                    except IOError:
                        message = ["Az alábbi fájl létrehozása sikertelen: "
                                   "(I/O hiba)",
                                  full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha shutil hibát kapunk, szintén nem sikerült
                    except shutil.Error:
                        message = ["Az alábbi fájl létrehozása "
                                   "sikertelen:", full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha OSError-t kapunk, valószínűleg létezik a fájl
                    except OSError:
                        message = ["Az alábbi fájl létrehozása "
                                   "sikertelen:", full_path,
                            "Lehetséges, hogy a megadott fájl már "
                            "létezik."]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # különben
                    else:
                        # tömbbe tessszük a kiírni valót (szöveg és fájlnév)
                        message = ["Az alábbi fájl sikeresen létrejött:",
                                  full_path]
                        # kiírjuk, hová másoltuk a fájlt
                        cf.print_list(item, message)


        elif x == ord('4'):  # ha a felhasználós a 4-es almenüt választja
            # a menüpont neve
            item = "UbiSysPy – Rendszeradatok menü – Alkalmazások" \
                      " visszaállítása listából."
            # tájákoztató sztring a print_list()-be
            items = ["Azt apt program nem minden verziója képes szimulálni a "
                     "műveletet,",
                     "ezért ezt a folytatás előtt ellenőrizni szükséges."]
            # záró üzenet
            finish = "Nyomj entert az apt ellenőrzéséhez"
            curses.endwin()  # befejezzük a cursest

            # a tájékoztató kiírása menüvel, záró üzenettel
            cf.print_list(item, items, finish=finish)

            # az installed_packages.py check_apt() meghívása és kiírása
            # ha az apt nem felel meg
            if not gip.check_apt():
                message = "Azt apt verzió nem megfelelő a művelet" \
                          " szimulálásához."
                # print_list(items[0], message, red=True)
                cf.print_string(item, message, red=True)

            # ha az apt megfelel
            else:
                # felhasználó és sudo jog ellenőrzése
                # ha 0-val tér vissza, a user root
                if sudo.check_root() == 0:
                    # kiírjuk az üzenetet
                    message = "Azt apt verzió megfelelő a művelet " \
                              "szimulálásához."
                    finish = "Nyomj entert a folytatáshoz"
                    curses.endwin()  # befejezzük a cursest
                    cf.print_string(item, message, finish=finish)

                    finish = "Nyomj entert a fájl helyének megadásához"
                    items = ["Szükség van egy, a dpkg --get-selections parancs "
                             "által előállított",
                             "vagy az előző menüpont segítségével generált "
                             "csomaglistára."]
                    # tájékoztató szöveg a csomaglistához
                    cf.print_list(item, items, finish=finish)

                    # a menüpont elemei
                    items_path = ["UbiSysPy – Rendszeradatok menü – Alkalmazások "
                                  "visszaállítása listából",
                                  "Add meg, hol van a telepített alkalmazások "
                                  "listája.", "Például: "
                                  "/home/felhasználó/könyvtár/csomagok.txt"]

                    # bekérjük a mentési útvonalat a felhasználótól
                    file_path = cf.get_param(items_path)

                    # ha a sztring üres (=False), nincs elérési út
                    if not file_path:
                        message = "Hiba: Nem adtál meg elérési utat."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a megadott úton nincs fájl
                    elif not os.path.isfile(file_path):
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési úton nincs fájl:",
                                   file_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    # különben
                    else:
                        curses.endwin()  # befejezzük a cursest
                        # üzenet
                        message = [
                            "Az elvégzendő műveletek szimulálása részletek "
                            "kiírásával.",
                            "Az áttekintés után Esc billentyűvel tudsz kilépni "
                            "a nézetből."]
                        # záró üzenet
                        finish = "Nyomj entert a szimuláláshoz"
                        # üzenet kiírása
                        cf.print_list(item, message, finish=finish)

                        # fájl helyének lekérése és szimulálás a
                        # gip.dselect()-tel
                        path = gip.dselect(file_path, True, True)
                        # Unicode string kell a curses scroll miatt
                        uitem = u"UbiSysPy – Rendszeradatok menü – " \
                                u"Alkalmazások visszaállítása listából"
                        # meghívjuk a görgető curses függvényt a fájl
                        # kiíráásához
                        cs.curses_scroll(uitem, path)

                        # kiírnadó elemek
                        items = ["UbiSysPy – Rendszeradatok menü – Alkalmazások "
                                 "visszaállítása listából",
                                 "Szeretnéd végrehajtani a műveletet? "
                                 "(igen/nem)",
                                 "A művelet végrehajtása percekig is "
                                 "eltarthat."]

                        answer = cf.get_param(items)  # válasz bekérése

                        # ha a felhasználó nem adott meg választ
                        if not answer:
                            message = "Hiba: Nem adtál meg választ."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha a válasz igen
                        elif answer == "igen" or answer == "Igen":
                            finish = "Nyomj entert a befejezéshez"
                            # meghívjuk a gip.dselect()-t a végrehajtáshoz
                            # ha igazzal tér vissza, sikeres volt
                            if gip.dselect(file_path, True, False):
                                message = "A művelet sikeres volt."
                                cf.print_string(item, message, finish=finish)

                            # ha hamissal tér vissza:
                            else:
                                message = "A művelet sikertelen volt."
                                cf.print_string(item, message, red=True,
                                    finish=finish)

                        # ha a válasz nem
                        elif answer == "nem" or answer == "Nem":
                            message = "Nem folytatjuk a műveletet."
                            # kiírjuk az üzenetet
                            cf.print_string(item, message)

                        # ha a válasz ismeretlen
                        else:
                            message = "Hiba: Nem adtál meg felismerhető " \
                                      "választ."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                if sudo.check_root() == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                if sudo.check_root() == 1:
                    # kiírjuk az üzenetet
                    message = "Azt apt verzió megfelelő a művelet " \
                              "szimulálásához."
                    finish = "Nyomj entert a folytatáshoz"
                    curses.endwin()  # befejezzük a cursest
                    cf.print_string(item, message, finish=finish)

                    finish = "Nyomj entert a fájl helyének megadásához"
                    items = ["Szükség van egy, a dpkg --get-selections parancs "
                             "által előállított",
                             "vagy az előző menüpont segítségével generált "
                             "csomaglistára."]
                    # tájékoztató szöveg a csomaglistához
                    cf.print_list(item, items, finish=finish)

                    # a menüpont elemei
                    items_path = ["UbiSysPy – Rendszeradatok menü – Alkalmazások "
                                  "visszaállítása listából",
                                  "Add meg, hol van a telepített alkalmazások "
                                  "listája.", "Például: "
                                  "/home/felhasználó/könyvtár/csomagok.txt"]

                    # bekérjük a mentési útvonalat a felhasználótól
                    file_path = cf.get_param(items_path)

                    # ha a sztring üres (=False), nincs elérési út
                    if not file_path:
                        message = "Hiba: Nem adtál meg elérési utat."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a megadott úton nincs fájl
                    elif not os.path.isfile(file_path):
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési úton nincs fájl:",
                                   file_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    # különben
                    else:
                        curses.endwin()  # befejezzük a cursest

                        # bekérő szöveg
                        prompt_str = "Add meg a jelszavad:"
                        # leírás a felhasználónak
                        descr = "A következő művelethez root jogra lesz " \
                                "szükséged."
                        # jelszó bekérése
                        passwd = cf.get_pass(item, descr, prompt_str)

                        # ha a felhasználó nem adott meg jelszót
                        if not passwd:
                            message = "Hiba: Nem adtál meg jelszót."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha adott meg jelszót
                        else:
                            # sudo jelszó ellenőrzés
                            # ha hamissal tér vissza, auth. hiba
                            if not sudo.sudo_test(passwd):
                                message = []
                                message1 = "Hitelesítési hiba"
                                message2 = "Elgépelted a jelszavadat."
                                message.append(message1)
                                message.append(message2)
                                cf.print_list(item, message, red=True)
                                break

                            # ha jó a jelszó
                            else:
                                # Unicode string kell a curses scroll miatt
                                uitem = u"UbiSysPy – Rendszeradatok menü – " \
                                       u"Alkalmazások visszaállítása listából"

                                # üzenet
                                message = ["Az elvégzendő műveletek "
                                           "szimulálása részletek "
                                           "kiírásával.",
                                    "Az áttekintés után az Esc billentyűvel "
                                    "tudsz kilépni a nézetből."]
                                # záró üzenet
                                finish = "Nyomj entert a szimuláláshoz"
                                # kiírás
                                cf.print_list(item, message, finish=finish)

                                # fájl helyének lekérése és szimulálás a
                                # gip.dselect()-tel
                                path = gip.dselect(file_path, passwd, True)
                                # meghívjuk a görgető curses függvényt a fájl
                                # kiíráásához
                                cs.curses_scroll(uitem, path)

                                # kiírnadó elemek
                                items = ["UbiSysPy – Rendszeradatok menü – "
                                         "Alkalmazások visszaállítása listából",
                                         "Szeretnéd végrehajtani a műveletet? "
                                         "(igen/nem)",
                                         "A művelet végrehajtása percekig is "
                                         "eltarthat."]

                                answer = cf.get_param(items)  # válasz bekérése

                                # ha a felhasználó nem adott meg választ
                                if not answer:
                                    message = "Hiba: Nem adtál meg választ."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha a válasz igen
                                elif answer == "igen" or answer == "Igen":
                                    finish = "Nyomj entert a befejezéshez"
                                    # meghívjuk a gip.dselect()-t a
                                    # végrehajtáshoz
                                    # ha igazzal tér vissza, sikeres volt
                                    if gip.dselect(file_path, passwd, False):
                                        message = "A művelet sikeres volt."
                                        cf.print_string(item, message,
                                            finish=finish)

                                    # ha hamissal tér vissza:
                                    else:
                                        message = "A művelet sikertelen volt."
                                        cf.print_string(item, message, red=True,
                                            finish=finish)

                                # ha a válasz nem
                                elif answer == "nem" or answer == "Nem":
                                    message = "Nem folytatjuk a műveletet."
                                    # kiírjuk az üzenetet
                                    cf.print_string(item, message)

                                # ha a válasz ismeretlen
                                else:
                                    message = "Hiba: Nem adtál meg " \
                                              "felismerhető választ."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                        # felülírjuk a felhasználó jelszavát
                        passwd = os.urandom(len(passwd))


        elif x == ord('5'):  # ha a felhasználó az 5-ös almenüt választja
            # a menüpont neve
            item = "UbiSysPy – Rendszeradatok menü – Adathordozók adatainak " \
                   "lekérdezése"
            curses.endwin()  # befejezzük a cursest

            # felhasználó és sudo jog ellenőrzése
            # ha 0-val tér vissza, a user root
            if sudo.check_root() == 0:
                # sudo nélkül hívjuk meg a parancsokat
                # tájákoztató sztring a print_list()-be
                items = ["Az eszközök SMART adatainak lekérdezéséhez az "
                         "alábbi,",
                         "smartmontools nevű csomagra van szükség."]
                # záró üzenet
                finish = "Nyomj entert a smartmontools ellenőrzéséhez"

                # a tájékoztató kiírása menüvel, záró üzenettel
                cf.print_list(item, items, finish=finish)

                # ha van telepített függőség
                if d.dep_check("smartmontools"):
                    # tömb a smart.get_devices()-nak
                    devices = smart.get_devices()
                    # az eszközök száma a tömb első eleme, amit
                    # kiveszünk a tömbből
                    dev_num = devices.pop(0)

                    # bejárjuk a már csak az eszközöket tartalmazó
                    # tömböt
                    for num, device in enumerate(devices):
                        # hozzáadunk egyet, mert nullától kezdődik a
                        # számolás
                        num += 1

                        # meghívjuk a smart.get_smart()-ot,
                        # hogy felmérjük a SMART-ot
                        get_smart = smart.get_smart(device, True, num,
                            dev_num)

                        # ha False-szal tér vissza, kiírjuk, hogy a
                        # SMART nem támogatott
                        if not get_smart:
                            finish = "Nyomj entert a folytatáshoz"
                            message = []
                            message1 = "SMART lekérdezési hiba"
                            empty = ""
                            message2 = "{dn}/{n}. észlelt " \
                                       "eszköz".format(dn=dev_num, n=num)
                            message3 = "A {d} eszköz nem támogatja a SMART " \
                                       "lekérdezést ".format(d=device)
                            message4 = "vagy az eszközön nincs " \
                                       "engedélyezve a SMART."
                            message.append(message1)
                            message.append(empty)
                            message.append(message2)
                            message.append(empty)
                            message.append(message3)
                            message.append(message4)
                            cf.print_list(item, message, red=True,
                                finish=finish)

                        # különben az eszköz támogatja a SMART-ot
                        else:
                            finish = "Nyomj entert a részletekért"
                            # meghívjuk a smart.get_smart()-ot
                            cf.print_function_with_args(item,
                                smart.get_smart(device, True, num, dev_num),
                                finish=finish)

                            # SSD/HDD-t a get_smart 4. eleme tartalmazza
                            rotation = smart.get_smart(device, True, num,
                                dev_num)[3]

                            # smart.smart_details() meghívása
                            finish = "Nyomj entert a folytatáshoz"
                            cf.print_function_with_args(item,
                                smart.smart_details(device, True, rotation,
                                    num, dev_num), finish=finish)

                # ha nincs telepített függőség
                else:
                    # kiírnadó elemek
                    item = "UbiSysPy – Rendszeradatok menü – Adathordozók  " \
                           "adatainak lekérdezése"
                    items = ["A folytatáshoz szükség van a smartmontools "
                             "nevű csomagra.",
                        "A telepítés eltarthat egy ideig.",
                        "Szeretnéd telepíteni a csomagot? (igen/nem)"]

                    # válasz bekérése
                    answer = cf.get_param_with_list(item, items)

                    # ha a felhasználó nem adott meg választ
                    if not answer:
                        message = "Hiba: Nem adtál meg választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a válasz igen
                    elif answer == "igen" or answer == "Igen":
                        finish = "Nyomj entert a befejezéshez"

                        package = "smartmontools"

                        return_value = ip.install_package(package,
                                    True)

                        # ha 2-vel tér vissza, nem sikerült a csomaglista
                        # frissítése
                        if return_value == 2:
                            message = "A csomaglista frissítése sikertelen."
                            cf.print_string(item, message, red=True,
                                            finish=finish)

                        # ha 0-val tér vissza, sikeres volt a
                        # programtelepítés
                        elif return_value == 0:
                            message = "A(z) {p} program sikeresen " \
                                      "telepítésre került.".format(p=package)
                            cf.print_string(item, message, finish=finish)

                        # ha 1-gyel tér vissza, sikertelen volt a telepítés
                        else:
                            message = "A(z) {p} program telepítése " \
                                      "sikertelen volt.".format(p=package)
                            cf.print_string(item, message, red=True,
                                finish=finish)

                    # ha a válasz nem
                    elif answer == "nem" or answer == "Nem":
                        message = "Nem telepítjük a csomagot."
                        cf.print_string(item, message)  # kiírjuk az üzenetet

                    # ha a válasz ismeretlen
                    else:
                        message = "Hiba: Nem adtál meg felismerhető választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

            # ha 2-vel tér vissza, nincs sudo joga
            if sudo.check_root() == 2:
                message = "Nincs sudo jogod, így a művelet nem folytatható."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha 1-gyel tér vissza, van sudo joga
            if sudo.check_root() == 1:
                # tájákoztató sztring a print_list()-be
                items = ["Az eszközök SMART adatainak lekérdezéséhez az "
                         "alábbi,",
                         "smartmontools nevű csomagra van szükség."]
                # záró üzenet
                finish = "Nyomj entert a smartmontools ellenőrzéséhez"

                # a tájékoztató kiírása menüvel, záró üzenettel
                cf.print_list(item, items, finish=finish)

                # ha van telepített függőség
                if d.dep_check("smartmontools"):
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "Az eszköz adatok lekérdezéséhez root jogra lesz " \
                            "szükséged."
                    # jelszó bekérése
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha adott meg jelszót
                    else:
                        # sudo jelszó ellenőrzés
                        # ha hamissal tér vissza, auth. hiba
                        if not sudo.sudo_test(passwd):
                            message = []
                            message1 = "Hitelesítési hiba"
                            message2 = "Elgépelted a jelszavadat."
                            message.append(message1)
                            message.append(message2)
                            cf.print_list(item, message, red=True)
                            break

                        # ha igazzal tér vissza, jó a jelszó
                        else:
                            # tömb a smart.get_devices()-nak
                            devices = smart.get_devices()
                            # az eszközök száma a tömb első eleme, amit
                            # kiveszünk a tömbből
                            dev_num = devices.pop(0)

                            # bejárjuk a már csak az eszközöket tartalmazó
                            # tömböt
                            for num, device in enumerate(devices):

                                # hozzáadunk egyet, mert nullától kezdődik a
                                # számolás
                                num += 1

                                # meghívjuk a smart.get_smart()-ot,
                                # hogy felmérjük a SMART-ot
                                get_smart = smart.get_smart(device, passwd, num,
                                    dev_num)

                                # ha False-szal tér vissza, kiírjuk, hogy a
                                # SMART nem támogatott
                                if not get_smart:
                                    finish = "Nyomj entert a folytatáshoz"
                                    message = []
                                    message1 = "SMART lekérdezési hiba"
                                    empty = ""
                                    message2 = "{dn}/{n}. észlelt " \
                                               "eszköz".format(dn=dev_num,
                                        n=num)
                                    message3 = "A {d} eszköz nem támogatja a " \
                                               "SMART " \
                                               "lekérdezést ".format(d=device)
                                    message4 = "vagy az eszközön nincs " \
                                               "engedélyezve a SMART."
                                    message.append(message1)
                                    message.append(empty)
                                    message.append(message2)
                                    message.append(empty)
                                    message.append(message3)
                                    message.append(message4)
                                    cf.print_list(item, message, red=True,
                                        finish=finish)

                                # különben az eszköz támogatja a SMART-ot
                                else:
                                    finish = "Nyomj entert a részletekért"
                                    # meghívjuk a smart.get_smart()-ot
                                    cf.print_function_with_args(item,
                                        smart.get_smart(device, passwd, num,
                                            dev_num), finish=finish)

                                    # SSD/HDD-t a get_smart 4. eleme tartalmazza
                                    rotation = smart.get_smart(device, passwd,
                                        num, dev_num)[3]

                                    # smart.smart_details() meghívása
                                    finish = "Nyomj entert a folytatáshoz"
                                    cf.print_function_with_args(item,
                                        smart.smart_details(device, passwd,
                                            rotation, num, dev_num),
                                        finish=finish)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))

                # ha nincs telepített függőség
                else:
                    # kiírnadó elemek
                    items = ["UbiSysPy – Rendszeradatok menü – Adathordozók "
                             "adatainak lekérdezése",
                    "A folytatáshoz szükség van a smartmontools nevű csomagra.",
                    "Szeretnéd telepíteni a csomagot? (igen/nem)"]

                    answer = cf.get_param(items)  # válasz bekérése

                    # ha a felhasználó nem adott meg választ
                    if not answer:
                        message = "Hiba: Nem adtál meg választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a válasz igen
                    elif answer == "igen" or answer == "Igen":
                        finish = "Nyomj entert a befejezéshez"

                        package = "smartmontools"

                        # bekérő szöveg
                        prompt_str = "Add meg a jelszavad:"
                        # leírás a felhasználónak
                        descr = "A telepítés eltarthat néhány percig."
                        # jelszó bekérése
                        passwd = cf.get_pass(item, descr, prompt_str)

                        # ha a felhasználó nem adott meg jelszót
                        if not passwd:
                            message = "Hiba: Nem adtál meg jelszót."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha adott meg jelszót
                        else:
                            # meghívjuk a csomagtelepítőt
                            return_value = ip.install_package(package, passwd)

                            # ha 3-mal tér vissza, autentikációs hiba (sudo)
                            if return_value == 3:
                                message = "Hitekesítési hiba. Elírhattad a " \
                                          "jelszavadat."
                                cf.print_string(item, message, red=True,
                                    finish=finish)

                            # ha 2-vel tér vissza, nem sikerült a csomaglista
                            #  frissítése
                            elif return_value == 2:
                                message = "A csomaglista frissítése sikertelen."
                                cf.print_string(item, message, red=True,
                                    finish=finish)

                            # ha 0-val tér vissza, sikeres volt a
                            # programtelepítés
                            elif return_value == 0:
                                message = "A(z) {p} program sikeresen " \
                                          "telepítve.".format(p=package)
                                cf.print_string(item, message, finish=finish)

                            # ha 1-gyel tér vissza, sikertelen volt a telepítés
                            else:
                                message = "A(z) {p} program telepítése " \
                                          "sikertelen volt.".format(p=package)
                                cf.print_string(item, message, red=True,
                                    finish=finish)

                        # felülírjuk a felhasználó jelszavát
                        passwd = os.urandom(len(passwd))

                    # ha a válasz nem
                    elif answer == "nem" or answer == "Nem":
                        message = "Nem telepítjük a csomagot."
                        cf.print_string(item, message)  # kiírjuk az üzenetet

                    # ha a válasz ismeretlen
                    else:
                        message = "Hiba: Nem adtál meg felismerhető választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)


        elif x == ord('6'):  # ha a felhasználó a 6-os almenüt választja
            # a menüpont neve a print_function_without_args()-be
            item = "UbiSysPy – Rendszeradatok menü – Memóriaadatok lekérdezése"
            curses.endwin()  # befejezzük a cursest
            # a memoria.py get_mem_info() meghívása és kiírása
            cf.print_function_without_args(item, memoria.get_mem_info)


    curses.endwin()  # curses vége
