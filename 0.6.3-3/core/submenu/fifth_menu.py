#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület ötödik menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
import curses

from curses_helpers import curses_scroll as cs
from curses_helpers import curses_functions as cf  # curses függvények
from fifth_menu_submenus import first_submenu as fs
from fifth_menu_submenus import second_submenu as ss
from system import dmesg as d

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def fifth_menu():
    """
    Az ötödik menü almenüjét és azok menüpontjait valósítja meg.
    """

    x = 0
    # amíg a felhasználó által megadott szám nem 6, ami az almenüpontok száma
    while x != ord('6'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # színpár def.
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # színpár def.
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # az almenü elemei
        items = [u"UbiSysPy – Naplózás menü",
        u"Szám billentyű megnyomásával válassz az alábbi menüpontok közül:",
        u"1) Naplózás beállítása",
        u"2) Naplók megtekintése",
        u"3) A dmesg lekérdezése",
        u"4) A syslog lekérdezés",
        u"5) A kern.log lekérdezése",
        u"6) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, items[0].encode("utf-8"),
            curses.A_BOLD | curses.color_pair(2))
        screen.addstr(5, 2, items[1].encode("utf-8"),
            curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))
        screen.addstr(11, 4, items[6].encode("utf-8"))
        screen.addstr(12, 4, items[7].encode("utf-8"))

        screen.refresh()  # képernyő frissítése

        x = screen.getch()  # várjuk a felhasználó bemenetét

        if x == ord('1'):   # ha 1, végrehajtjuk a parancsot
            curses.endwin()  # befejezzük a cursest
            # meghívjuk a first nevű függvényt az almenükkel
            cf.call_function(fs.first_submenu)

        elif x == ord('2'):
            curses.endwin()
            # meghívjuk a second nevű függvényt az almenükkel
            cf.call_function(ss.second_submenu)

        elif x == ord('3'):
            curses.endwin()
            menu_str = u"UbiSysPy – Naplózás menü – A dmesg lekérdezése"
            # elmentjük a fájl elérési útját, amibe a dmesget mentettük
            dmesg_file = d.dmesg()
            # meghívjuk a görgető curses függvényt a napló kiíráásához
            cs.curses_scroll(menu_str, dmesg_file)

        elif x == ord('4'):   # ha 4, végrehajtjuk a parancsot
            curses.endwin()
            menu_str = u"UbiSysPy – Naplózás menü – A syslog lekérdezése"
            path = "/var/log/syslog"
            # meghívjuk a görgető curses függvényt a napló kiíráásához
            cs.curses_scroll(menu_str, path)

        elif x == ord('5'):   # ha 5, végrehajtjuk a parancsot
            curses.endwin()
            menu_str = u"UbiSysPy – Naplózás menü – A kern.log lekérdezése"
            path = "/var/log/kern.log"
            # meghívjuk a görgető curses függvényt a napló kiíráásához
            cs.curses_scroll(menu_str, path)
