#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület negyedik menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import glob
import locale
import os
import shutil
import subprocess
import sys
import time
import curses

from curses_helpers import curses_functions as cf  # curses függvények
from package_handling import mirrors_ppas as mp
from system import settings as s  # könyvtárméret összehasonlításhoz
from package_handling import get_installed_packages as gip

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def fourth_menu():
    """
    A negyedik menü almenüjét és azok menüpontjait valósítja meg.
    """

    x = 0
    # amíg a felhasználó által megadott szám nem 9, ami az almenüpontok száma
    while x != ord('8'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # színpár def.
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # színpár def.
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # az almenü elemei
        items = [u"UbiSysPy – Biztonsági mentés menü",
            u"Szám billentyű megnyomásával válassz az alábbi menüpontok közül:",
            u"1) Megadott könyvtár mentése",
            u"2) Firefox profilkönyvtár mentése",
            u"3) Thunderbird könyvtár mentése",
            u"4) Telepített alkalmazások listájának mentése",
            u"5) Használt PPA-k listájának mentése",
            u"6) SSH könyvtár mentése",
            u"7) GnuPG könyvtár mentése",
            u"8) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, items[0].encode("utf-8"),
            curses.A_BOLD | curses.color_pair(2))
        screen.addstr(5, 2, items[1].encode("utf-8"),
            curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))
        screen.addstr(11, 4, items[6].encode("utf-8"))
        screen.addstr(12, 4, items[7].encode("utf-8"))
        screen.addstr(13, 4, items[8].encode("utf-8"))
        screen.addstr(14, 4, items[9].encode("utf-8"))

        screen.refresh()  # képernyő frissítése

        x = screen.getch()  # várjuk a felhasználó bemenetét


        if x == ord('1'):
            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – Megadott könyvtár mentése"

            curses.endwin()  # befejezzük a cursest

            # a menüpont elemei
            items_path = [item]
            items_path.append("Add meg, melyik könyvtárt szeretnéd menteni.")
            items_path.append("Például: /home/valaki/könyvtáram/")

            # bekérjük a mentendő könyvtárat a felhasználótól
            source_path = cf.get_param(items_path)

            if not source_path:  # ha az útvonal nem került megadásra
                message = "Hiba: Nem adtál meg elérési utat."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            elif not os.path.isdir(source_path):  # ha a könyvtár nem létezik
                curses.endwin()  # befejezzük a cursest
                # hibaüzenet összeillesztése
                message = ["Hiba: A megadott elérési út NEM létezik:",
                    source_path]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            # ha adott meg könyvtárat
            else:
                # a menüpont elemei
                items_path = []
                items_path.append("Add meg, hová szeretnéd menteni a")
                items_path.append(source_path)
                items_path.append("könyvtárat.")
                items_path.append("Például: /media/adatok/könyvtár/")

                # bekérjük a mentési útvonalat a felhasználótól
                dest_path = cf.get_param_with_list(item, items_path)

                if not dest_path:  # ha az útvonal nem került megadásra
                    message = "Hiba: Nem adtál meg elérési utat."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM létezik:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha létezik a megadott könyvtár
                    # ha a felhasználónak nincs írási joga a megadott útvonalra
                    if not os.access(dest_path, os.W_OK):
                        curses.endwin()  # befejezzük a cursest
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési út NEM írható:",
                            dest_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    else:  # ha a felhasználónak van írási joga
                        # a menüpont elemei
                        items_name = ["Add meg, milyen néven szeretnéd "
                                      "menteni a könyvtárat.",
                            "Alapértelmezetten: eredetinév_időbélyeg",
                            "A mentés eltarthat egy ideig a könyvtár "
                            "méretétől és az eszköztől függően."]

                        # lekérdezzük, hogy a célmappa elég nagy-e a mentendő
                        # könyvtárhoz
                        # ha a célkönyvtár nagyobb, kiírjuk, hogy a mentés
                        # eltarthat egy ideig
                        if s.compare_dirs(source_path, dest_path):
                            # bekérjük a mentési nevet a felhasználótól
                            name = cf.get_param_with_list(item, items_name)

                            # ha a felhasználó nem adott meg fájlnevet
                            if name == "":
                                # az időbélyeg
                                time_stamp = time.strftime('%Y-%m-%d_%T')

                                # könyvtárnév lekérdezése
                                base_name = os.path.basename(source_path)

                                # az alapértelmezett név
                                default_name = base_name + "_" + time_stamp

                                # az útvonal a megadott útvonal és az
                                # alapértelmezett név
                                full_path = os.path.join(dest_path,
                                    default_name)

                            # különben
                            else:
                                # ha adott meg nevet, akkor összefűzzük a
                                # megadott útvonallal
                                full_path = os.path.join(dest_path, name)

                            # megpróbáljuk elvégezni a másolást
                            try:
                                shutil.copytree(source_path, full_path)

                            # ha IO hibát kapunk, nem sikerült
                            except IOError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha shutil hibát kapunk, szintén nem sikerült
                            except shutil.Error:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha OSError-t kapunk, valószínűleg létezik a fájl
                            except OSError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Lehetséges, hogy a megadott könyvtár már "
                                    "létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # különben
                            else:
                                # tömbbe tessszük a kiírni valót (szöveg és
                                # fájlnév)
                                message = ["Az alábbi könyvtár sikeresen "
                                          "létrejött:", full_path]
                                # kiírjuk, hová másoltuk a fájlt
                                cf.print_list(item, message)

                        # ha a célkönyvtár kisebb, hibát írunk ki
                        else:
                            message = "A megadott célkönyvtárban nincs elég " \
                                      "szabad hely!"
                            cf.print_string(item, message, red=True)


        elif x == ord('2'):
            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – Firefox profilkönyvtár  " \
                   "mentése"

            curses.endwin()  # befejezzük a cursest

            # lekérdezzük a user Home könyvtárát
            user_dir = os.environ["HOME"]

            # a Firefox profilkönyvtár könyvtár elérési útja
            full_source = os.path.join(user_dir, ".mozilla")

            # megnézzük, hogy létezik-e a könyvtár
            # ha nem
            if not os.path.isdir(full_source):
                message = ["Hiba: Az alábbi forráskönyvtár nem létezik:",
                    full_source,
                "Győzödj meg róla, hogy a Firefox program telepítve van."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            # ha igen, megyünk tovább
            else:
                # a menüpont elemei
                items_path = [item]
                items_path.append("Add meg, hová szeretnéd menteni a Firefox "
                                  "profilkönyvtárat")
                items_path.append("Például: /media/adatok/könyvtár/")

                # bekérjük a mentési útvonalat a felhasználótól
                dest_path = cf.get_param(items_path)

                if not dest_path:  # ha az útvonal nem került megadásra
                    message = "Hiba: Nem adtál meg elérési utat."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM létezik:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha létezik a megadott könyvtár
                    # ha a felhasználónak nincs írási joga a megadott útvonalra
                    if not os.access(dest_path, os.W_OK):
                        curses.endwin()  # befejezzük a cursest
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési út NEM írható:",
                            dest_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    else:  # ha a felhasználónak van írási joga
                        # a menüpont elemei
                        items_name = ["Add meg, milyen néven szeretnéd "
                                      "menteni a Firefox profilkönyvtárat.",
                            "Alapértelmezetten: _.mozilla_időbélyeg",
                            "Mentés előtt zárd be a programot.",
                            "A mentés eltarthat egy ideig a könyvtár "
                            "méretétől és az eszköztől függően."]

                        # lekérdezzük, hogy a célmappa elég nagy-e a mentendő
                        # könyvtárhoz
                        # ha a célkönyvtár nagyobb, kiírjuk, hogy zárja be a
                        # programot, és hogy a mentés eltarthat egy ideig
                        if s.compare_dirs(full_source, dest_path):
                            # bekérjük a mentési nevet a felhasználótól
                            name = cf.get_param_with_list(item, items_name)

                            # ha a felhasználó nem adott meg fájlnevet
                            if name == "":
                                # az időbélyeg
                                time_stamp = time.strftime('%Y-%m-%d_%T')

                                # az alapértelmezett név
                                default_name = "_.mozilla_" + time_stamp

                                # az útvonal a megadott útvonal és az
                                # alapértelmezett név
                                full_path = os.path.join(dest_path,
                                    default_name)

                            # különben
                            else:
                                # ha adott meg nevet, akkor összefűzzük a
                                # megadott útvonallal
                                full_path = os.path.join(dest_path, name)

                            # megpróbáljuk elvégezni a másolást
                            try:
                                shutil.copytree(full_source, full_path)

                            # ha IO hibát kapunk, nem sikerült
                            except IOError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen: (I/O hiba)", full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha shutil hibát kapunk, szintén nem sikerült
                            except shutil.Error:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen: (másolási hiba)",
                                    full_path,
                                    "Győzödj meg róla, hogy mentés előtt "
                                    "bezártad a programot."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha OSError-t kapunk, valószínűleg létezik a fájl
                            except OSError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Lehetséges, hogy a megadott könyvtár már "
                                    "létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha nincs .firefox mappa
                            except subprocess.CalledProcessError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Győzödj meg róla, hogy a ~/.firefox "
                                    "könyvtár létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # különben
                            else:
                                # tömbbe tessszük a kiírni valót (szöveg és
                                # fájlnév)
                                message = ["Az alábbi könyvtár sikeresen "
                                          "létrejött:",
                                    full_path]
                                # kiírjuk, hová másoltuk a fájlt
                                cf.print_list(item, message)

                        # ha a célkönyvtár kisebb, hibát írunk ki
                        else:
                            message = "A megadott célkönyvtárban nincs elég " \
                                      "szabad hely!"
                            cf.print_string(item, message, red=True)


        elif x == ord('3'):
            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – Thunderbird könyvtár mentése"

            curses.endwin()  # befejezzük a cursest

            # lekérdezzük a user Home könyvtárát
            user_dir = os.environ["HOME"]

            # a Thunderbird könyvtár elérési útja
            full_source = os.path.join(user_dir, ".thunderbird")

            # megnézzük, hogy létezik-e a könyvtár
            # ha nem
            if not os.path.isdir(full_source):
                message = ["Hiba: Az alábbi forráskönyvtár nem létezik:",
                    full_source,
                "Győzödj meg róla, hogy a Thunderbird program telepítve van."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            # ha igen, megyünk tovább
            else:
                # a menüpont elemei
                items_path = [item]
                items_path.append("Add meg, hová szeretnéd menteni a "
                                  "Thunderbird könyvtárat")
                items_path.append("Például: /media/adatok/könyvtár/")

                # bekérjük a mentési útvonalat a felhasználótól
                dest_path = cf.get_param(items_path)

                if not dest_path:  # ha az útvonal nem került megadásra
                    message = "Hiba: Nem adtál meg elérési utat."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM létezik:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha létezik a megadott könyvtár
                    # ha a felhasználónak nincs írási joga a megadott útvonalra
                    if not os.access(dest_path, os.W_OK):
                        curses.endwin()  # befejezzük a cursest
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési út NEM írható:",
                            dest_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    else:  # ha a felhasználónak van írási joga
                        # a menüpont elemei
                        items_name = ["Add meg, milyen néven szeretnéd "
                                      "menteni a Thunderbird könyvtárat.",
                            "Alapértelmezetten: _.thunderbird_időbélyeg",
                            "Mentés előtt zárd be a programot.",
                            "A mentés eltarthat egy ideig a könyvtár "
                            "méretétől és az eszköztől függően."]

                        # lekérdezzük, hogy a célmappa elég nagy-e a mentendő
                        # könyvtárhoz
                        # ha a célkönyvtár nagyobb, kiírjuk, hogy zárja be a
                        # programot, és hogy a mentés eltarthat egy ideig
                        if s.compare_dirs(full_source, dest_path):
                            # bekérjük a mentési nevet a felhasználótól
                            name = cf.get_param_with_list(item, items_name)

                            # ha a felhasználó nem adott meg fájlnevet
                            if name == "":
                                # az időbélyeg
                                time_stamp = time.strftime('%Y-%m-%d_%T')

                                # az alapértelmezett név
                                default_name = "_.thunderbird_" + time_stamp

                                # az útvonal a megadott útvonal és az
                                # alapértelmezett név
                                full_path = os.path.join(dest_path,
                                    default_name)

                            # különben
                            else:
                                # ha adott meg nevet, akkor összefűzzük a
                                # megadott útvonallal
                                full_path = os.path.join(dest_path, name)

                            # megpróbáljuk elvégezni a másolást
                            try:
                                shutil.copytree(full_source, full_path)

                            # ha IO hibát kapunk, nem sikerült
                            except IOError:
                                message = [
                                    "Az alábbi könyvtár létrehozása "
                                    "sikertelen (I/O hiba):",
                                    full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha shutil hibát kapunk, szintén nem sikerült
                            except shutil.Error:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen (másolási hiba):",
                                    full_path,
                                    "Győzödj meg róla, hogy mentés előtt "
                                    "bezártad a programot."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha OSError-t kapunk, valószínűleg létezik a fájl
                            except OSError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Lehetséges, hogy a megadott könyvtár már "
                                    "létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha nincs .thunderbird mappa
                            except subprocess.CalledProcessError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Győzödj meg róla, hogy a ~/.thunderbird "
                                    "könyvtár létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # különben
                            else:
                                # tömbbe tessszük a kiírni valót (szöveg és
                                # fájlnév)
                                message = ["Az alábbi könyvtár sikeresen "
                                          "létrejött:",
                                    full_path]
                                # kiírjuk, hová másoltuk a fájlt
                                cf.print_list(item, message)

                        # ha a célkönyvtár kisebb, hibát írunk ki
                        else:
                            message = "A megadott célkönyvtárban nincs elég " \
                                      "szabad hely!"
                            cf.print_string(item, message, red=True)


        elif x == ord('4'):
            item = "UbiSysPy – Biztonsági mentés menü – Telepített alkalmazások " \
                   "listája"

            curses.endwin()  # befejezzük a cursest
            # telepített csomagok lekérdezése
            gip.installed_packages_list()
            # cf.print_function_without_args(item, gip.installed_packages_list)

            # a menüpont elemei
            items_path = [item]
            items_path.append("Add meg, hová szeretnéd menteni a telepített "
                              "alkalmazások listáját.")
            items_path.append("Például: /media/adatok/könyvtár/")

            # bekérjük a mentési útvonalat a felhasználótól
            dest_path = cf.get_param(items_path)

            if not dest_path:  # ha az útvonal nem került megadásra
                message = "Hiba: Nem adtál meg elérési utat."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                curses.endwin()  # befejezzük a cursest
                # hibaüzenet összeillesztése
                message = ["Hiba: A megadott elérési út NEM létezik:",
                    dest_path]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            else:  # ha létezik a megadott könyvtár
                # ha a felhasználónak nincs írási joga a megadott útvonalra
                if not os.access(dest_path, os.W_OK):
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM írható:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha a felhasználónak van írási joga
                    # a menüpont elemei
                    items_name = [item]
                    items_name.append("Add meg, milyen néven szeretnéd "
                                      "menteni a rendszeradatokat.")
                    items_name.append("Alapértelmezetten: "
                                      "telepitett-csomagok_időbélyeg.txt")

                    # lekérdezzük a user Home könyvtárát
                    user_dir = os.environ["HOME"]
                    # létrehozzuk az elérési út sztringjét, csillaggal a
                    # glob()-nak
                    source_dir = user_dir
                    source_dir += "/.ubisyspy/telepitett_csomagok/*"
                    # a másolandó fájl neve a könyvtár egyetlen fájlja
                    # a glob.glob() tömben adja vissza a könyvtárak elemeit
                    # elérési úttal
                    full_source = glob.glob(source_dir)[0]
                    file_name = os.path.basename(full_source)

                    # bekérjük a mentési nevet a felhasználótól
                    name = cf.get_param(items_name)

                    # ha a felhasználó nem adott meg fájlnevet
                    if name == "":
                        # akkor a megadott útvonal és az eredeti fájlnév
                        full_path = os.path.join(dest_path, file_name)
                    # különben
                    else:
                        # ha adott meg fájlnevet, akkor összefűzzük a
                        # megadott útvonallal
                        full_path = os.path.join(dest_path, name)

                    # megpróbáljuk elvégezni a másolást
                    try:
                        shutil.copy(full_source, full_path)

                    # ha IO hibát kapunk, nem sikerült
                    except IOError:
                        message = ["Az alábbi fájl létrehozása sikertelen: ("
                                   "I/O hiba)",
                            full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha shutil hibát kapunk, szintén nem sikerült
                    except shutil.Error:
                        message = ["Az alábbi fájl létrehozása sikertelen: ("
                                   "másolási hiba)",
                            full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha OSError-t kapunk, valószínűleg létezik a fájl
                    except OSError:
                        message = ["Az alábbi fájl létrehozása sikertelen:",
                            full_path,
                            "Lehetséges, hogy a megadott fájl már létezik."]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # különben
                    else:
                        # tömbbe tessszük a kiírni valót (szöveg és fájlnév)
                        message = ["Az alábbi fájl sikeresen létrejött:",
                            full_path]
                        # kiírjuk, hová másoltuk a fájlt
                        cf.print_list(item, message)


        elif x == ord('5'):
            # lekérdezzük és mentjük a PPA-k listáját
            mp.save_ppa_list()

            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – Használt PPA-k listájának  " \
                   "mentése"
            items_path = [item]
            items_path.append("Add meg, hová szeretnéd menteni a használt "
                              "PPA-k listáját.")
            items_path.append("Például: /media/adatok/könyvtár/")

            curses.endwin()  # befejezzük a cursest

            # bekérjük a mentési útvonalat a felhasználótól
            dest_path = cf.get_param(items_path)

            if not dest_path:  # ha az útvonal nem került megadásra
                message = "Hiba: Nem adtál meg elérési utat."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                curses.endwin()  # befejezzük a cursest
                # hibaüzenet összeillesztése
                message = ["Hiba: A megadott elérési út NEM létezik:",
                    dest_path]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            else:  # ha létezik a megadott könyvtár
                # ha a felhasználónak nincs írási joga a megadott útvonalra
                if not os.access(dest_path, os.W_OK):
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM írható:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha a felhasználónak van írási joga
                    # a menüpont elemei
                    items_name = [item]
                    items_name.append("Add meg, milyen néven szeretnéd "
                                      "menteni a a használt PPA-k listáját.")
                    items_name.append("Alapértelmezetten: "
                                      "PPA_list_időbélyeg.txt")

                    # bekérjük a mentési nevet a felhasználótól
                    name = cf.get_param(items_name)
                    curses.endwin()  # befejezzük a cursest

                    # lekérdezzük a user Home könyvtárát
                    user_dir = os.environ["HOME"]
                    # létrehozzuk az elérési út sztringjét csillaggal,
                    # a glob()-nak
                    source_dir = user_dir
                    source_dir += "/.ubisyspy/rendszeradatok/*"

                    # a másolandó fájl neve a könyvtár egyetlen fájlja
                    # a glob.glob() tömbben adja vissza a könyvtárak elemeit
                    # elérési úttal
                    full_source = glob.glob(source_dir)[0]
                    file_name = os.path.basename(full_source)

                    # ha a felhasználó nem adott meg fájlnevet
                    if name == "":
                        # akkor a megadott útvonal és az eredeti fájlnév
                        full_path = os.path.join(dest_path, file_name)
                    else:
                        # összeillesztjük a cél útvonalat és a fájlnevet
                        # ha adott meg fájlnevet, akkor összefűzzük
                        # a megadott útvonallal
                        full_path = os.path.join(dest_path, name)

                    # megpróbáljuk elvégezni a másolást
                    try:
                        shutil.copy(full_source, full_path)

                    # ha IO hibát kapunk, nem sikerült
                    except IOError:
                        message = ["Az alábbi fájl létrehozása sikertelen: ("
                                  "I/O hiba)",
                            full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha shutil hibát kapunk, szintén nem sikerült
                    except shutil.Error:
                        message = ["Az alábbi fájl létrehozása sikertelen: ("
                                   "másolási hiba)",
                            full_path]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # ha OSError-t kapunk, valószínűleg létezik a fájl
                    except OSError:
                        message = ["Az alábbi fájl létrehozása sikertelen:",
                            full_path,
                            "Lehetséges, hogy a megadott fájl már létezik."]
                        # tehát kiírjuk, hogy a művelet sikertelen volt
                        cf.print_list(item, message, red=True)

                    # különben
                    else:
                        # tömbbe tessszük a kiírni valót (szöveg és fájlnév)
                        message = ["Az alábbi fájl sikeresen létrejött:",
                            full_path]
                        # kiírjuk, hová másoltuk a fájlt
                        cf.print_list(item, message)


        elif x == ord('6'):
            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – SSH könyvtár mentése"

            curses.endwin()  # befejezzük a cursest

            # lekérdezzük a user Home könyvtárát
            user_dir = os.environ["HOME"]

            # az SSH könyvtár elérési útja
            full_source = os.path.join(user_dir, ".ssh")

            # megnézzük, hogy létezik-e a könyvtár
            # ha nem
            if not os.path.isdir(full_source):
                message = ["Hiba: Az alábbi forráskönyvtár nem létezik:",
                    full_source,
                "Győzödj meg róla, hogy a SSH program telepítve van."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            # ha igen, megyünk tovább
            else:
                # a menüpont elemei
                items_path = [item]
                items_path.append("Add meg, hová szeretnéd menteni az SSH "
                                  "könyvtárat.")
                items_path.append("Például: /media/adatok/könyvtár/")

                # bekérjük a mentési útvonalat a felhasználótól
                dest_path = cf.get_param(items_path)

                if not dest_path:  # ha az útvonal nem került megadásra
                    message = "Hiba: Nem adtál meg elérési utat."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM létezik:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha létezik a megadott könyvtár
                    # ha a felhasználónak nincs írási joga a megadott útvonalra
                    if not os.access(dest_path, os.W_OK):
                        curses.endwin()  # befejezzük a cursest
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési út NEM írható:",
                            dest_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    else:  # ha a felhasználónak van írási joga
                        # a menüpont elemei
                        items_name = [
                            "Add meg, milyen néven szeretnéd menteni az SSH "
                            "könyvtárat.", "Alapértelmezetten: _.ssh_időbélyeg",
                            "Mentés előtt zárd be a programot.",
                            "A mentés eltarthat egy ideig a könyvtár "
                            "méretétől és az eszköztől függően."]

                        # lekérdezzük, hogy a célmappa elég nagy-e a mentendő
                        # könyvtárhoz
                        # ha a célkönyvtár nagyobb, kiírjuk, hogy zárja be a
                        # programot, és hogy a mentés eltarthat egy ideig
                        if s.compare_dirs(full_source, dest_path):
                            # bekérjük a mentési nevet a felhasználótól
                            name = cf.get_param_with_list(item, items_name)

                            # ha a felhasználó nem adott meg fájlnevet
                            if name == "":
                                # az időbélyeg
                                time_stamp = time.strftime('%Y-%m-%d_%T')

                                # az alapértelmezett név
                                default_name = "_.ssh_" + time_stamp

                                # az útvonal a megadott útvonal és az
                                # alapértelmezett név
                                full_path = os.path.join(dest_path,
                                    default_name)

                            # különben
                            else:
                                # ha adott meg nevet, akkor összefűzzük a
                                # megadott útvonallal
                                full_path = os.path.join(dest_path, name)

                            # megpróbáljuk elvégezni a másolást
                            try:
                                shutil.copytree(full_source, full_path)

                            # ha IO hibát kapunk, nem sikerült
                            except IOError:
                                message = ["Az alábbi könyvtár létrehozása "
                                          "sikertelen (I/O hiba):", full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)
                                return 1

                            # ha shutil hibát kapunk, szintén nem sikerült
                            except shutil.Error:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen (másolási hiba):",
                                    full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)
                                return 2

                            # ha OSError-t kapunk, valószínűleg létezik a fájl
                            except OSError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Lehetséges, hogy a megadott könyvtár már "
                                    "létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha nincs .ssh mappa
                            except subprocess.CalledProcessError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Győzödj meg róla, hogy a ~/.ssh könyvtár "
                                    "létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # különben
                            else:
                                # tömbbe tessszük a kiírni valót (szöveg és
                                # fájlnév)
                                message = ["Az alábbi könyvtár sikeresen "
                                          "létrejött:",
                                    full_path]
                                # kiírjuk, hová másoltuk a fájlt
                                cf.print_list(item, message)

                        # ha a célkönyvtár kisebb, hibát írunk ki
                        else:
                            message = "A megadott célkönyvtárban nincs elég " \
                                      "szabad hely!"
                            cf.print_string(item, message, red=True)


        elif x == ord('7'):
            # a menüpont elemei
            item = "UbiSysPy – Rendszeradatok menü – GnuPG könyvtár mentése"

            curses.endwin()  # befejezzük a cursest

            # lekérdezzük a user Home könyvtárát
            user_dir = os.environ["HOME"]

            # a GnuPG könyvtár elérési útja
            full_source = os.path.join(user_dir, ".gnupg")

            # megnézzük, hogy létezik-e a könyvtár
            # ha nem
            if not os.path.isdir(full_source):
                message = ["Hiba: Az alábbi forráskönyvtár nem létezik:",
                    full_source,
                "Győzödj meg róla, hogy a GnuPG program telepítve van."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)

            # ha igen, megyünk tovább
            else:
                # a menüpont elemei
                items_path = [item]
                items_path.append("Add meg, hová szeretnéd menteni a GnuPG "
                                  "könyvtárat.")
                items_path.append("Például: /media/adatok/könyvtár/")

                # bekérjük a mentési útvonalat a felhasználótól
                dest_path = cf.get_param(items_path)

                if not dest_path:  # ha az útvonal nem került megadásra
                    message = "Hiba: Nem adtál meg elérési utat."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                elif not os.path.isdir(dest_path):  # ha a könyvtár nem létezik
                    curses.endwin()  # befejezzük a cursest
                    # hibaüzenet összeillesztése
                    message = ["Hiba: A megadott elérési út NEM létezik:",
                        dest_path]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

                else:  # ha létezik a megadott könyvtár
                    # ha a felhasználónak nincs írási joga a megadott útvonalra
                    if not os.access(dest_path, os.W_OK):
                        curses.endwin()  # befejezzük a cursest
                        # hibaüzenet összeillesztése
                        message = ["Hiba: A megadott elérési út NEM írható:",
                            dest_path]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    else:  # ha a felhasználónak van írási joga
                        # a menüpont elemei
                        items_name = ["Add meg, milyen néven szeretnéd "
                                      "menteni a GnuPG könyvtárat.",
                            "Alapértelmezetten: _.gnupg_időbélyeg",
                            "Mentés előtt zárd be a programot.",
                            "A mentés eltarthat egy ideig a könyvtár "
                            "méretétől és az eszköztől függően."]

                        # lekérdezzük, hogy a célmappa elég nagy-e a mentendő
                        # könyvtárhoz
                        # ha a célkönyvtár nagyobb, kiírjuk, hogy zárja be a
                        # programot, és hogy a mentés eltarthat egy ideig
                        if s.compare_dirs(full_source, dest_path):
                            # bekérjük a mentési nevet a felhasználótól
                            name = cf.get_param_with_list(item, items_name)

                            # ha a felhasználó nem adott meg fájlnevet
                            if name == "":
                                # az időbélyeg
                                time_stamp = time.strftime('%Y-%m-%d_%T')

                                # az alapértelmezett név
                                default_name = "_.gnupg_" + time_stamp

                                # az útvonal a megadott útvonal és az
                                # alapértelmezett név
                                full_path = os.path.join(dest_path,
                                    default_name)

                            # különben
                            else:
                                # ha adott meg nevet, akkor összefűzzük a
                                # megadott útvonallal
                                full_path = os.path.join(dest_path, name)

                            # megpróbáljuk elvégezni a másolást
                            try:
                                shutil.copytree(full_source, full_path)

                            # ha IO hibát kapunk, nem sikerült
                            except IOError:
                                message = ["Az alábbi könyvtár létrehozása "
                                          "sikertelen (I/O hiba):", full_path]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)
                                sys.exit(1)

                            # ha shutil hibát kapunk, szintén nem sikerült
                            except shutil.Error as errno_tuple:
                                # Ha errno.ENXIO = [Errno 6] Nem létezik eszköz
                                # vagy cím hiba lép fel, akkor socketet
                                # próbáltunk másolni, amit nem lehet.
                                # A backupot nem érinti, ezért figyelmen kívül
                                # hagyjuk.
                                # Mivel a shutil.Error-nak nincs errno
                                # attribútuma, workaroundolunk:
                                # a visszatérési tuple-ből listát csinálunk
                                errno_list = list(errno_tuple[0])[0]
                                # kinyerjük belőle az errnót tartalmazó
                                # sztringet
                                errno_string = errno_list[2]

                                # és úgy keresünk rá a 6-ra
                                # ha tartalmazza, megyünk tovább
                                if errno_string.find("6") != -1:
                                    # tömbbe tessszük a kiírni valót (szöveg és
                                    # fájlnév)
                                    message = [
                                        "Az alábbi könyvtár sikeresen "
                                        "létrejött:",
                                        full_path]
                                    # kiírjuk, hová másoltuk a fájlt
                                    cf.print_list(item, message)

                                # ha nem Errno 6 volt, akkor sikertelen a
                                # másolás
                                else:
                                    message = ["Az alábbi könyvtár létrehozása "
                                               "sikertelen (másolási hiba):",
                                        full_path]
                                    # tehát kiírjuk, hogy a művelet
                                    # sikertelen volt
                                    cf.print_list(item, message, red=True)

                            # ha OSError-t kapunk, valószínűleg létezik a fájl
                            except OSError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Lehetséges, hogy a megadott könyvtár már "
                                    "létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # ha nincs .gnupg mappa
                            except subprocess.CalledProcessError:
                                message = ["Az alábbi könyvtár létrehozása "
                                           "sikertelen:", full_path,
                                    "Győzödj meg róla, hogy a ~/.gnupg "
                                    "könyvtár létezik."]
                                # tehát kiírjuk, hogy a művelet sikertelen volt
                                cf.print_list(item, message, red=True)

                            # különben
                            else:
                                # tömbbe tessszük a kiírni valót (szöveg és
                                # fájlnév)
                                message = ["Az alábbi könyvtár sikeresen "
                                          "létrejött:",
                                    full_path]
                                # kiírjuk, hová másoltuk a fájlt
                                cf.print_list(item, message)

                        else:
                            message = "A megadott célkönyvtárban nincs elég " \
                                      "szabad hely!"
                            cf.print_string(item, message, red=True)


    curses.endwin()  # befejezzük a cursest
