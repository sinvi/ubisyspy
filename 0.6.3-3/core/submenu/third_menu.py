#!/usr/bin/env python
# -*- coding: utf-8 -*-


"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület harmadik menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale
import os
import curses

from curses_helpers import curses_functions as cf  # curses függvények
from package_handling import mirrors_ppas as mp
from system import settings as s  # shell kezelés
from system import sys_details as sd  # partíció ellenőrzéshez
from system import sudo

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def third_menu():
    """
    A harmadik menü almenüjét és azok menüpontjait valósítja meg.
    """

    x = 0
    # amíg a felhasználó által megadott szám nem 7, ami az almenüpontok száma
    while x != ord('7'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # színpár def.
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # színpár def.
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # az almenü elemei
        items = [u"UbiSysPy – Rendszerbeállítások menü",
            u"Szám billentyű megnyomásával válassz az alábbi menüpontok közül:",
            u"1) Alapértelmezett alkalmazások listája",
            u"2) Frissítések telepítése",
            u"3) Fontosabb könyvtárak méretének lekérdezése",
            u"4) A swappiness beállítása",
            u"5) Alias hozzáadása",
            u"6) Alapértelmezett shell beállítása",
            u"7) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, items[0].encode("utf-8"),
            curses.A_BOLD | curses.color_pair(2))
        screen.addstr(5, 2, items[1].encode("utf-8"),
            curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))
        screen.addstr(11, 4, items[6].encode("utf-8"))
        screen.addstr(12, 4, items[7].encode("utf-8"))
        screen.addstr(13, 4, items[8].encode("utf-8"))

        screen.refresh()

        x = screen.getch()  # várjuk a felhasználó bemenetét


        if x == ord('1'):  # ha 1, végrehajtjuk a parancsot
            curses.endwin()
            # menü
            menu_str = "UbiSysPy – Rendszerbeállítások menü – Alapértelmezett " \
                       "alkalmazások"
            # meghívjuk a függvényt
            cf.print_function_without_args(menu_str,
                sd.get_default_applications)


        elif x == ord('2'):
            curses.endwin()
            # menü
            menu_str = "UbiSysPy – Rendszerbeállítások menü – Frissítések " \
                       "telepítése"

            # root ellenőrzés
            return_value = sudo.check_root()

            # ha nullával tér vissza, a felhasználó root
            if return_value == 0:
                # üzenet
                message = "A frissítések telepítése hosszabb ideig is " \
                          "eltarthat."
                finish = "Nyomj entert a folytatáshoz"
                # kiírás
                cf.print_string(menu_str, message, finish=finish)

                # meghívjuk a rendszerfrissítő függvényt
                # ha nullával tér vissza, sikeres
                if mp.sys_upgrade(True) == 0:
                    # üzenet
                    message = "A frissítések telepítése sikeres volt."
                    # kiírás
                    cf.print_string(menu_str, message)

                # ha eggyel tér vissza, sikertelen
                elif mp.sys_upgrade(True) == 1:
                    # hibaüzenet
                    message = "A frissítések telepítése sikertelen volt."
                    # kiírás
                    cf.print_string(menu_str, message, red=True)

                # különben, ha kettővel tér vissza, nem sikerült a csomaglista
                # frissítése
                else:
                    # hibaüzenet
                    message = "A csomaglista frissítése sikertelen volt."
                    # kiírás
                    cf.print_string(menu_str, message, red=True)

            # ha 2-vel tér vissza, nincs sudo joga
            elif return_value == 2:
                message = "Nincs sudo jogod, így a művelet nem folytatható."
                # kiírjuk a hibaüzenetet
                cf.print_string(menu_str, message, red=True)

            # ha 1-gyel tér vissza, van sudo joga
            elif return_value == 1:
                # bekérő szöveg
                prompt_str = "Add meg a jelszavad:"
                # leírás a felhasználónak
                descr = "A következő művelethez root jogra lesz szükséged."
                # jelszó bekérése a tükörváltáshoz
                passwd = cf.get_pass(menu_str, descr, prompt_str)

                # ha a felhasználó nem adott meg jelszót
                if not passwd:
                    message = "Hiba: Nem adtál meg jelszót."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(menu_str, message, red=True)

                # ha a felhasználó adott meg jelszót
                else:
                    # ellenőrizzük a sudo jelszót
                    if not sudo.sudo_test(passwd):
                        message = "Hitelesítési hiba. Elírtad a jelszavad."
                        cf.print_string(menu_str, message, red=True)

                    # ha igazzal tér vissza, rendben van a jelszó
                    else:
                        # üzenet
                        message = "A frissítések telepítése hosszabb ideig " \
                                  "is eltarthat."
                        finish = "Nyomj entert a folytatáshoz"
                        # kiírás
                        cf.print_string(menu_str, message, finish=finish)
                        # meghívjuk a rendszerfrissítő függvényt
                        # ha nullával tér vissza, sikeres
                        if mp.sys_upgrade(passwd) == 0:
                            # üzenet
                            message = "A frissítések telepítése sikeres volt."
                            # kiírás
                            cf.print_string(menu_str, message)

                        # ha eggyel tér vissza, sikertelen
                        elif mp.sys_upgrade(passwd) == 1:
                            # hibaüzenet
                            message = "A frissítések telepítése sikertelen " \
                                      "volt."
                            # kiírás
                            cf.print_string(menu_str, message, red=True)

                        # ha kettővel tér vissza, nem sikerült a a csomaglista
                        # frissítése
                        else:
                            # hibaüzenet
                            message = "A csomaglista frissítése sikertelen " \
                                      "volt."
                            # kiírás
                            cf.print_string(menu_str, message, red=True)

                # felülírjuk a felhasználó jelszavát
                passwd = os.urandom(len(passwd))


        elif x == ord('3'):  # ha 4, végrehajtjuk a parancsot
            curses.endwin()
            # menü neve
            menu_str = "UbiSysPy – Rendszerbeállítások menü – Könyvtárak " \
                       "méreteinek lekérdezése"
            # függvény meghívása
            cf.print_function_without_args(menu_str, sd.get_partitions)


        elif x == ord('4'):
            curses.endwin()

            # menüpont neve
            item = "UbiSysPy – Rendszerbeállítások menü – Swappiness beállítás"
            # aktuális swappiness érték lekérdezése
            swappiness = s.get_current_swappiness()
            swappiness.append("")
            swappiness.append("A módosítás az aktuális munkamenetre érvényes.")
            swappiness.append("Később lesz lehetőség a beállítások "
                              "véglegesítésére.")
            swappiness.append("")
            # a kérdés
            swappiness.append("Szeretnéd megváltoztatni? (igen/nem)")

            # bekérjük a választ
            answer = cf.get_param_with_list(item, swappiness)

            # ha a felhasználó nem adott meg választ
            if not answer:
                message = "Hiba: Nem adtál meg választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha a válasz igen
            elif answer == "igen" or answer == "Igen":

                # root ellenőrzés
                return_value = sudo.check_root()

                # ha nullával tér vissza, a felhasználó root
                if return_value == 0:
                    # bekérjük az új swappiness értéket
                    ask_swappiness = []
                    ask_swappiness.append(item)
                    descr = "Add meg az új swappiness értéket:"
                    ask_swappiness.append(descr)
                    sample = "Az alapértelmezett érték 60."
                    ask_swappiness.append(sample)

                    # bekérjük az új értéket
                    value = cf.get_param(ask_swappiness)

                    # ha a felhasználó nem adott meg értéket
                    if not value:
                        message = "Hiba: Nem adtál meg értéket."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha adott meg értéket
                    else:
                        # meghívjuk a függvényt
                        # az érték megfelelőségét a program vizsgálja
                        return_code = s.temp_change_swappiness(value, True)

                        # ha kettővel tér vissza, rossz az érték
                        if return_code == 2:
                            # hibaüzenet
                            message = "Hiba: Az érték nem megfelelő."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, sikertelen volt a művelet
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A swappiness érték beállítása " \
                                      "sikertelen volt."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, IO hiba a backupnál
                        elif return_code == 3:
                            # hibaüzenet
                            message = "A /etc/sysctl.conf biztonsági " \
                                      "mentése sikertelen (I/O hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha néggyel tér vissza, shutil hiba a backupnál
                        elif return_code == 4:
                            # hibaüzenet
                            message = "A /etc/sysctl.conf biztonsági " \
                                      "mentése sikertelen (shutil hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha öttel tér vissza, OS hiba a backupnál
                        elif return_code == 5:
                            # hibaüzenet
                            message = "A /etc/sysctl.conf biztonsági " \
                                      "mentése sikertelen (OS hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # különben sikerült
                        else:
                            # megkérdezzük a véglegesítést
                            question = []
                            question.append("A swappiness érték beállítása "
                                            "sikeres volt.")
                            question.append("Szeretnéd véglegesíteni a "
                                            "beállítást? (igen/nem)")

                            # megkérdezzük
                            answer = cf.get_param_with_list(item, question)

                            # ha a felhasználó nem adott meg választ
                            if not answer:
                                # hibaüzenet
                                message = "Hiba: Nem adtál meg választ."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha nemmel válaszolt
                            elif answer.find("nem") != -1 or answer.find(
                                    "Nem") != -1:
                                # üzenet
                                message = "Nem folytatjuk a műveletet."
                                cf.print_string(item, message)

                            # ha igennel válaszolt
                            elif answer.find("igen") != -1 or answer.find(
                                    "Igen") != -1:
                                # meghívjuk a függvényt
                                return_code = s.change_swappiness(value, True)

                                # ha kettővel tér vissza,
                                # nem sikerült a fáj visszamásolása
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/sysctl.conf visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, sikertelen
                                # a tulajdonos módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/sysctl.conf " \
                                              "jogosultságainak beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hárommal tér vissza, IO hiba a backupnál
                                elif return_code == 3:
                                    # hibaüzenet
                                    message = "A /etc/sysctl.conf biztonsági " \
                                              "mentése sikertelen (I/O hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha néggyel tér vissza, shutil hiba a backupnál
                                elif return_code == 4:
                                    # hibaüzenet
                                    message = "A /etc/sysctl.conf biztonsági " \
                                              "mentése sikertelen (shutil " \
                                              "hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha öttel tér vissza, OS hiba a backupnál
                                elif return_code == 5:
                                    # hibaüzenet
                                    message = "A /etc/sysctl.conf biztonsági " \
                                              "mentése sikertelen (OS hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # különben sikerült
                                else:
                                    # üzenet
                                    message = "A swappiness beállítás " \
                                              "véglegesítése sikeres volt."
                                    # kiírjuk azüzenetet
                                    cf.print_string(item, message)

                            # különben
                            else:
                                message = "Hiba: Nem adtál meg felismerhető " \
                                          "választ."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif return_value == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif return_value == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó adott meg jelszót
                    else:
                        # ellenőrizzük a sudo jelszót
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                        # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            # bekérjük az új swappiness értéket
                            ask_swappiness = []
                            ask_swappiness.append(item)
                            descr = "Add meg az új swappiness értéket:"
                            ask_swappiness.append(descr)
                            sample = "Az alapértelmezett érték 60."
                            ask_swappiness.append(sample)

                            # bekérjük az új értéket
                            value = cf.get_param(ask_swappiness)

                            # ha a felhasználó nem adott meg értéket
                            if not value:
                                message = "Hiba: Nem adtál meg értéket."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha a felhasználó adott meg értéket
                            else:
                                # meghívjuk a függvényt
                                # az érték megfelelőségét a program vizsgálja
                                return_code = s.temp_change_swappiness(value,
                                    passwd)

                                # ha kettővel tér vissza, rossz az érték
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Hiba: Az érték nem megfelelő."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, sikertelen volt a
                                # művelet
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A swappiness érték beállítása " \
                                              "sikertelen volt."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # különben sikerült
                                else:
                                    # megkérdezzük a véglegesítést
                                    question = []
                                    question.append("A swappiness érték "
                                                    "beállítása sikeres volt.")
                                    question.append("Szeretnéd véglegesíteni "
                                                    "a beállítást? (igen/nem)")

                                    # megkérdezzük
                                    answer = cf.get_param_with_list(item,
                                        question)

                                    # ha a felhasználó nem adott meg választ
                                    if not answer:
                                        message = "Hiba: Nem adtál meg választ."
                                        # kiírjuk a hibaüzenetet
                                        cf.print_string(item, message, red=True)

                                    # ha nemmel válaszolt
                                    elif answer.find("nem") != -1 or \
                                            answer.find("Nem") != -1:
                                        message = "Nem folytatjuk a műveletet."
                                        cf.print_string(item, message)

                                    # ha igennel válaszolt
                                    elif answer.find("igen") != -1 or \
                                            answer.find("Igen") != -1:
                                        # meghívjuk a függvényt
                                        return_code = s.change_swappiness(
                                            value, passwd)

                                        # ha kettővel tér vissza,
                                        # nem sikerült a fáj visszamásolása
                                        if return_code == 2:
                                            # hibaüzenet
                                            message = "Nem sikerült a " \
                                                      "/etc/sysctl.conf " \
                                                      "visszamásolása."
                                            # kiírjuk a hibaüzenetet
                                            cf.print_string(item, message,
                                                red=True)

                                        # ha eggyel tér vissza, sikertelen
                                        # a tulajdonos módosítása
                                        elif return_code == 1:
                                            # hibaüzenet
                                            message = "A /etc/sysctl.conf " \
                                                      "jogosultságainak " \
                                                      "beállítása sikertelen."
                                            # kiírjuk a hibaüzenetet
                                            cf.print_string(item, message,
                                                red=True)

                                        # ha hárommal tér vissza, IO hiba a
                                        # backupnál
                                        elif return_code == 3:
                                            # hibaüzenet
                                            message = "A /etc/sysctl.conf " \
                                                      "biztonsági mentése " \
                                                      "sikertelen (I/O hiba)."
                                            # kiírjuk a hibaüzenetet
                                            cf.print_string(item, message,
                                                red=True)

                                        # ha néggyel tér vissza, shutil hiba
                                        # a backupnál
                                        elif return_code == 4:
                                            # hibaüzenet
                                            message = "A /etc/sysctl.conf " \
                                                      "biztonsági mentése " \
                                                      "sikertelen (shutil " \
                                                      "hiba)."
                                            # kiírjuk a hibaüzenetet
                                            cf.print_string(item, message,
                                                red=True)

                                        # ha öttel tér vissza, OS hiba a
                                        # backupnál
                                        elif return_code == 5:
                                            # hibaüzenet
                                            message = "A /etc/sysctl.conf " \
                                                      "biztonsági mentése " \
                                                      "sikertelen (OS hiba)."
                                            # kiírjuk a hibaüzenetet
                                            cf.print_string(item, message,
                                                red=True)

                                        # különben sikerült
                                        else:
                                            # üzenet
                                            message = "A swappiness beállítás" \
                                                      " véglegesítése " \
                                                      "sikeres volt."
                                            # kiírjuk azüzenetet
                                            cf.print_string(item, message)

                                    # különben
                                    else:
                                        message = "Hiba: Nem adtál meg " \
                                                  "felismerhető választ."
                                        # kiírjuk a hibaüzenetet
                                        cf.print_string(item, message, red=True)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))

            # ha a válasz nem
            elif answer == "nem" or answer == "Nem":
                message = "Nem folytatjuk a műveletet."
                # kiírjuk az üzenetet
                cf.print_string(item, message)

            # ha a válasz ismeretlen
            else:
                message = "Hiba: Nem adtál meg felismerhető választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)


        elif x == ord('5'):
            curses.endwin()
            # menü neve
            item = "UbiSysPy – Rendszerbeállítások menü – Alias hozzáadása"

            # alias bekérő tömb
            get_alias = ["Az aliassal felülírhatsz meglévő parancsokat is."]
            get_alias.append("Például az 'ls' aliast az 'ls -al' paranccsal.")
            get_alias.append("Vagy megadhatsz tetszőleges, új neveket is.")
            get_alias.append("Például a 'cls' aliast az 'ls --color=auto' "
                             "paranccsal.")
            get_alias.append("Add meg a hozzáadni kívánt alias nevét.")
            get_alias.append("Például 'ls' vagy 'cls', idézőjelek nélkül.")

            # bekérjük az alias nevét
            alias = cf.get_param_with_list(item, get_alias)

            # ha nem adott meg aliast
            if not alias:
                # hibaüzenet
                message = "Nem adtál meg alias nevet."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha adott meg alias nevet
            else:
                # parancs bekérő tömb
                get_command = ["Add meg az előbb megadott alias névhez "
                               "tartozó parancsot."]
                get_command.append("Például 'ls -al' vagy 'sudo apt-get "
                                   "update', idézőjelek nélkül.")

                # bekérjük a parancsot is
                command = cf.get_param_with_list(item, get_command)

                # ha nem adott meg parancsot
                if not command:
                    # hibaüzenet
                    message = "Nem adtál meg parancsot az aliashoz."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha adott meg parancsot a felhasználó, meghívjuk a hozzáadó
                # függvényt
                else:
                    # elmentjük az alias hozzáadó függvény visszatérési
                    # tuple-jét
                    add_alias_list = s.add_alias(alias, command)
                    # a visszatérési érték az első elem
                    return_value = add_alias_list[1]
                    # a kiírandó sztringtömb pedig a nulladik elem
                    message = add_alias_list[0]

                    # ha hárommal tér vissza, IO hiba a backupnál
                    if return_value == 3:
                        # hibaüzenet
                        message = "A ~/.bash_aliases biztonsági " \
                                  "mentése sikertelen (I/O hiba)."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha néggyel tér vissza, shutil hiba a backupnál
                    elif return_value == 4:
                        # hibaüzenet
                        message = "A ~/.bash_aliases biztonsági " \
                                  "mentése sikertelen (shutil hiba)."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha öttel tér vissza, OS hiba a backupnál
                    elif return_value == 5:
                        # hibaüzenet
                        message = "A ~/.bash_aliases biztonsági " \
                                  "mentése sikertelen (OS hiba)."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a visszatérési érték True:
                    elif return_value:
                        # sortörés
                        message.append("")
                        # üzenet
                        message.append("Az alias hozzáadása sikeresen "
                                       "megtörtént.")
                        # kiírjuk az üzenetet
                        cf.print_list(item, message)

                    # ha False, nem sikerült a hozzáadás
                    else:
                        # hibaüzenet
                        message = "Az alias hozzáadása sikertelen."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)


        elif x == ord('6'):
            curses.endwin()

            # jelenlegi shell lekérdezése
            shell = s.get_default_shell()

            # menü neve
            item = "UbiSysPy – Rendszerbeállítások menü – Shell módosítása"
            # az akutális shellt tartalmazó tömb
            items = shell
            items.append("A módosítás a következő bejelentkezéskor lép életbe.")
            items.append("")
            # a kérdés
            items.append("Szeretnéd megváltoztatni? (igen/nem)")

            # bekérjük a választ
            answer = cf.get_param_with_list(item, items)

            # ha a felhasználó nem adott meg választ
            if not answer:
                message = "Hiba: Nem adtál meg választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha a válasz igen
            elif answer == "igen" or answer == "Igen":

                # root ellenőrzés
                return_value = sudo.check_root()

                # ha nullával tér vissza, a felhasználó root
                if return_value == 0:

                    # az elérhető shelleket tömbbe mentjük
                    shell_list = s.get_shell_list()

                    items = shell_list
                    items.insert(0, "Az elérhető shellek listája:")
                    # a tömbhöz adunk egy új sort és a bekérő szöveget
                    items.append("")
                    items.append("Add meg a kiválasztott shell számát:")

                    # bekérjük a választ
                    choose = cf.get_param_with_list(item, items)

                    # ha a felhasználó nem adott meg választ
                    if not choose:
                        message = "Hiba: Nem adtál meg választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # különben megnézzük, melyik elemet választotta
                    else:
                        # az elérhető shellek száma
                        shells = len(shell_list)

                        # ha a válasz szám és belefér a tömb méretébe
                        if choose.isdigit() and int(choose) <= shells:
                            # bejárjuk a shell_list tömböt, hogy megtudjuk,
                            # mit választott a felhasználó
                            for shell in shell_list:
                                # ha a számozott shell sorban szerepel a
                                # felhasználó válasza, azt választotta
                                if shell.find(choose) != -1:
                                    # a shell sor két szóból áll, a második maga
                                    # a shell, az első a sorszáma
                                    new_shell = shell.split()[1]

                                    # megvizsgáljuk, hogy az rbash került-e
                                    # kiválasztásra
                                    if new_shell.find("rbash") != -1:
                                        message = []
                                        message.append(item)
                                        message.append("Korlázott shellt "
                                                       "választottál "
                                                       "alapértelmezettnek.")
                                        message.append("Biztosan ezt szeretnéd?"
                                                       " (igen/nem)")

                                        # bekérjük a választ
                                        confirmation = cf.get_param(message)

                                        # ha nem adott meg választ
                                        if not confirmation:
                                            message = "Nem adtál meg választ."
                                            cf.print_string(item, message,
                                                red=True)
                                            break

                                        # ha nemmel válaszolt
                                        elif confirmation.find("nem") != -1 or \
                                                confirmation.find("Nem") != -1:
                                            message = "Nem folytatjuk a " \
                                                      "műveletet."
                                            cf.print_string(item, message)
                                            break

                                        # ha igennel válaszolt
                                        elif confirmation.find("igen") != -1 \
                                                or \
                                                confirmation.find("Igen") != -1:

                                            # meghívjuk a shell váltó függvényt
                                            # és ha igazzal tér vissza, sikeres
                                            # volt a csere
                                            if s.change_shell(new_shell, True):
                                                message = "A shell cseréje " \
                                                          "sikeres volt."
                                                cf.print_string(item, message)

                                            # különben, sikertelen
                                            else:
                                                message = "A shell cseréje " \
                                                          "sikertelen volt."
                                                cf.print_string(item,
                                                    message, red=True)

                                        # különben
                                        else:
                                            message = "Nem adtál meg " \
                                                      "felismerhető választ."
                                            cf.print_string(item, message,
                                                red=True)
                                            break

                                    # meghívjuk a shell váltó függvényt és
                                    # ha igazzal tér vissza, sikeres volt a
                                    # csere
                                    if s.change_shell(new_shell, True):
                                        message = "A shell cseréje sikeres " \
                                                  "volt."
                                        cf.print_string(item, message)

                                    # különben, sikertelen
                                    else:
                                        message = "A shell cseréje " \
                                                  "sikertelen volt."
                                        cf.print_string(item, message, red=True)

                        # ha a válasz ismeretlen (nem esik a tömbbe vagy nem
                        # szám)
                        else:
                            message = "Hiba: Nem adtál meg felismerhető " \
                                      "választ."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif return_value == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif return_value == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # különben
                    else:
                        # ellenőrizzük a sudo jelszót
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                            # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            # az elérhető shelleket tömbbe mentjük
                            shell_list = s.get_shell_list()

                            items = shell_list
                            items.insert(0, "Az elérhető shellek listája:")
                            # a tömbhöz adunk egy új sort és a bekérő szöveget
                            items.append("")
                            items.append("Add meg a kiválasztott shell számát:")

                            # bekérjük a választ
                            choose = cf.get_param_with_list(item, items)

                            # az elérhető shellek száma
                            shells = len(shell_list)

                            # ha a válasz szám és belefér a tömb méretébe
                            if choose.isdigit() and int(choose) <= shells:
                                # bejárjuk a shell_list tömböt, hogy megtudjuk,
                                # mit választott a felhasználó
                                for shell in shell_list:
                                    # ha a számozott shell sorban szerepel a
                                    # felhasználó válasza, azt választotta
                                    if shell.find(choose) != -1:
                                        # a shell sor két szóból áll, a második
                                        # maga a shell, az első a sorszáma
                                        new_shell = shell.split()[1]

                                        # megvizsgáljuk, hogy az rbash került-e
                                        # kiválasztásra
                                        if new_shell.find("rbash") != -1:
                                            message = []
                                            message.append(item)
                                            message.append("Korlázott shellt "
                                                           "választottál "
                                                           "alapértelmezett "
                                                           "shellnek.")
                                            message.append("Biztosan ezt "
                                                           "szeretnéd? "
                                                           "(igen/nem)")

                                            # bekérjük a választ
                                            confirmation = cf.get_param(message)

                                            # ha nem adott meg választ
                                            if not confirmation:
                                                message = "Nem adtál meg " \
                                                          "választ."
                                                cf.print_string(item, message,
                                                    red=True)
                                                break

                                            # ha nemmel válaszolt
                                            elif confirmation.find("nem") != -1\
                                                    or \
                                                    confirmation.find("Nem") \
                                                    != -1:
                                                message = "Nem folytatjuk a " \
                                                          "műveletet."
                                                cf.print_string(item, message)
                                                break

                                            # ha igennel válaszolt
                                            elif confirmation.find("igen") !=\
                                                    -1 or \
                                                    confirmation.find("Igen")\
                                                    != -1:

                                                # meghívjuk a shell váltó
                                                # függvényt és ha igazzal tér
                                                #  vissza, sikeres volt a csere
                                                if s.change_shell(new_shell,
                                                        passwd):
                                                    message = "A shell " \
                                                              "cseréje " \
                                                              "sikeres volt."
                                                    cf.print_string(item,
                                                        message)

                                                # különben, sikertelen
                                                else:
                                                    message = "A shell " \
                                                              "cseréje " \
                                                              "sikertelen volt."
                                                    cf.print_string(item,
                                                        message, red=True)

                                            # különben
                                            else:
                                                message = "Nem adtál meg " \
                                                          "felismerhető " \
                                                          "választ."
                                                cf.print_string(item,
                                                    message, red=True)
                                                break

                                        # meghívjuk a shell váltó függvényt és
                                        # ha igazzal tér vissza, sikeres volt a
                                        # csere
                                        if s.change_shell(new_shell, passwd):
                                            message = "A shell cseréje " \
                                                      "sikeres volt."
                                            cf.print_string(item, message)

                                        # különben, sikertelen
                                        else:
                                            message = "A shell cseréje " \
                                                      "sikertelen volt."
                                            cf.print_string(item, message,
                                                red=True)

                            # ha a válasz ismeretlen (nem esik a tömbbe vagy nem
                            # szám)
                            else:
                                message = "Hiba: Nem adtál meg felismerhető " \
                                          "választ."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))

            # ha a válasz nem
            elif answer == "nem" or answer == "Nem":
                message = "Nem folytatjuk a műveletet."
                # kiírjuk az üzenetet
                cf.print_string(item, message)

            # ha a válasz ismeretlen
            else:
                message = "Hiba: Nem adtál meg felismerhető választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)
