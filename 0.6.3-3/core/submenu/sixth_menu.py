#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület hatodik menüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import locale

import curses
from curses_helpers import curses_functions as cf


locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def sixth_menu():
    """
    A hatodik menü almenüjét és azok menüpontjait valósítja meg.
    """

    x = 0
    # amíg a felhasználó által megadott szám nem 6, ami az almenüpontok száma
    while x != ord('4'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # színpár def.
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # színpár def.
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # az almenü elemei
        items = [u"UbiSysPy – A programról menü",
        u"Szám billentyű megnyomásával válassz az alábbi menüpontok közül:",
        u"1) A program célja",
        u"2) A program készítői",
        u"3) A program licence",
        u"4) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, items[0].encode("utf-8"),
            curses.A_BOLD | curses.color_pair(2))
        screen.addstr(5, 2, items[1].encode("utf-8"),
            curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))

        screen.refresh()

        x = screen.getch()  # várjuk a felhasználó bemenetét

        if x == ord('1'):   # ha 1, végrehajtjuk a parancsot
            curses.endwin()
            item = "UbiSysPy – A programról menü – A program célja"
            items = ["Az UbiSysPy rendszeradat lekérdező, beállító és naplózó "
                     "program",
            "abban a reményben készült, hogy hasznos lesz.",
            "",
            "A program célja, hogy lehetővé tegye és megkönnyítse:",
            "– a rendszeradatok lekérdezését és áttekintését",
            "– a rendszernaplók megtekintését és mentését",
            "– a háttértárak állapotának vizsgálatát",
            "– a telepített csomagok és tárolók kezelését",
            "– a biztonsági mentés beállításait",
            "– egyéb haladóbb rendszerműveleteket."]

            cf.print_list(item, items)

        elif x == ord('2'):
            curses.endwin()
            item = "UbiSysPy – A programról menü – A program készítői"
            items = ["Az UbiSysPy rendszeradat lekérdező, beállító és naplózó "
                     "programot",
            "Sinkovics Vivien készítette.",
            "",
            "Elérhetőség: sinkovics.vivien@gmail.com",
            "",
            "A program jelenlegi verziója: 0.6.3-3",
            "",
            "A program letölthető a Bitbucket oldaláról:",
            "https://bitbucket.org/sinvi/ubisyspy/"]

            cf.print_list(item, items)

        elif x == ord('3'):
            curses.endwin()
            item = "UbiSysPy – A programról menü – A program licence"
            items = ["Az UbiSysPy program mindenféle garancia nélkül érkezik,",
            "ám a készítők minden észrevételt, javaslatot és",
            "hibabejelentést szívesen fogadnak.",
            "",
            "Az UbiSysPy GNU General Public License v2 alatt került kiadásra.",
            "Ez azt jelenti, hogy a program szabadon letölthető",
            "megismerhető, módosítható, terjeszthető ezen licenc alatt.",
            "",
            "A GPLv2 licenc teljes szövege megtalálható az alábbi oldalon:",
            "http://www.gnu.org/licenses/gpl-2.0.html",
            "",
            "A nem hivatalos magyar fordítása pedig itt olvasható:",
            "http://gnu.hu/gpl.html"]

            cf.print_list(item, items)

    curses.endwin()  # befejezzük a cursest
