#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Copyright (c) 2016, Sinkovics Vivien

A curses felület ötödik menüjének első almenüje.

This file is part of UbiSysPy.

UbiSysPy is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License,
or any later version.

UbiSysPy is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Sinkovics Vivien"
__copyright__ = "Copyright 2016"
__license__ = "GPLv2"
__version__ = "0.6.3-3"
__email__ = "sinkovics.vivien@gmail.com"
__status__ = "Development"

import curses
import locale
import os

from curses_helpers import curses_functions as cf  # curses függvények
from dependency import depscanner as d  # függőség ellenőrzés
from graph import cron as c
from package_handling import install_package as ip
from system import sysstat as stat  # sysstat beállításhoz
from system import sudo

locale.setlocale(locale.LC_ALL, "")  # az UTF-8-hoz szükséges


def first_submenu():
    """
    Az ötödik menü első almenüjét és azok menüpontjait valósítja meg.
    """

    x = 0
    # amíg a felhasználó által megadott szám nem 7, ami az almenüpontok száma
    while x != ord('7'):
        screen = curses.initscr()  # új ablakot inicializál
        curses.start_color()  # lehetővé teszi a színek használatát
        # színpár def.
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        # színpár def.
        curses.init_pair(2, curses.COLOR_YELLOW, curses.COLOR_BLUE)

        screen.bkgd(' ', curses.color_pair(1))  # háttérszín beállítás
        screen.refresh()  # képernyő frissítése

        # az almenü elemei
        items = [u"UbiSysPy – Naplózás menü – Naplózás beállítása",
        u"Szám billentyű megnyomásával válassz az alábbi menüpontok közül:",
        u"1) A sysstat ellenőrzése, telepítése, alapbeállítása",
        u"2) Naplók megőrzési idejének beállítása",
        u"3) Naplók tömörítési idejének beállítása",
        u"4) Naplók tömörítési programjának beállítása",
        u"5) Naplózás ütemezésének beállítása",
        u"6) Grafikon készítés beállítása",
        u"7) Visszalépés a szülőmenübe"]

        # kiírjuk az almenü elemeit
        screen.clear()
        screen.border(0)
        screen.addstr(2, 2, items[0].encode("utf-8"),
            curses.A_BOLD | curses.color_pair(2))
        screen.addstr(5, 2, items[1].encode("utf-8"),
            curses.A_UNDERLINE | curses.color_pair(2))
        screen.addstr(7, 4, items[2].encode("utf-8"))
        screen.addstr(8, 4, items[3].encode("utf-8"))
        screen.addstr(9, 4, items[4].encode("utf-8"))
        screen.addstr(10, 4, items[5].encode("utf-8"))
        screen.addstr(11, 4, items[6].encode("utf-8"))
        screen.addstr(12, 4, items[7].encode("utf-8"))
        screen.addstr(13, 4, items[8].encode("utf-8"))

        screen.refresh()  # képernyő frissítése

        x = screen.getch()  # várjuk a felhasználó bemenetét

        if x == ord('1'):
            # a menüpont neve
            item = "UbiSysPy – Naplózás menü – Naplózás beállítása – " \
                   "Alapbeállítások"
            curses.endwin()  # befejezzük a cursest

            # felhasználó és sudo jog ellenőrzése
            # ha 0-val tér vissza, a user root
            if sudo.check_root() == 0:
                # sudo nélkül hívjuk meg a parancsokat
                # tájákoztató sztring a print_list()-be
                items = ["A naplózáshoz és annak beállításához az alábbi,",
                    "sysstat nevű csomagra van szükség."]
                # záró üzenet
                finish = "Nyomj entert a sysstat ellenőrzéséhez"

                # a tájékoztató kiírása menüvel, záró üzenettel
                cf.print_list(item, items, finish=finish)

                # ha van telepített függőség
                if d.dep_check("sysstat"):
                    # visszatérési érték elmentése
                    return_code = stat.sysstat_init(True)

                    # ha kettővel tér vissza, nem sikerült a másolás
                    if return_code == 2:
                        # hibaüzenet
                        message = "Nem sikerült a /etc/default/sysstat " \
                                  "visszamásolása."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha eggyel tér vissza, nem sikerült a fájl
                    # tulajdonosának módosítása
                    elif return_code == 1:
                        # hibaüzenet
                        message = "A /etc/default/sysstat jogosultságainak " \
                                  "beállítása sikertelen."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha hárommal tér vissza, IO hiba a backupnál
                    elif return_code == 3:
                        # hibaüzenet
                        message = "A /etc/default/sysstat biztonsági mentése " \
                                  "sikertelen (I/O hiba)."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha néggyel tér vissza, shutil hiba a backupnál
                    elif return_code == 4:
                        # hibaüzenet
                        message = "A /etc/default/sysstat biztonsági mentése " \
                                  "sikertelen (shutil hiba)."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha öttel tér vissza, OS hiba a backupnál
                    elif return_code == 5:
                        # hibaüzenet
                        message = "A /etc/default/sysstat biztonsági mentése " \
                                  "sikertelen (OS hiba)."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha hattal tér vissza, már engedélyeze volt a naplózás
                    elif return_code == 6:
                        # üzenet
                        message = "A naplózás már engedélyezve van."
                        # kiírjuk az üzenetet
                        cf.print_string(item, message)

                    # ha nullával tért vissza, sikeres volt a művelet
                    elif return_code == 0:
                        # újraindítjuk a szolgáltatást
                        if stat.sysstat_restart(True) == 0:
                            message = ["A sysstat engedélyezése, valamint",
                                "a szolgáltatás újraindítása sikeres volt."]
                            # kiírjuk
                            cf.print_list(item, message)

                        # ha a szolgáltatás nem indult újra
                        else:
                            # hibaüzenet
                            message = [
                                "Hiba a telepítés után:",
                                "A sysstat engedélyezése, sikeres volt, ám",
                                "a szolgáltatás újraindítása sikertelen."]
                            # kiírjuk
                            cf.print_list(item, message, red=True)

                # ha nincs telepített függőség
                else:
                    # kiírnadó elemek
                    item = "UbiSysPy – Naplózás menü – Naplózás beállítása"
                    items = ["A folytatáshoz szükség van a sysstat nevű "
                             "csomagra.", "A telepítés eltarthat egy ideig.",
                        "Szeretnéd telepíteni a csomagot? (igen/nem)"]

                    # válasz bekérése
                    answer = cf.get_param_with_list(item, items)

                    # ha a felhasználó nem adott meg választ
                    if not answer:
                        message = "Hiba: Nem adtál meg választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a válasz igen
                    elif answer == "igen" or answer == "Igen":
                        finish = "Nyomj entert a befejezéshez"
                        package = "sysstat"

                        return_value = ip.install_package(package, True)

                        # ha 2-vel tér vissza, nem sikerült a csomaglista
                        # frissítése
                        if return_value == 2:
                            message = "A csomaglista frissítése sikertelen."
                            cf.print_string(item, message, red=True,
                                finish=finish)

                        # ha 0-val tér vissza, sikeres volt a programtelepítés
                        elif return_value == 0:
                            # ekkor elvégezzük az alabpbeállításokat
                            # visszatérési érték elmentése
                            return_code = stat.sysstat_init(True)

                            # ha kettővel tér vissza, nem sikerült a másolás
                            if return_code == 2:
                                # hibaüzenet
                                message = "Nem sikerült a " \
                                          "/etc/default/sysstat visszamásolása."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha eggyel tér vissza, nem sikerült a fájl
                            # tulajdonosának módosítása
                            elif return_code == 1:
                                # hibaüzenet
                                message = "A /etc/default/sysstat " \
                                          "jogosultságainak beállítása " \
                                          "sikertelen."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha hárommal tér vissza, IO hiba a backupnál
                            elif return_code == 3:
                                # hibaüzenet
                                message = "A /etc/default/sysstat biztonsági " \
                                          "mentése sikertelen (I/O hiba)."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha néggyel tér vissza, shutil hiba a backupnál
                            elif return_code == 4:
                                # hibaüzenet
                                message = "A /etc/default/sysstat biztonsági " \
                                          "mentése sikertelen (shutil hiba)."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha öttel tér vissza, OS hiba a backupnál
                            elif return_code == 5:
                                # hibaüzenet
                                message = "A /etc/default/sysstat biztonsági " \
                                          "mentése sikertelen (OS hiba)."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha hattal tér vissza, már engedélyeze volt a
                            # naplózás
                            elif return_code == 6:
                                # üzenet
                                message = "A naplózás már engedélyezve van."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message)

                            # ha nullával tért vissza, sikeres volt a művelet
                            elif return_code == 0:
                                # újraindítjuk a szolgáltatást
                                if stat.sysstat_restart(True) == 0:
                                    message = ["A sysstat engedélyezése, "
                                               "valamint",
                                        "a szolgáltatás újraindítása sikeres "
                                        "volt."]
                                    # kiírjuk
                                    cf.print_list(item, message)

                                # ha a szolgáltatás nem indult újra
                                else:
                                    # hibaüzenet
                                    message = [
                                        "Hiba a telepítés után:",
                                        "A sysstat engedélyezése, sikeres "
                                        "volt, ám",
                                        "a szolgáltatás újraindítása "
                                        "sikertelen."]
                                    # kiírjuk
                                    cf.print_list(item, message, red=True)

                        # ha 1-gyel tér vissza, sikertelen volt a telepítés
                        else:
                            message = "A(z) {p} program telepítése " \
                                      "sikertelen volt.".format(p=package)
                            cf.print_string(item, message, red=True,
                                finish=finish)

                    # ha a válasz nem
                    elif answer == "nem" or answer == "Nem":
                        message = "Nem telepítjük a csomagot."
                        cf.print_string(item, message)  # kiírjuk az üzenetet

                    # ha a válasz ismeretlen
                    else:
                        message = "Hiba: Nem adtál meg felismerhető választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

            # ha 2-vel tér vissza, nincs sudo joga
            elif sudo.check_root() == 2:
                message = "Nincs sudo jogod, így a művelet nem folytatható."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha 1-gyel tér vissza, van sudo joga
            elif sudo.check_root() == 1:
                # tájákoztató sztring a print_list()-be
                items = ["A naplózáshoz és annak beállításához az alábbi,",
                    "sysstat nevű csomagra van szükség."]
                # záró üzenet
                finish = "Nyomj entert a sysstat ellenőrzéséhez"

                # a tájékoztató kiírása menüvel, záró üzenettel
                cf.print_list(item, items, finish=finish)

                # ha van telepített függőség
                if d.dep_check("sysstat"):
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A naplózás beállításához root jogra lesz " \
                            "szükséged."
                    # jelszó bekérése
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha adott meg jelszót
                    else:
                        # sudo jelszó ellenőrzés
                        # ha hamissal tér vissza, auth. hiba
                        if not sudo.sudo_test(passwd):
                            message = []
                            message1 = "Hitelesítési hiba"
                            message2 = "Elgépelted a jelszavadat."
                            message.append(message1)
                            message.append(message2)
                            # kiírjuk a hibaüzenetet
                            cf.print_list(item, message, red=True)
                            # kilépünk a ciklusból
                            break

                        # ha igazzal tér vissza, jó a jelszó
                        else:
                            # visszatérési érték elmentése
                            return_code = stat.sysstat_init(passwd)

                            # ha kettővel tér vissza, nem sikerült a másolás
                            if return_code == 2:
                                # hibaüzenet
                                message = "Nem sikerült a " \
                                          "/etc/default/sysstat visszamásolása."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha eggyel tér vissza, nem sikerült a fájl
                            # tulajdonosának módosítása
                            elif return_code == 1:
                                # hibaüzenet
                                message = "A /etc/default/sysstat " \
                                          "jogosultságainak beállítása " \
                                          "sikertelen."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha hárommal tér vissza, IO hiba a backupnál
                            elif return_code == 3:
                                # hibaüzenet
                                message = "A /etc/default/sysstat biztonsági " \
                                          "mentése sikertelen (I/O hiba)."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha néggyel tér vissza, shutil hiba a backupnál
                            elif return_code == 4:
                                # hibaüzenet
                                message = "A /etc/default/sysstat biztonsági " \
                                          "mentése sikertelen (shutil hiba)."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha öttel tér vissza, OS hiba a backupnál
                            elif return_code == 5:
                                # hibaüzenet
                                message = "A /etc/default/sysstat biztonsági " \
                                          "mentése sikertelen (OS hiba)."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha hattal tér vissza, már engedélyeze volt a
                            # naplózás
                            elif return_code == 6:
                                # üzenet
                                message = "A naplózás már engedélyezve van."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message)

                            # ha nullával tért vissza, sikeres volt a művelet
                            elif return_code == 0:
                                # újraindítjuk a szolgáltatást
                                if stat.sysstat_restart(passwd) == 0:
                                    message = [
                                        "A sysstat engedélyezése, valamint",
                                        "a szolgáltatás újraindítása sikeres "
                                        "volt."]
                                    # kiírjuk
                                    cf.print_list(item, message)

                                # ha a szolgáltatás nem indult újra
                                else:
                                    # hibaüzenet
                                    message = [
                                        "A sysstat engedélyezése, sikeres "
                                        "volt, ám",
                                        "a szolgáltatás újraindítása "
                                        "sikertelen."]
                                    # kiírjuk
                                    cf.print_list(item, message, red=True)

                    # felülírjuk a felhasználó jelszavát
                    passwd = os.urandom(len(passwd))

                # ha nincs telepített függőség
                else:
                    # kiírnadó elemek
                    items = ["A folytatáshoz szükség van a sysstat nevű "
                             "csomagra.", "A telepítés eltarthat egy ideig.",
                        "Szeretnéd telepíteni a csomagot? (igen/nem)"]

                    # válasz bekérése
                    answer = cf.get_param_with_list(item, items)

                    # ha a felhasználó nem adott meg választ
                    if not answer:
                        message = "Hiba: Nem adtál meg választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a válasz igen
                    elif answer == "igen" or answer == "Igen":
                        # befejező szöveg
                        finish = "Nyomj entert a befejezéshez"
                        # bekérő szöveg
                        prompt_str = "Add meg a jelszavad:"
                        # leírás a felhasználónak
                        descr = "A telepítés eltarthat néhány percig."
                        # jelszó bekérése
                        passwd = cf.get_pass(item, descr, prompt_str)

                        # ha a felhasználó nem adott meg jelszót
                        if not passwd:
                            message = "Hiba: Nem adtál meg jelszót."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha adott meg jelszót
                        else:
                            package = "sysstat"
                            # meghívjuk a csomagtelepítőt
                            return_value = ip.install_package(package, passwd)

                            # ha 3-mal tér vissza, autentikációs hiba (sudo)
                            if return_value == 3:
                                message = []
                                message1 = "Hitelesítési hiba"
                                message2 = "Elgépelted a jelszavadat."
                                message.append(message1)
                                message.append(message2)
                                # kiírjuk a hibaüzenetet
                                cf.print_list(item, message, red=True)

                            # ha 2-vel tér vissza, nem sikerült a csomaglista
                            #  frissítése
                            elif return_value == 2:
                                message = "A csomaglista frissítése sikertelen."
                                cf.print_string(item, message, red=True,
                                    finish=finish)

                            # ha 0-val tér vissza, sikeres volt a
                            # programtelepítés
                            elif return_value == 0:
                                # ekkor elvégezzük az alapbeállításokat
                                # visszatérési érték elmentése
                                return_code = stat.sysstat_init(passwd)

                                # ha kettővel tér vissza, nem sikerült a másolás
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/default/sysstat " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/default/sysstat " \
                                              "jogosultságainak beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hárommal tér vissza, IO hiba a backupnál
                                elif return_code == 3:
                                    # hibaüzenet
                                    message = "A /etc/default/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(I/O hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha néggyel tér vissza, shutil hiba a backupnál
                                elif return_code == 4:
                                    # hibaüzenet
                                    message = "A /etc/default/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(shutil hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha öttel tér vissza, OS hiba a backupnál
                                elif return_code == 5:
                                    # hibaüzenet
                                    message = "A /etc/default/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(OS hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hattal tér vissza, már engedélyeze volt a
                                # naplózás
                                elif return_code == 6:
                                    # üzenet
                                    message = "A naplózás már engedélyezve van."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message)

                                #  ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    # újraindítjuk a szolgáltatást
                                    if stat.sysstat_restart(passwd) == 0:
                                        message = ["A sysstat engedélyezése, "
                                                   "valamint",
                                            "a szolgáltatás újraindítása "
                                            "sikeres volt."]
                                        # kiírjuk
                                        cf.print_list(item, message)

                                    # ha a szolgáltatás nem indult újra
                                    else:
                                        # hibaüzenet
                                        message = [
                                            "Hiba a telepítés után:",
                                            "A sysstat engedélyezése, sikeres "
                                            "volt, ám",
                                            "a szolgáltatás újraindítása "
                                            "sikertelen."]
                                        # kiírjuk
                                        cf.print_list(item, message, red=True)

                            # ha 1-gyel tér vissza, sikertelen volt a telepítés
                            else:
                                message = "A(z) {p} program telepítése " \
                                          "sikertelen volt.".format(p=package)
                                cf.print_string(item, message, red=True,
                                    finish=finish)

                        # felülírjuk a felhasználó jelszavát
                        passwd = os.urandom(len(passwd))

                    # ha a válasz nem
                    elif answer == "nem" or answer == "Nem":
                        message = "Nem telepítjük a csomagot."
                        # kiírjuk az üzenetet
                        cf.print_string(item, message)

                    # ha a válasz ismeretlen
                    else:
                        message = "Hiba: Nem adtál meg felismerhető választ."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)


        if x == ord('2'):
            # a menüpont neve
            item = "UbiSysPy – Naplózás menü – Naplózás beállítása – " \
                   "Naplók megőrzési ideje"
            curses.endwin()  # befejezzük a cursest

            # ellenőrizzük a sysstatot
            # ha van telepített függőség
            if d.dep_check("sysstat"):
                # felhasználó és sudo jog ellenőrzése
                # ha 0-val tér vissza, a user root
                if sudo.check_root() == 0:
                    messages = ["Add meg, hány napig szeretnéd tárolni a "
                                "naplókat.",
                    "Amennyiben 28-nál nagyobb számot adsz meg, a napló fájlok",
                    "több könyvtárba kerülnek mentésre 28-asával.",
                    "",
                    "Adj meg egy számot:"]
                    # bekérjük a számot a felhasználótól
                    value = cf.get_param_with_list(item, messages)

                    # ha a felhasználó nem adott meg semmit
                    if not value:
                        message = "Hiba: Nem adtál meg értéket."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó nem számot adott meg, vagy 0-t adott meg
                    elif not value.isdigit() or int(value) < 1:
                        message = ["Hiba: Nem adtál meg megfelelő értéket.",
                        "Adj meg egy nullánál nagyobb számot."]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    # ha megfelelő értéket adott meg
                    else:
                        # visszatérési érték elmentése
                        # a kulcs a HISTORY, mivel a megőrzési időt módosítjuk
                        return_code = stat.sysstat_setting(True, value,
                            "HISTORY")

                        # ha kettővel tér vissza, nem sikerült a másolás
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/sysstat/sysstat " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat " \
                                      "jogosultságainak beállítása sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, IO hiba a backupnál
                        elif return_code == 3:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (I/O hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha néggyel tér vissza, shutil hiba a backupnál
                        elif return_code == 4:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (shutil hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha öttel tér vissza, OS hiba a backupnál
                        elif return_code == 5:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (OS hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            # újraindítjuk a szolgáltatást
                            if stat.sysstat_restart(True) == 0:
                                message = ["A beállítás módosítása, valamint",
                                    "a szolgáltatás újraindítása sikeres volt."]
                                # kiírjuk
                                cf.print_list(item, message)

                            # ha a szolgáltatás nem indult újra
                            else:
                                # hibaüzenet
                                message = [
                                    "A beállítás módosítása, sikeres volt, ám",
                                    "a szolgáltatás újraindítása sikertelen."]
                                # kiírjuk
                                cf.print_list(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif sudo.check_root() == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif sudo.check_root() == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó adott meg jelszót
                    else:
                        # ellenőrizzük a sudo jelszót
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                        # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            messages = ["Add meg, hány napig szeretnéd "
                                        "tárolni a naplókat.",
                            "Amennyiben 28-nál nagyobb számot adsz meg, "
                            "a napló fájlok",
                            "több könyvtárba kerülnek mentésre 28-asával.",
                            "",
                            "Adj meg egy számot:"]
                            # bekérjük a számot a felhasználótól
                            value = cf.get_param_with_list(item, messages)

                            # ha a felhasználó nem adott meg semmit
                            if not value:
                                message = "Hiba: Nem adtál meg értéket."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha a felhasználó nem számot adott meg, vagy 0-t
                            #  adott meg
                            elif not value.isdigit() or int(value) < 1:
                                message = ["Hiba: Nem adtál meg megfelelő "
                                           "értéket.",
                                "Adj meg egy nullánál nagyobb számot."]
                                # kiírjuk a hibaüzenetet
                                cf.print_list(item, message, red=True)

                            # ha megfelelő értéket adott meg
                            else:
                                # visszatérési érték elmentése
                                # a kulcs a HISTORY, mivel a megőrzési időt
                                # módosítjuk
                                return_code = stat.sysstat_setting(passwd,
                                    value, "HISTORY")

                                # ha kettővel tér vissza, nem sikerült a másolás
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/sysstat/sysstat " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "jogosultságainak beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hárommal tér vissza, IO hiba a backupnál
                                elif return_code == 3:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(I/O hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha néggyel tér vissza, shutil hiba a backupnál
                                elif return_code == 4:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(shutil hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha öttel tér vissza, OS hiba a backupnál
                                elif return_code == 5:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(OS hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    # újraindítjuk a szolgáltatást
                                    if stat.sysstat_restart(passwd) == 0:
                                        message = ["A beállítás módosítása, "
                                                   "valamint",
                                            "a szolgáltatás újraindítása "
                                            "sikeres volt."]
                                        # kiírjuk
                                        cf.print_list(item, message)

                                    # ha a szolgáltatás nem indult újra
                                    else:
                                        # hibaüzenet
                                        message = [
                                            "A beállítás módosítása, sikeres "
                                            "volt, ám",
                                            "a szolgáltatás újraindítása "
                                            "sikertelen."]
                                        # kiírjuk
                                        cf.print_list(item, message, red=True)

            # ha nincs telepített függőség
            else:
                # hibaüzenet
                message = ["Hiba: hiányzó függőség",
                "A művelet elvégzéséhez szükség van a sysstat nevű csomagra.",
                "A Naplózás menü > Naplózás beállítása >",
                "A sysstat ellenőrzése, telepítése, alapbeállítása",
                "menüpont segítségével telepítheted a programot."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)


        if x == ord('3'):
            # a menüpont neve
            item = "UbiSysPy – Naplózás menü – Naplózás beállítása – " \
                   "Naplók tömörítési ideje"
            curses.endwin()  # befejezzük a cursest

            # ellenőrizzük a sysstatot
            # ha van telepített függőség
            if d.dep_check("sysstat"):
                # felhasználó és sudo jog ellenőrzése
                # ha 0-val tér vissza, a user root
                if sudo.check_root() == 0:
                    messages = ["Add meg, hány nap után szeretnéd "
                                "tömöríteni a naplókat.",
                        "",
                        "Adj meg egy számot:"]
                    # bekérjük a számot a felhasználótól
                    value = cf.get_param_with_list(item, messages)

                    # ha a felhasználó nem adott meg semmit
                    if not value:
                        message = "Hiba: Nem adtál meg értéket."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó nem számot adott meg, vagy 0-t adott meg
                    elif not value.isdigit() or int(value) < 1:
                        message = ["Hiba: Nem adtál meg megfelelő értéket.",
                            "Adj meg egy nullánál nagyobb számot."]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    # ha megfelelő értéket adott meg
                    else:
                        # visszatérési érték elmentése
                        # a kulcs a COMPRESSAFTER, mivel a tömörítési időt
                        # módosítjuk
                        return_code = stat.sysstat_setting(True, value,
                            "COMPRESSAFTER")

                        # ha kettővel tér vissza, nem sikerült a másolás
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/sysstat/sysstat " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat " \
                                      "jogosultságainak beállítása sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, IO hiba a backupnál
                        elif return_code == 3:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (I/O hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha néggyel tér vissza, shutil hiba a backupnál
                        elif return_code == 4:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (shutil hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha öttel tér vissza, OS hiba a backupnál
                        elif return_code == 5:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (OS hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            # újraindítjuk a szolgáltatást
                            if stat.sysstat_restart(True) == 0:
                                message = ["A beállítás módosítása, valamint",
                                    "a szolgáltatás újraindítása sikeres volt."]
                                # kiírjuk
                                cf.print_list(item, message)

                            # ha a szolgáltatás nem indult újra
                            else:
                                # hibaüzenet
                                message = ["A beállítás módosítása, sikeres "
                                           "volt, ám",
                                    "a szolgáltatás újraindítása sikertelen."]
                                # kiírjuk
                                cf.print_list(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif sudo.check_root() == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif sudo.check_root() == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó adott meg jelszót
                    else:
                        # ellenőrizzük a sudo jelszót
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                        # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            messages = ["Add meg, hány nap után szeretnéd "
                                        "tömöríteni a naplókat.",
                                "",
                                "Adj meg egy számot:"]
                            # bekérjük a számot a felhasználótól
                            value = cf.get_param_with_list(item, messages)

                            # ha a felhasználó nem adott meg semmit
                            if not value:
                                message = "Hiba: Nem adtál meg értéket."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha a felhasználó nem számot adott meg, vagy 0-t
                            #  adott meg
                            elif not value.isdigit() or int(value) < 1:
                                message = ["Hiba: Nem adtál meg megfelelő "
                                           "értéket.",
                                    "Adj meg egy nullánál nagyobb számot."]
                                # kiírjuk a hibaüzenetet
                                cf.print_list(item, message, red=True)

                            # ha megfelelő értéket adott meg
                            else:
                                # visszatérési érték elmentése
                                # a kulcs a COMPRESSAFTER, mivel a tömörítési
                                #  időt
                                # módosítjuk
                                return_code = stat.sysstat_setting(passwd,
                                    value, "COMPRESSAFTER")

                                # ha kettővel tér vissza, nem sikerült a másolás
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/sysstat/sysstat " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "jogosultságainak beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hárommal tér vissza, IO hiba a backupnál
                                elif return_code == 3:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(I/O hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha néggyel tér vissza, shutil hiba a backupnál
                                elif return_code == 4:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(shutil hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha öttel tér vissza, OS hiba a backupnál
                                elif return_code == 5:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(OS hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    # újraindítjuk a szolgáltatást
                                    if stat.sysstat_restart(passwd) == 0:
                                        message = ["A beállítás módosítása, "
                                                   "valamint",
                                            "a szolgáltatás újraindítása "
                                            "sikeres volt."]
                                        # kiírjuk
                                        cf.print_list(item, message)

                                    # ha a szolgáltatás nem indult újra
                                    else:
                                        # hibaüzenet
                                        message = ["A beállítás módosítása, "
                                                   "sikeres volt, ám",
                                            "a szolgáltatás újraindítása "
                                            "sikertelen."]
                                        # kiírjuk
                                        cf.print_list(item, message, red=True)

            # ha nincs telepített függőség
            else:
                # hibaüzenet
                # hibaüzenet
                message = ["Hiba: hiányzó függőség",
                "A művelet elvégzéséhez szükség van a sysstat nevű csomagra.",
                "A Naplózás menü > Naplózás beállítása >",
                "A sysstat ellenőrzése, telepítése, alapbeállítása",
                "menüpont segítségével telepítheted a programot."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)


        if x == ord('4'):
            # a menüpont neve
            item = "UbiSysPy – Naplózás menü – Naplózás beállítása – " \
                   "Naplók tömörítési programja"
            curses.endwin()  # befejezzük a cursest

            # ellenőrizzük a sysstatot
            # ha van telepített függőség
            if d.dep_check("sysstat"):
                # felhasználó és sudo jog ellenőrzése
                # ha 0-val tér vissza, a user root
                if sudo.check_root() == 0:
                    messages = ["Add meg, melyik programmal szeretnéd "
                                "tömöríteni a naplókat.",
                        "A lehetőségek:",
                        "1) xz",
                        "2) gzip",
                        "3) bzip2",
                        "",
                        "Adj meg egy számot:"]
                    # bekérjük a számot a felhasználótól
                    value_number = cf.get_param_with_list(item, messages)

                    # ha a felhasználó nem adott meg semmit
                    if not value_number:
                        message = "Hiba: Nem adtál meg értéket."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó nem számot adott meg, vagy 0-t adott
                    # meg, vagy háromnál nagyobb számot
                    elif not value_number.isdigit() or int(value_number) < 1 \
                            or int(value_number) > 3:
                        message = ["Hiba: Nem adtál meg megfelelő értéket.",
                            "Adj meg egy nullánál nagyobb, de háromnál kisebb "
                            "számot."]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    # különben továbbmegyünk
                    else:
                        # ha egyet adott meg, az xz-t választotta
                        if int(value_number) == 1:
                            value = "xz"

                        # ha kettőt adott meg, a gzip-et választotta
                        elif int(value_number) == 2:
                            value = "gzip"

                        # ha hármat adott meg, a bzip2-t választotta
                        elif int(value_number) == 3:
                            value = "bzip2"

                        # visszatérési érték elmentése
                        # a kulcs a ZIP, mivel a tömörítési programot
                        # módosítjuk
                        return_code = stat.sysstat_setting(True, value,
                            "ZIP")

                        # ha kettővel tér vissza, nem sikerült a másolás
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/sysstat/sysstat " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat " \
                                      "jogosultságainak beállítása sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, IO hiba a backupnál
                        elif return_code == 3:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (I/O hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha néggyel tér vissza, shutil hiba a backupnál
                        elif return_code == 4:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (shutil hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha öttel tér vissza, OS hiba a backupnál
                        elif return_code == 5:
                            # hibaüzenet
                            message = "A /etc/sysstat/sysstat biztonsági " \
                                      "mentése sikertelen (OS hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            # újraindítjuk a szolgáltatást
                            if stat.sysstat_restart(True) == 0:
                                message = ["A beállítás módosítása, valamint",
                                    "a szolgáltatás újraindítása sikeres volt."]
                                # kiírjuk
                                cf.print_list(item, message)

                            # ha a szolgáltatás nem indult újra
                            else:
                                # hibaüzenet
                                message = ["A beállítás módosítása, sikeres "
                                           "volt, ám",
                                    "a szolgáltatás újraindítása sikertelen."]
                                # kiírjuk
                                cf.print_list(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif sudo.check_root() == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif sudo.check_root() == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó adott meg jelszót
                    else:
                        # ellenőrizzük a sudo jelszót
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                        # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            messages = ["Add meg, melyik programmal szeretnéd "
                                        "tömöríteni a naplókat.",
                                "A lehetőségek:",
                                "1) xz",
                                "2) gzip",
                                "3) bzip2",
                                "",
                                "Adj meg egy számot:"]
                            # bekérjük a számot a felhasználótól
                            value_number = cf.get_param_with_list(item,
                                messages)

                            # ha a felhasználó nem adott meg semmit
                            if not value_number:
                                message = "Hiba: Nem adtál meg értéket."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha a felhasználó nem számot adott meg, vagy 0-t
                            # adott meg, vagy háromnál nagyobb számot
                            elif not value_number.isdigit() or \
                                    int(value_number) < 1 or \
                                    int(value_number) > 3:
                                message = [
                                    "Hiba: Nem adtál meg megfelelő értéket.",
                                    "Adj meg egy nullánál nagyobb, "
                                    "de háromnál kisebb számot."]
                                # kiírjuk a hibaüzenetet
                                cf.print_list(item, message, red=True)

                            # különben továbbmegyünk
                            else:
                                # ha egyet adott meg, az xz-t választotta
                                if int(value_number) == 1:
                                    value = "xz"

                                # ha kettőt adott meg, a gzip-et választotta
                                elif int(value_number) == 2:
                                    value = "gzip"

                                # ha hármat adott meg, a bzip2-t választotta
                                elif int(value_number) == 3:
                                    value = "bzip2"
                                # visszatérési érték elmentése
                                # a kulcs a ZIP, mivel a tömörítési
                                # programot módosítjuk
                                return_code = stat.sysstat_setting(passwd,
                                    value, "ZIP")

                                # ha kettővel tér vissza, nem sikerült a másolás
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/sysstat/sysstat " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "jogosultságainak beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hárommal tér vissza, IO hiba a backupnál
                                elif return_code == 3:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(I/O hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha néggyel tér vissza, shutil hiba a backupnál
                                elif return_code == 4:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(shutil hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha öttel tér vissza, OS hiba a backupnál
                                elif return_code == 5:
                                    # hibaüzenet
                                    message = "A /etc/sysstat/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(OS hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    # újraindítjuk a szolgáltatást
                                    if stat.sysstat_restart(passwd) == 0:
                                        message = ["A beállítás módosítása, "
                                                   "valamint",
                                            "a szolgáltatás újraindítása "
                                            "sikeres volt."]
                                        # kiírjuk
                                        cf.print_list(item, message)

                                    # ha a szolgáltatás nem indult újra
                                    else:
                                        # hibaüzenet
                                        message = ["A beállítás módosítása, "
                                                   "sikeres volt, ám",
                                            "a szolgáltatás újraindítása "
                                            "sikertelen."]
                                        # kiírjuk
                                        cf.print_list(item, message, red=True)

            # ha nincs telepített függőség
            else:
                # hibaüzenet
                message = ["Hiba: hiányzó függőség",
                "A művelet elvégzéséhez szükség van a sysstat nevű csomagra.",
                "A Naplózás menü > Naplózás beállítása >",
                "A sysstat ellenőrzése, telepítése, alapbeállítása",
                "menüpont segítségével telepítheted a programot."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)


        if x == ord('5'):
            # a menüpont neve
            item = "UbiSysPy – Naplózás menü – Naplózás beállítása – " \
                   "Naplózás ütemezése"
            curses.endwin()  # befejezzük a cursest

            # ellenőrizzük a sysstatot
            # ha van telepített függőség
            if d.dep_check("sysstat"):
                # felhasználó és sudo jog ellenőrzése
                # ha 0-val tér vissza, a user root
                if sudo.check_root() == 0:
                    messages = ["Add meg, hány percenként szeretnéd "
                                "menteni a naplókat.",
                                "",
                                "Adj meg egy számot:"]

                    # bekérjük a számot a felhasználótól
                    value = cf.get_param_with_list(item, messages)

                    # ha a felhasználó nem adott meg semmit
                    if not value:
                        message = "Hiba: Nem adtál meg értéket."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó nem számot adott meg, vagy 0-t adott meg
                    elif not value.isdigit() or int(value) < 1 or \
                            int(value) > 60:
                        message = ["Hiba: Nem adtál meg megfelelő értéket.",
                            "Adj meg egy nullánál nagyobb, de hatvannál kisebb "
                            "számot."]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                    # különben továbbmegyünk
                    else:
                        # visszatérési érték elmentése
                        return_code = stat.change_cron(True, value)

                        # ha kettővel tér vissza, nem sikerült a másolás
                        if return_code == 2:
                            # hibaüzenet
                            message = "Nem sikerült a /etc/cron.d/sysstat " \
                                      "visszamásolása."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha eggyel tér vissza, nem sikerült a fájl
                        # tulajdonosának módosítása
                        elif return_code == 1:
                            # hibaüzenet
                            message = "A /etc/cron.d/sysstat " \
                                      "jogosultságainak beállítása sikertelen."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha hárommal tér vissza, IO hiba a backupnál
                        elif return_code == 3:
                            # hibaüzenet
                            message = "A /etc/cron.d/sysstat biztonsági " \
                                      "mentése sikertelen (I/O hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha néggyel tér vissza, shutil hiba a backupnál
                        elif return_code == 4:
                            # hibaüzenet
                            message = "A /etc/cron.d/sysstat biztonsági " \
                                      "mentése sikertelen (shutil hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha öttel tér vissza, OS hiba a backupnál
                        elif return_code == 5:
                            # hibaüzenet
                            message = "A /etc/cron.d/sysstat biztonsági " \
                                      "mentése sikertelen (OS hiba)."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha nullával tért vissza, sikeres volt a művelet
                        elif return_code == 0:
                            # újraindítjuk a szolgáltatást
                            if stat.sysstat_restart(True) == 0:
                                message = ["A beállítás módosítása, valamint",
                                    "a szolgáltatás újraindítása sikeres volt."]
                                # kiírjuk
                                cf.print_list(item, message)

                            # ha a szolgáltatás nem indult újra
                            else:
                                # hibaüzenet
                                message = ["A beállítás módosítása, sikeres "
                                           "volt, ám",
                                    "a szolgáltatás újraindítása sikertelen."]
                                # kiírjuk
                                cf.print_list(item, message, red=True)

                # ha 2-vel tér vissza, nincs sudo joga
                elif sudo.check_root() == 2:
                    message = "Nincs sudo jogod, így a művelet nem folytatható."
                    # kiírjuk a hibaüzenetet
                    cf.print_string(item, message, red=True)

                # ha 1-gyel tér vissza, van sudo joga
                elif sudo.check_root() == 1:
                    # bekérő szöveg
                    prompt_str = "Add meg a jelszavad:"
                    # leírás a felhasználónak
                    descr = "A következő művelethez root jogra lesz szükséged."
                    # jelszó bekérése a tükörváltáshoz
                    passwd = cf.get_pass(item, descr, prompt_str)

                    # ha a felhasználó nem adott meg jelszót
                    if not passwd:
                        message = "Hiba: Nem adtál meg jelszót."
                        # kiírjuk a hibaüzenetet
                        cf.print_string(item, message, red=True)

                    # ha a felhasználó adott meg jelszót
                    else:
                        # ellenőrizzük a sudo jelszót
                        if not sudo.sudo_test(passwd):
                            message = "Hitelesítési hiba. Elírtad a jelszavad."
                            cf.print_string(item, message, red=True)

                        # ha igazzal tér vissza, rendben van a jelszó
                        else:
                            messages = ["Add meg, hány percenként szeretnéd "
                                        "menteni a naplókat.", "",
                                "Adj meg egy számot:"]

                            # bekérjük a számot a felhasználótól
                            value = cf.get_param_with_list(item,
                                messages)

                            # ha a felhasználó nem adott meg semmit
                            if not value:
                                message = "Hiba: Nem adtál meg értéket."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                            # ha a felhasználó nem számot adott meg, vagy 0-t
                            #  adott meg
                            elif not value.isdigit() or int(value) < 1 or \
                                    int(value) > 60:
                                message = [
                                    "Hiba: Nem adtál meg megfelelő értéket.",
                                    "Adj meg egy nullánál nagyobb, "
                                    "de hatvannál kisebb számot."]
                                # kiírjuk a hibaüzenetet
                                cf.print_list(item, message, red=True)

                            # különben továbbmegyünk
                            else:
                                # visszatérési érték elmentése
                                return_code = stat.change_cron(passwd, value)

                                # ha kettővel tér vissza, nem sikerült a másolás
                                if return_code == 2:
                                    # hibaüzenet
                                    message = "Nem sikerült a " \
                                              "/etc/cron.d/sysstat " \
                                              "visszamásolása."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha eggyel tér vissza, nem sikerült a fájl
                                # tulajdonosának módosítása
                                elif return_code == 1:
                                    # hibaüzenet
                                    message = "A /etc/cron.d/sysstat " \
                                              "jogosultságainak beállítása " \
                                              "sikertelen."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha hárommal tér vissza, IO hiba a backupnál
                                elif return_code == 3:
                                    # hibaüzenet
                                    message = "A /etc/cron.d/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(I/O hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha néggyel tér vissza, shutil hiba a backupnál
                                elif return_code == 4:
                                    # hibaüzenet
                                    message = "A /etc/cron.d/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(shutil hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha öttel tér vissza, OS hiba a backupnál
                                elif return_code == 5:
                                    # hibaüzenet
                                    message = "A /etc/cron.d/sysstat " \
                                              "biztonsági mentése sikertelen " \
                                              "(OS hiba)."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha nullával tért vissza, sikeres volt a
                                # művelet
                                elif return_code == 0:
                                    # újraindítjuk a szolgáltatást
                                    if stat.sysstat_restart(passwd) == 0:
                                        message = ["A beállítás módosítása, "
                                                   "valamint",
                                            "a szolgáltatás újraindítása "
                                            "sikeres volt."]
                                        # kiírjuk
                                        cf.print_list(item, message)

                                    # ha a szolgáltatás nem indult újra
                                    else:
                                        # hibaüzenet
                                        message = ["A beállítás módosítása, "
                                                   "sikeres volt, ám",
                                            "a szolgáltatás újraindítása "
                                            "sikertelen."]
                                        # kiírjuk
                                        cf.print_list(item, message, red=True)

            # ha nincs telepített függőség
            else:
                # hibaüzenet
                message = ["Hiba: hiányzó függőség",
                "A művelet elvégzéséhez szükség van a sysstat nevű csomagra.",
                "A Naplózás menü > Naplózás beállítása >",
                "A sysstat ellenőrzése, telepítése, alapbeállítása",
                "menüpont segítségével telepítheted a programot."]
                # kiírjuk a hibaüzenetet
                cf.print_list(item, message, red=True)


        if x == ord('6'):
            # a menüpont neve
            item = "UbiSysPy – Naplózás menü – Naplózás beállítása – " \
                   "Grafikon készítés"

            items = ["A mentett rendszernaplókból grafikonok készíthetőek.",
                "Engedélyezés esetén 5 percenként frissülnek a grafikonok.",
                "Szeretnéd beállítani az ütemezést? (igen/nem)"]

            # válasz bekérése
            answer = cf.get_param_with_list(item, items)

            # ha a felhasználó nem adott meg választ
            if not answer:
                message = "Hiba: Nem adtál meg választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)

            # ha a válasz igen
            elif answer == "igen" or answer == "Igen":
                # ha van telepített függőség
                if d.dep_check("sysstat"):
                    # sysstat init függvény visszatérési érték elmentése
                    return_code = stat.sysstat_init(True)
                    # ha a sysstat beállítás hattal tér vissza, már be van
                    # állítva a naplózás, mehetünk tovább
                    if return_code == 6:
                        message = ["Szeretnéd beállítani/leállítani a "
                                   "grafikonok készítésének",
                                  "ütemezését? (igen/nem)"]

                        # válasz bekérése
                        answer = cf.get_param_with_list(item, message)

                        # ha a felhasználó nem adott meg választ
                        if not answer:
                            message = "Hiba: Nem adtál meg választ."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                        # ha a válasz igen
                        elif answer == "igen" or answer == "Igen":
                            # megnézzük, hogy van-e cron, mentjük az eredményt
                            return_code = c.check_crons()

                            # ha nullával tér vissza, mindkét cron létezik
                            if return_code == 0:
                                # üzenet
                                message = ["Az ütemezés már be van állítva.",
                                "Szeretnéd leállítani a grafikon készítést? "
                                "(igen/nem)"]

                                # válasz bekérése
                                answer = cf.get_param_with_list(item, message)

                                # ha a felhasználó nem adott meg választ
                                if not answer:
                                    message = "Hiba: Nem adtál meg választ."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha a válasz igen, rákérdezünk a törlésre
                                elif answer == "igen" or answer == "Igen":
                                    # meghívjuk a delete_cron()-t
                                    c.delete_cron()

                                    # ellenőrzésként meghívjuk a check_cront()
                                    return_code = c.check_crons()

                                    # ha nullával tér vissza, mindkét cron
                                    # létezik
                                    if return_code == 0:
                                        message = "Az ütemezés törlése nem " \
                                                  "sikerült."
                                        # kiírjuk a hibaüzenetet
                                        cf.print_string(item, message, red=True)

                                    # ha eggyel tér vissza, nincs cron
                                    elif return_code == 1:
                                        message = "Az ütemezés törlése " \
                                                  "sikeres volt."
                                        # kiírjuk az üzenetet
                                        cf.print_string(item, message)

                                    # ha kettővel tér vissza, egy cron van
                                    elif return_code == 2:
                                        message = "Hiba: Az ütemezés törlése " \
                                                  "nem teljes."
                                        # kiírjuk a hibaüzenetet
                                        cf.print_string(item, message, red=True)

                                # ha a válasz nem, nem törlünk
                                elif answer == "nem" or answer == "Nem":
                                    message = "Nem állítjuk le az ütemezést."
                                    # kiírjuk az üzenetet
                                    cf.print_string(item, message)

                                # ha a válasz ismeretlen
                                else:
                                    message = "Hiba: Nem adtál meg " \
                                              "felismerhető választ."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                            # ha eggyel tér vissza, nincs cron
                            elif return_code == 1:
                                # üzenet
                                message = ["Az ütemezés nincs beállítva.",
                                "Szeretnéd beállítani a grafikon készítést? "
                                "(igen/nem)"]

                                # válasz bekérése
                                answer = cf.get_param_with_list(item, message)

                                # ha a felhasználó nem adott meg választ
                                if not answer:
                                    message = "Hiba: Nem adtál meg választ."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                                # ha a válasz igen, beállítjuk
                                elif answer == "igen" or answer == "Igen":
                                    # meghívjuk a cron beállító függvényt
                                    c.create_cron()

                                    # ellenőrzésként meghívjuk a check_cront()
                                    return_code = c.check_crons()

                                    # ha nullával tér vissza, mindkét cron
                                    # létezik
                                    if return_code == 0:
                                        message = "Az ütemezés beállítása " \
                                                  "sikerült."
                                        # kiírjuk az üzenetet
                                        cf.print_string(item, message)

                                    # ha eggyel tér vissza, nincs cron
                                    elif return_code == 1:
                                        message = "Az ütemezés beállítása " \
                                                  "sikertelen volt."
                                        # kiírjuk a hibaüzenetet
                                        cf.print_string(item, message, red=True)

                                    # ha kettővel tér vissza, csak egy cron van
                                    elif return_code == 2:
                                        message = "Hiba: Az ütemezés " \
                                                  "beállítása nem teljes."
                                        # kiírjuk a hibaüzenetet
                                        cf.print_string(item, message, red=True)

                                # ha a válasz nem
                                elif answer == "nem" or answer == "Nem":
                                    message = "Nem állítjuk be az ütemezést."
                                    # kiírjuk az üzenetet
                                    cf.print_string(item, message)

                                # ha a válasz ismeretlen
                                else:
                                    message = "Hiba: Nem adtál meg " \
                                              "felismerhető választ."
                                    # kiírjuk a hibaüzenetet
                                    cf.print_string(item, message, red=True)

                            # ha kettővel tér vissza, csak egy cron van
                            elif return_code == 2:
                                message = "Hiba: Az ütemezés beállítása " \
                                          "nem teljes."
                                # kiírjuk a hibaüzenetet
                                cf.print_string(item, message, red=True)

                        # ha a válasz nem
                        elif answer == "nem" or answer == "Nem":
                            message = "Nem állítjuk be az ütemezést."
                            # kiírjuk az üzenetet
                            cf.print_string(item, message)

                        # ha a válasz ismeretlen
                        else:
                            message = "Hiba: Nem adtál meg felismerhető " \
                                      "választ."
                            # kiírjuk a hibaüzenetet
                            cf.print_string(item, message, red=True)

                    # ha nullával tér vissza, most végeztük el a műveletet
                    # írjuk ki a felhasználónak, hogy előbb állítsa be a
                    # mentést az előző menüpontokkal
                    elif return_code == 0:
                        # hibaüzenet
                        message = ["A naplózás engedélyezése sikeres volt.",
                        "Ám a grafikonok ütemezése előtt a 2-5 menüpontokkal",
                            "állítsd be a naplózást az igényeid szerint."]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message)

                    # ha bármi mással tér vissza, hiba volt a művelet
                    # elvégzésekor, írjuk ki, hogy az első menüponttal
                    # engedélyezze a naplózást, a többi menüponttal pedig
                    # állítsa be
                    else:
                        # hibaüzenet
                        message = ["A naplózás nincs engedélyezve.",
                        "A grafikonok ütemezése előtt az 1-5 menüpontokkal",
                            "állítsd be a naplózást az igényeid szerint."]
                        # kiírjuk a hibaüzenetet
                        cf.print_list(item, message, red=True)

                # ha nincs telepített függőség
                else:
                    # kiírnadó elemek
                    message = ["A grafikonok megjelenítéséhez a sysstat nevű "
                             "csomagra",
                        "és a naplózás engedélyezésére, beállítására van "
                        "szükség.",
                        "A grafikonok ütemezése előtt az 1-5 menüpontokkal",
                            "állítsd be a naplózást az igényeid szerint."]
                    # kiírjuk a hibaüzenetet
                    cf.print_list(item, message, red=True)

            # ha a válasz nem
            elif answer == "nem" or answer == "Nem":
                message = "Nem állítjuk be az ütemezést."
                # kiírjuk az üzenetet
                cf.print_string(item, message)

            # ha a válasz ismeretlen
            else:
                message = "Hiba: Nem adtál meg felismerhető választ."
                # kiírjuk a hibaüzenetet
                cf.print_string(item, message, red=True)
