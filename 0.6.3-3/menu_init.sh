#! /bin/bash

# Copyright (c) 2016, Sinkovics Vivien
#
# This file is part of UbiSysPy.
#
# UbiSysPy is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or any later version.
#
# UbiSysPy is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with UbiSysPy. If not, see <http://www.gnu.org/licenses/>.

# activation of the virtual environment
. venv/bin/activate

# start of UbiSysPy
python2 core/menu.py
